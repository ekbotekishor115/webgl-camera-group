class Triangle
{
    constructor()
    {
        this.shader = new TriangleShader();
        this.shader_texture = new TextureShader();
        this.fbo_scene = new Framebuffer();
        this.cube = new Cube();
        this.shaderProgramObject;
        this.shaderProgramObject_texture;
        this.vao;
        this.vbo;
        this.vbo_texcoord;

        this.smiley_texture = null;

        // light demo
        this.lightAds;
    }

    initialize()
    {
        this.shaderProgramObject = this.shader.getShaderProgramObject();
        this.shaderProgramObject_texture = this.shader_texture.getShaderProgramObject();

        // create fbo of required resolution
        this.fbo_scene.createFBO(1920, 1080);
        

        // geometry attribute declaration
        var trianglePosition = new Float32Array(
            [
                0.0, 1.0, 0.0,
                -1.0, -1.0, 0.0,
                1.0, -1.0, 0.0
            ]
        );

        var texcoord = new Float32Array([
                0.5, 1.0,
                0.0, 0.0,
                1.0, 0.0
        ]);

        // vao
        this.vao = gl.createVertexArray();
        gl.bindVertexArray(this.vao);

        // vbo pos
        this.vbo = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);
        gl.bufferData(gl.ARRAY_BUFFER, trianglePosition, gl.STATIC_DRAW);
        gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        // vbo texcoord
        this.vbo_texcoord = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_texcoord);
        gl.bufferData(gl.ARRAY_BUFFER, texcoord, gl.STATIC_DRAW);
        gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS, 2, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        gl.bindVertexArray(null);




        // gl.enable(gl.TEXTURE_2D);

        // this.smiley_texture = loadTextureFromFile('./assets/textures/Smiley.png');
        this.smiley_texture = loadTextureFromFile('./assets/textures/newHieghtMap.bmp');
        if (this.smiley_texture < 1)
        {
            alert("Texture loading failed");
        }

        // light demo
        this.lightAds = new Light();
        this.lightAds.initialize();
    }

    display(cam)
    {
        gl.useProgram(this.shaderProgramObject);

        // transformation
        var modelMatrix = mat4.create();
        var viewMatrix = mat4.create();
        var modelViewMatrix = mat4.create();
        var modelViewProjection = mat4.create();

        viewMatrix = cam.getViewMatrix();
        
        mat4.multiply(modelViewMatrix, modelMatrix, viewMatrix);
        mat4.multiply(modelViewProjection, perspectiveProjectionMatrix, modelViewMatrix);

        gl.uniformMatrix4fv(this.shader.mvpMatrixUniform, false, modelViewProjection);

        gl.bindVertexArray(this.vao);
        gl.drawArrays(gl.TRIANGLES, 0, 3);
        gl.bindVertexArray(null);

        gl.useProgram(null);
    }

    displayTexture(cam)
    {
        gl.clearColor(0.5, 0.3, 0.5, 1.0);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        gl.useProgram(this.shaderProgramObject_texture);

        // transformation
        var modelMatrix = mat4.create();
        var viewMatrix = mat4.create();
        var modelViewMatrix = mat4.create();
        var modelViewProjection = mat4.create();

        viewMatrix = cam.getViewMatrix();
        // mat4.lookAt(viewMatrix,
        //             [0.0, 0.0, 1.0],
        //             [0.0, 0.0 , 0.0],
        //             [0.0, 1.0 , 0.0]);
        
        mat4.multiply(modelViewMatrix, modelMatrix, viewMatrix);
        mat4.multiply(modelViewProjection, perspectiveProjectionMatrix, modelViewMatrix);
        
        gl.uniformMatrix4fv(this.shader_texture.mvpMatrixUniform, false, modelViewProjection);
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, this.smiley_texture);
        gl.uniform1i(this.shader_texture.textureSamplerUniform, 0);

        gl.bindVertexArray(this.vao);
        gl.drawArrays(gl.TRIANGLES, 0, 3);
        gl.bindVertexArray(null);
        
        gl.bindTexture(gl.TEXTURE_2D, null);

        gl.useProgram(null);
        
    }



    displayFBOScene(cam)
    {
        // render scene on custom fbo
        this.fbo_scene.bindFBO(canvas.width, canvas.height);

        this.displayTexture(cam);

        // unbind the custom fbo after rendering the scene
        this.fbo_scene.unbindFBO();

        // ============ rendering the fbo texture on screen quad ========= //
        gl.clearColor(0.3, 0.3, 0.3, 1.0);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        gl.useProgram(this.shaderProgramObject_texture);

        // transformation
        var modelMatrix = mat4.create();
        var viewMatrix = mat4.create();
        var modelViewMatrix = mat4.create();
        var modelViewProjection = mat4.create();

        viewMatrix = cam.getViewMatrix();
        
        mat4.multiply(modelViewMatrix, modelMatrix, viewMatrix);
        mat4.multiply(modelViewProjection, perspectiveProjectionMatrix, modelViewMatrix);
        
        gl.uniformMatrix4fv(this.shader_texture.mvpMatrixUniform, false, modelViewProjection);
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, this.fbo_scene.getFBOTexture());
        gl.uniform1i(this.shader_texture.textureSamplerUniform, 0);

        this.cube.drawCube();
        
        gl.bindTexture(gl.TEXTURE_2D, null);

        gl.useProgram(null);

    }

    displayLight(cam)
    {
        // set the light properties first
        this.lightAds.setAmbient(undefined, undefined);
        this.lightAds.setDiffuse(undefined, undefined);
        this.lightAds.setSpecular([0.3, 0.3, 0.3], [0.7, 0.7, 0.7]);

        // and then draw the lightened object
        this.lightAds.display(cam);
    }

    update()
    {
        // code
    }

    uninitializeEffect()
    {
        if (this.shader)
        {
            this.shader.uninitializeShader();
            this.shader = null;
        }

        if (this.vbo)
        {
            gl.deleteBuffer(this.vbo);
            this.vbo = null;
        }

        if (this.vbo_texcoord)
        {
            gl.deleteBuffer(this.vbo_texcoord);
            this.vbo_texcoord = null;
        }
        
        if (this.vao)
        {
            gl.deleteVertexArray(this.vao);
            this.vao = null;
        }
    }
}