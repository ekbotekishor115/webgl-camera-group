class Camera {

    constructor(width, height, position) {
        this.width = width;
        this.height = height;

        this.YAW = -90.0;
        this.PITCH = 0.0;
        this.SPEED = 4.5;
        this.SENSITIVITY = 0.1;
        this.ZOOM = 45.0;

        // Camera attributes
        this.position = vec3.fromValues(position[0], position[1], position[2]);
        this.front = vec3.fromValues(0.0, 0.0, -1.0);
        this.up = vec3.fromValues(0.0, 1.0, 0.0);
        this.right = vec3.create();
        this.worldUp = this.up;
        // euler angles
        this.yaw = this.YAW;
        this.pitch = this.PITCH;
        // Camera options
        this.movementSpeed = this.SPEED;
        this.mouseSensitivity = this.SENSITIVITY;
        this.zoom = this.ZOOM;

        this.lastX = 0.0;
        this.lastY = 0.0;
        this.firstMouse = true;

        this.updateCameraVectors();
    }

    getViewMatrix() {
        let tempViewMatrix = mat4.create();
        let center = vec3.create();
        vec3.add(center, this.position, this.front);
        mat4.lookAt(tempViewMatrix,
            this.position,
            center,
            this.up);
        return tempViewMatrix;
    }

    getEye() {
        return this.position;
    }

    updateCameraVectors() {

        let front_ = vec3.fromValues(
            Math.cos(degToRad(this.yaw)) * Math.cos(degToRad(this.pitch)),
            Math.sin(degToRad(this.pitch)),
            Math.sin(degToRad(this.yaw)) * Math.cos(degToRad(this.pitch)),
        );

        vec3.normalize(this.front, front_);

        // Recalculate right and up vector
        let tempVector = vec3.create();
        vec3.cross(tempVector, this.front, this.worldUp);
        vec3.normalize(this.right, tempVector);
    }

    updateResolution(_width, _height) {
        this.width = _width;
        this.height = _height;
    }

    keyboardInputs(keyEvent) {
        // in
        let velocity = this.movementSpeed * 0.1;
        if (keyEvent.code == 'KeyW') {
            vec3.scaleAndAdd(this.position, this.position, this.front, velocity);
        }

        // left
        if (keyEvent.code == 'KeyA') {
            let tempScale = vec3.create();
            vec3.scale(tempScale, this.right, velocity);
            vec3.sub(this.position, this.position, tempScale);
        }

        // out
        if (keyEvent.code == 'KeyS') {
            let tempScale = vec3.create();
            vec3.scale(tempScale, this.front, velocity);
            vec3.sub(this.position, this.position, tempScale);
        }

        // right
        if (keyEvent.code == 'KeyD') {
            vec3.scaleAndAdd(this.position, this.position, this.right, velocity);
        }

        if (keyEvent.shiftKey) {
            this.movementSpeed = 6.5;
        }
    }

    // Detects shift key up to decrease the movementSpeed
    inputOnKeyUp(event) {
        if (event.code == 'ShiftLeft') {
            this.movementSpeed = 4.5;
        }
    }
    mouseInputs(event) {
        if (this.firstMouse) {
            this.lastX = event.x;
            this.lastY = event.y;
            this.firstMouse = false;
        }
        let xoffset = event.x - this.lastX;
        let yoffset = this.lastY - event.y;
        this.lastX = event.x;
        this.lastY = event.y;


        let constrainPitch = true;
        xoffset *= this.mouseSensitivity;
        yoffset *= this.mouseSensitivity;

        this.yaw = (this.yaw + xoffset) % 360.0;
        this.pitch += yoffset;

        if (constrainPitch) {
            if (this.pitch > 89.0)
                this.pitch = 89.0;
            if (this.pitch < -89.0)
                this.pitch = -89.0;
        }

        this.updateCameraVectors();
    }

    // Process mouse scroll
    mouseScroll(event) {
        // console.log(event); 

        this.zoom += (event.deltaY / 100.0);
        console.log(this.zoom)
        if (this.zoom < 1.0)
            this.zoom = 1.0;
        if (this.zoom > 1000.0)
            this.zoom = 1000.0;

        mat4.perspective(
            perspectiveProjectionMatrix,
            degToRad(this.zoom),
            parseFloat(canvas.width) / parseFloat(canvas.height),
            0.1,
            1000.0)
    }

    
}