// webGL related variables
const VertexAttributeEnum = 
{
    CAM_ATTRIBUTE_POSITION : 0,
    CAM_ATTRIBUTE_COLOR : 1,
    CAM_ATTRIBUTE_NORMAL : 2,
    CAM_ATTRIBUTE_TEXCOORDS : 3,
    CAM_ATTRIBUTE_TANGENTS : 4,
    CAM_ATTRIBUTE_BITANGENTS : 5,
};


    function createAndCompileShaders(shaderStr, shaderType)
    {
        let shaderObject = gl.createShader(shaderType);
        gl.shaderSource(shaderObject, shaderStr);
        gl.compileShader(shaderObject);

        // Error Checking
        if (gl.getShaderParameter(shaderObject, gl.COMPILE_STATUS) == false) {
            // Failure
            let error = gl.getShaderInfoLog(shaderObject);
            if (error.length > 0) {
                console.log(shaderType + " shader compilation error : \n" + error);
                alert("SHADER COMPILATION ERROR, CHECK LOG FOR MORE INFO");
                return null;
            }
        }

        // Success
        return shaderObject;
    }

    function attachAndLinkShaderObjects(shaderNames, attributeEnums, attributeNames)
    {
        let shaderProgramObject = gl.createProgram();

        // Attaching Shader
        for (let shader of shaderNames)
            gl.attachShader(shaderProgramObject, shader);

        if (attributeEnums.length != attributeNames.length) {
            alert("Invalid Attributes passed to createAndCompileShaderProgramObject");
            return null;
        }

        /// Prelinking
        for (let i = 0; i < attributeEnums.length; i++) {
            gl.bindAttribLocation(shaderProgramObject, attributeEnums[i], attributeNames[i]);
        }

        // Shader Program Linking 
        gl.linkProgram(shaderProgramObject);

        // Error Checking
        if (gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS) == false) {
            let error = gl.getProgramInfoLog(shaderProgramObject);
            if (error.length > 0) {
                console.log("SHADER PROGRAM COMPILATION ERROR \n" + error);
                alert("SHADER PROGRAM OBJECT LINK ERROR");
                return null
            }
        }
        return shaderProgramObject;
    }

    function uninitializeShaders(shaderProgramObject) {

        if (shaderProgramObject) {
            gl.useProgram(shaderProgramObject);
    
            let shaderObjects = gl.getAttachedShaders(shaderProgramObject);
            for (let i = 0; i < shaderObjects.length; i++) {
                gl.detachShader(shaderProgramObject, shaderObjects[i]);
                gl.deleteShader(shaderObjects[i]);
                shaderObjects[i] = null;
            }
            gl.useProgram(null);
            gl.deleteProgram(shaderProgramObject);
            shaderProgramObject = null;
        }
    }
