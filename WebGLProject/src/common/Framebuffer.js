class Framebuffer {
    constructor() {
        this.fbo;
        this.rbo;
        this.fbo_texture;
    }

    createFBO(textureWidth, textureHeight) 
    {
        // local variable
        var maxRenderBufferSize;

        // // code
        maxRenderBufferSize = gl.getParameter(gl.MAX_RENDERBUFFER_SIZE);
        if (maxRenderBufferSize < textureWidth || maxRenderBufferSize < textureHeight) {
            console.log("maxRenderBufferSize is less than Tex width and height\n");
            return (false);
        }
        else {
            console.log("maxRenderBufferSize is greater than Tex Width and Height\n");
        }

        // create framebuffer object
        this.fbo = gl.createFramebuffer();
        gl.bindFramebuffer(gl.FRAMEBUFFER, this.fbo);

        // create renderbuffer object
        this.rbo = gl.createRenderbuffer();
        gl.bindRenderbuffer(gl.RENDERBUFFER, this.rbo);

        // Where to keep this Renderbuffer and what will be the format of Renderbuffer ?
        gl.renderbufferStorage(gl.RENDERBUFFER, gl.DEPTH_COMPONENT16, textureWidth, textureHeight);

        // Create empty texture for target scene
        var texture = new ImageData(textureWidth, textureHeight);

        this.fbo_texture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, this.fbo_texture);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);

        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGB, textureWidth, textureHeight, 0, gl.RGB, gl.UNSIGNED_SHORT_5_6_5, texture);
        gl.bindTexture(gl.TEXTURE_2D, null);

        //give above texxture to fbo
        gl.framebufferTexture2D(gl.FRAMEBUFFER,
            gl.COLOR_ATTACHMENT0,
            gl.TEXTURE_2D,
            this.fbo_texture,
            0);

        // give rbo to fbo
        gl.framebufferRenderbuffer(gl.FRAMEBUFFER, gl.DEPTH_ATTACHMENT, gl.RENDERBUFFER, this.rbo);

        // check whether the framebuffer is created successfully or not
        var result = gl.checkFramebufferStatus(gl.FRAMEBUFFER);
        if (result != gl.FRAMEBUFFER_COMPLETE) {
            console.log("FrameBuffer is not complete\n");
            return (false);
        }

        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
        return (true);

    }

    bindFBO(textureWidth, textureHeight)
    {
        gl.viewport(0, 0, textureWidth, textureHeight);
        gl.bindFramebuffer(gl.FRAMEBUFFER, this.fbo);
    }

    unbindFBO()
    {
        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    }

    getFBOTexture()
    {
        return this.fbo_texture;
    }
};