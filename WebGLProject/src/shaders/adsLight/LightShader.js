class LightShader
{
    constructor()
    {
        this.shaderProgramObject;
        this.vao;
        this.vbo_position;
        this.vbo_texcoord;
        this.perspectiveProjectionMatrix;

        this.textureSamplerUniform;

        this.modelMatrixUniform;
        this.viewMatrixUniform;
        this.projectionMatrixUniform;

        this.laUniform;
        this.ldUniform;
        this.lsUniform;

        this.kaUniform;
        this.kdUniform;
        this.ksUniform;

        this.materialShinessUniform;
        this.lightPositionUniform;

        this.lightAmbient = new Float32Array([0.1, 0.1, 0.1]);
        this.lightDiffuse = new Float32Array([1.0, 1.0, 1.0]);
        this.lightSpecular = new Float32Array([1.0, 1.0, 1.0]);
        this.lightPosition = new Float32Array([100.0, 100.0, 100.0]);

        this.materialAmbient = new Float32Array([0.0, 0.0, 0.0]);
        this.materialDiffuse = new Float32Array([0.5, 0.2, 0.7]);
        this.materialSpecular = new Float32Array([0.7, 0.7, 0.7]);
        this.materialShininess = 128.0;
        
        // vertex shader
        var vertexShaderSourceCode = 
        `#version 300 es
        \n
        in vec4 a_position;
        in vec3 a_normal;
        uniform mat4 u_modelMatrix;
        uniform mat4 u_viewMatrix;
        uniform mat4 u_projectionMatrix;
        uniform vec3 u_lightPosition;
        uniform mediump int u_lightingEnabled;
        out vec3 transformedNormals;
        out vec3 lightDirection;
        out vec3 viewerVector;
        void main(void)
        {
            // u_lightPosition.w = 0.0;
            if (u_lightingEnabled == 1)
            {
                vec4 eyeCoordinates = u_viewMatrix * u_modelMatrix * a_position;
                mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);
                transformedNormals = normalMatrix * a_normal;
                lightDirection = u_lightPosition - eyeCoordinates.xyz;
                viewerVector = -eyeCoordinates.xyz;
            }
            gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;
        }`;

        // vertex shader object
        this.vertexShaderObject = this.createShaderObjects(vertexShaderSourceCode, gl.VERTEX_SHADER);

        // fragment shader
        var fragmentShaderSourceCode = 
        `#version 300 es
        \n
        precision highp float;
        in vec3 transformedNormals;
        in vec3 lightDirection;
        in vec3 viewerVector;
        uniform vec3 u_la;
        uniform vec3 u_ld;
        uniform vec3 u_ls;
        uniform vec3 u_ka;
        uniform vec3 u_kd;
        uniform vec3 u_ks;
        uniform float u_materialShiness;
        uniform mediump int u_lightingEnabled;
        out vec4 FragColor;
        void main(void)
        {
            vec3 phong_ads_light;
            if (u_lightingEnabled == 1)
            {
                vec3 ambient = u_la * u_ka;
                vec3 normalized_transformed_normals = normalize(transformedNormals);
                vec3 normalized_light_direction = normalize(lightDirection);
                vec3 diffuse = u_ld * u_kd * max(dot(normalized_light_direction, normalized_transformed_normals), 0.0);
                vec3 reflectionVector = reflect(-normalized_light_direction, normalized_transformed_normals);
                vec3 normalized_viewervector = normalize(viewerVector);
                vec3 specular = u_ls * u_ks * pow(max(dot(reflectionVector, normalized_viewervector), 0.0), u_materialShiness);
                phong_ads_light = ambient + diffuse + specular;
            }
            else
            {
                phong_ads_light = vec3(1.0, 1.0, 1.0);
            }
            FragColor = vec4(phong_ads_light, 1.0);
        }`;

        // fragment shader object
        this.fragmentShaderObject = this.createShaderObjects(fragmentShaderSourceCode, gl.FRAGMENT_SHADER);

        // Attach and Link shaderObjects
        this.shaderProgramObject = attachAndLinkShaderObjects(
            [this.vertexShaderObject, this.fragmentShaderObject],
            [VertexAttributeEnum.CAM_ATTRIBUTE_POSITION, VertexAttributeEnum.CAM_ATTRIBUTE_NORMAL],
            ["a_position", "a_normal"]
        );
        if (this.shaderProgramObject == null) {
            alert("ShaderProgramObject failed");
        }

        // uniforms
        this.modelMatrixUniform = gl.getUniformLocation(this.shaderProgramObject, "u_modelMatrix");
        this.viewMatrixUniform = gl.getUniformLocation(this.shaderProgramObject, "u_viewMatrix");
        this.projectionMatrixUniform = gl.getUniformLocation(this.shaderProgramObject, "u_projectionMatrix");

        this.laUniform = gl.getUniformLocation(this.shaderProgramObject, "u_la");
        this.ldUniform = gl.getUniformLocation(this.shaderProgramObject, "u_ld");
        this.lsUniform = gl.getUniformLocation(this.shaderProgramObject, "u_ls");

        this.kaUniform = gl.getUniformLocation(this.shaderProgramObject, "u_ka");
        this.kdUniform = gl.getUniformLocation(this.shaderProgramObject, "u_kd");
        this.ksUniform = gl.getUniformLocation(this.shaderProgramObject, "u_ks");

        this.materialShinessUniform = gl.getUniformLocation(this.shaderProgramObject, "u_materialShiness");
        this.lightPositionUniform = gl.getUniformLocation(this.shaderProgramObject, "u_lightPosition");
        this.lightingEnabledUniform = gl.getUniformLocation(this.shaderProgramObject, "u_lightingEnabled");

    }

    initialize()
    {
        
    }

    createShaderObjects(shaderStr, shaderType)
    {
        let shaderObject = createAndCompileShaders(shaderStr, shaderType);
        if (shaderObject == null) 
        {
            alert(shaderType + " Shader Compilation failed !!!");
            return null;
        }

        return shaderObject;
    }

    getUniformLocation(uniform)
    {
        return gl.getUniformLocation(this.shaderProgramObject, uniform);
    }

    getShaderProgramObject()
    {
        return this.shaderProgramObject;
    }

    uninitializeShader()
    {
        uninitializeShaders(this.shaderProgramObject);
    }

};