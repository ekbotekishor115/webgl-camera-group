class Shapes
{
    constructor()
    {
        var cubePosition = new Float32Array([
            // top
            1.0, 1.0, -1.0,
            -1.0, 1.0, -1.0,
            -1.0, 1.0, 1.0,
            1.0, 1.0, 1.0,
    
            // bottom
            1.0, -1.0, -1.0,
            -1.0, -1.0, -1.0,
            -1.0, -1.0, 1.0,
            1.0, -1.0, 1.0,
    
            // front
            1.0, 1.0, 1.0,
            -1.0, 1.0, 1.0,
            -1.0, -1.0, 1.0,
            1.0, -1.0, 1.0,
    
            // back
            1.0, 1.0, -1.0,
            -1.0, 1.0, -1.0,
            -1.0, -1.0, -1.0,
            1.0, -1.0, -1.0,
    
            // right
            1.0, 1.0, -1.0,
            1.0, 1.0, 1.0,
            1.0, -1.0, 1.0,
            1.0, -1.0, -1.0,
    
            // left
            -1.0, 1.0, 1.0,
            -1.0, 1.0, -1.0,
            -1.0, -1.0, -1.0,
            -1.0, -1.0, 1.0, 
        ]);
    
        var cubeTexcoord = new Float32Array([
            1.0, 1.0,
            0.0, 1.0,
            0.0, 0.0,
            1.0, 0.0,
    
            1.0, 1.0,
            0.0, 1.0,
            0.0, 0.0,
            1.0, 0.0,
    
            1.0, 1.0,
            0.0, 1.0,
            0.0, 0.0,
            1.0, 0.0,
    
            1.0, 1.0,
            0.0, 1.0,
            0.0, 0.0,
            1.0, 0.0,
    
            1.0, 1.0,
            0.0, 1.0,
            0.0, 0.0,
            1.0, 0.0,
    
            1.0, 1.0,
            0.0, 1.0,
            0.0, 0.0,
            1.0, 0.0
        ]);
    
    
        // vao Cube
        this.vao_cube = gl.createVertexArray();
        gl.bindVertexArray(this.vao_cube);
    
        // vbo position
        this.vbo_cube_position = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_cube_position);
        gl.bufferData(gl.ARRAY_BUFFER, cubePosition, gl.STATIC_DRAW);
        gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION, 
            3, 
            gl.FLOAT, 
            false, 
            0, 
            0);
        gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
    
        // vbo color
        this.vbo_cube_texcoord = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_cube_texcoord);
        gl.bufferData(gl.ARRAY_BUFFER, cubeTexcoord, gl.STATIC_DRAW);
        gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS, 
            2, 
            gl.FLOAT, 
            false, 
            0, 
            0);
        gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
    
        // unbind vao
        gl.bindVertexArray(null);

        //================= Quad ==================//
        let position_ = new Float32Array([
            1.0, 1.0, 0.0,
            -1.0, 1.0, 0.0,
            -1.0, -1.0, 0.0,
            1.0, -1.0, 0.0]);

        let texcoord_ = new Float32Array([
            1.0, 1.0,
            0.0, 1.0,
            0.0, 0.0,
            1.0, 0.0]);
        
        let normals_ = new Float32Array([
            0.0, 0.0, 1.0,
            0.0, 0.0, 1.0,
            0.0, 0.0, 1.0,
            0.0, 0.0, 1.0
        ]);


        this.vao_quad = gl.createVertexArray();
        gl.bindVertexArray(this.vao_quad);
        {
            this.vbo_quad_position = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_quad_position);
            gl.bufferData(gl.ARRAY_BUFFER, position_, gl.STATIC_DRAW);
            gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
            gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);

            this.vbo_quad_texcoord = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_quad_texcoord);
            gl.bufferData(gl.ARRAY_BUFFER, texcoord_, gl.STATIC_DRAW);
            gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS, 2, gl.FLOAT, false, 0, 0);
            gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);

            this.vbo_quad_normal = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_quad_normal);
            gl.bufferData(gl.ARRAY_BUFFER, normals_, gl.STATIC_DRAW);
            gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
            gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_NORMAL);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);
        }
        gl.bindVertexArray(null);

        // 
        // this.vao_quad_n = null;
        // this.vbo_quad_position_n = null;
        // this.vbo_quad_texcoord_n = null;
        // this.vbo_quad_normal_n = null;
        // this.vbo_quad_tangent_n = null;
        // this.vbo_quad_bitangent_n = null;

        // this.initQuadForNormalMapping();

        //================= Quad Normalmapped ==================//
        let position = new Float32Array([
            1.0, 1.0, 0.0,
            -1.0, 1.0, 0.0,
            -1.0, -1.0, 0.0,
            1.0, -1.0, 0.0]);

        let texcoord = new Float32Array([
            1.0, 1.0,
            0.0, 1.0,
            0.0, 0.0,
            1.0, 0.0]);

        let normals = new Float32Array([
            0.0, 0.0, 1.0
        ]);

        // calculate tangent / bitangent vectors of both triangles
        var tangent1 = vec3.create();
        var bitangent1 = vec3.create();
        var tangent2 = vec3.create();
        var bitangent2 = vec3.create();

        // triangle 1
        var edge1 = vec3.create();
        var edge2 = vec3.create();
        var deltaUV1 = vec2.create();
        var deltaUV2 = vec2.create();
        vec3.sub(edge1, vec3.fromValues(position[3], position[4], position[5]), vec3.fromValues(position[0], position[1], position[2]));
        vec3.sub(edge2, vec3.fromValues(position[6], position[7], position[8]), vec3.fromValues(position[0], position[1], position[2]));
        vec2.sub(deltaUV1, vec2.fromValues(texcoord[2], texcoord[3]), vec2.fromValues(texcoord[0], texcoord[1]));
        vec2.sub(deltaUV2, vec2.fromValues(texcoord[4], texcoord[5]), vec2.fromValues(texcoord[0], texcoord[1]));

        // console.log(vec3.fromValues(position[3], position[4], position[5]));
        // console.log(deltaUV1[0]);

        let f = 1.0 / (deltaUV1[0] * deltaUV2[1] - deltaUV2[0] * deltaUV1[1]);

        tangent1[0] = f * (deltaUV2[1] * edge1[0] - deltaUV1[1] * edge2[0]);
        tangent1[1] = f * (deltaUV2[1] * edge1[1] - deltaUV1[1] * edge2[1]);
        tangent1[2] = f * (deltaUV2[1] * edge1[2] - deltaUV1[1] * edge2[2]);

        bitangent1[0] = f * (-deltaUV2[0] * edge1[0] + deltaUV1[0] * edge2[0]);
        bitangent1[1] = f * (-deltaUV2[0] * edge1[1] + deltaUV1[0] * edge2[1]);
        bitangent1[2] = f * (-deltaUV2[0] * edge1[2] + deltaUV1[0] * edge2[2]);
        
        // triangle 2
        vec3.sub(edge1, vec3.fromValues(position[6], position[7], position[8]), vec3.fromValues(position[0], position[1], position[2]));
        vec3.sub(edge2, vec3.fromValues(position[9], position[10], position[11]), vec3.fromValues(position[0], position[1], position[2]));
        vec2.sub(deltaUV1, vec2.fromValues(texcoord[4], texcoord[5]), vec2.fromValues(texcoord[0], texcoord[1]));
        vec2.sub(deltaUV2, vec2.fromValues(texcoord[6], texcoord[7]), vec2.fromValues(texcoord[0], texcoord[1]));

        f = 1.0 / (deltaUV1[0] * deltaUV2[1] - deltaUV2[0] * deltaUV1[1]);

        tangent2[0] = f * (deltaUV2[1] * edge1[0] - deltaUV1[1] * edge2[0]);
        tangent2[1] = f * (deltaUV2[1] * edge1[1] - deltaUV1[1] * edge2[1]);
        tangent2[2] = f * (deltaUV2[1] * edge1[2] - deltaUV1[1] * edge2[2]);

        bitangent2[0] = f * (-deltaUV2[0] * edge1[0] + deltaUV1[1] * edge2[0]);
        bitangent2[1] = f * (-deltaUV2[0] * edge1[1] + deltaUV1[1] * edge2[1]);
        bitangent2[2] = f * (-deltaUV2[0] * edge1[2] + deltaUV1[1] * edge2[2]);

        let quad_position = new Float32Array([
            position[0], position[1], position[2],
            position[3], position[4], position[5],
            position[6], position[7], position[8],
    
            position[0], position[1], position[2],
            position[6], position[7], position[8],
            position[9], position[10], position[11]	
        ]);          	

        let quad_normals = new Float32Array([
            normals[0], normals[1], normals[2],
            normals[0], normals[1], normals[2],
            normals[0], normals[1], normals[2],
    
            normals[0], normals[1], normals[2],
            normals[0], normals[1], normals[2],
            normals[0], normals[1], normals[2]
        ]);

        let quad_texcoords = new Float32Array([
            texcoord[0], texcoord[1],
            texcoord[2], texcoord[3],
            texcoord[4], texcoord[5],
    
            texcoord[0], texcoord[1],
            texcoord[4], texcoord[5],
            texcoord[6], texcoord[7],
        ]);

        let quad_tangent = new Float32Array([
            tangent1[0], tangent1[1], tangent1[2],
            tangent1[0], tangent1[1], tangent1[2],
            tangent1[0], tangent1[1], tangent1[2],
    
            tangent2[0], tangent2[1], tangent2[2],
            tangent2[0], tangent2[1], tangent2[2],
            tangent2[0], tangent2[1], tangent2[2]
        ]);

        let quad_bitangent = new Float32Array([
            bitangent1[0], bitangent1[1], bitangent1[2],
            bitangent1[0], bitangent1[1], bitangent1[2],
            bitangent1[0], bitangent1[1], bitangent1[2],
    
            bitangent2[0], bitangent2[1], bitangent2[2],
            bitangent2[0], bitangent2[1], bitangent2[2],
            bitangent2[0], bitangent2[1], bitangent2[2]
        ]);

        // console.log(quad_position);
        // console.log(quad_bitangent);

        // console.log(bitangent1);
        // console.log(bitangent2);
        
        this.vao_quad_n = gl.createVertexArray();
        gl.bindVertexArray(this.vao_quad_n);
        {
            this.vbo_quad_position_n = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_quad_position_n);
            gl.bufferData(gl.ARRAY_BUFFER, quad_position, gl.STATIC_DRAW);
            gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
            gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);

            this.vbo_quad_texcoord_n = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_quad_texcoord_n);
            gl.bufferData(gl.ARRAY_BUFFER, quad_texcoords, gl.STATIC_DRAW);
            gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS, 2, gl.FLOAT, false, 0, 0);
            gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);

            this.vbo_quad_normal_n = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_quad_normal_n);
            gl.bufferData(gl.ARRAY_BUFFER, quad_normals, gl.STATIC_DRAW);
            gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
            gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_NORMAL);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);

            this.vbo_quad_tangent_n = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_quad_tangent_n);
            gl.bufferData(gl.ARRAY_BUFFER, quad_tangent, gl.STATIC_DRAW);
            gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_TANGENTS, 3, gl.FLOAT, false, 0, 0);
            gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_TANGENTS);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);

            this.vbo_quad_bitangent_n = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_quad_bitangent_n);
            gl.bufferData(gl.ARRAY_BUFFER, quad_bitangent, gl.STATIC_DRAW);
            gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_BITANGENTS, 3, gl.FLOAT, false, 0, 0);
            gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_BITANGENTS);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);
        }
        gl.bindVertexArray(null);
    }

    initQuadForNormalMapping()
    {
        //================= Quad ==================//
        let position = new Float32Array([
            1.0, 1.0, 0.0,
            -1.0, 1.0, 0.0,
            -1.0, -1.0, 0.0,
            1.0, -1.0, 0.0]);

        let texcoord = new Float32Array([
            1.0, 1.0,
            0.0, 1.0,
            0.0, 0.0,
            1.0, 0.0]);

        let normals = new Float32Array([
            0.0, 0.0, 1.0
        ])

        // calculate tangent / bitangent vectors of both triangles
        var tangent1 = vec3.create();
        var bitangent1 = vec3.create();
        var tangent2 = vec3.create();
        var bitangent2 = vec3.create();

        // triangle 1
        var edge1 = vec3.create();
        var edge2 = vec3.create();
        var deltaUV1 = vec2.create();
        var deltaUV2 = vec2.create();
        vec3.sub(edge1, vec3.fromValues(position[3], position[4], position[5]), vec3.fromValues(position[0], position[1], position[2]));
        vec3.sub(edge2, vec3.fromValues(position[6], position[7], position[8]), vec3.fromValues(position[0], position[1], position[2]));
        vec2.sub(deltaUV1, vec2.fromValues(texcoord[2], texcoord[3]), vec2.fromValues(texcoord[0], texcoord[1]));
        vec2.sub(deltaUV2, vec2.fromValues(texcoord[4], texcoord[5]), vec2.fromValues(texcoord[0], texcoord[1]));

        // console.log(vec3.fromValues(position[3], position[4], position[5]));
        // console.log(deltaUV1[0]);

        let f = 1.0 / (deltaUV1[0] * deltaUV2[1] - deltaUV2[0] * deltaUV1[1]);

        tangent1[0] = f * (deltaUV2[1] * edge1[0] - deltaUV1[1] * edge2[0]);
        tangent1[1] = f * (deltaUV2[1] * edge1[1] - deltaUV1[1] * edge2[1]);
        tangent1[2] = f * (deltaUV2[1] * edge1[2] - deltaUV1[1] * edge2[2]);

        bitangent1[0] = f * (-deltaUV2[0] * edge1[0] + deltaUV1[0] * edge2[0]);
        bitangent1[1] = f * (-deltaUV2[0] * edge1[1] + deltaUV1[0] * edge2[1]);
        bitangent1[2] = f * (-deltaUV2[0] * edge1[2] + deltaUV1[0] * edge2[2]);
        
        // triangle 2
        vec3.sub(edge1, vec3.fromValues(position[6], position[7], position[8]), vec3.fromValues(position[0], position[1], position[2]));
        vec3.sub(edge2, vec3.fromValues(position[9], position[10], position[11]), vec3.fromValues(position[0], position[1], position[2]));
        vec2.sub(deltaUV1, vec2.fromValues(texcoord[4], texcoord[5]), vec2.fromValues(texcoord[0], texcoord[1]));
        vec2.sub(deltaUV2, vec2.fromValues(texcoord[6], texcoord[7]), vec2.fromValues(texcoord[0], texcoord[1]));

        f = 1.0 / (deltaUV1[0] * deltaUV2[1] - deltaUV2[0] * deltaUV1[1]);

        tangent2[0] = f * (deltaUV2[1] * edge1[0] - deltaUV1[1] * edge2[0]);
        tangent2[1] = f * (deltaUV2[1] * edge1[1] - deltaUV1[1] * edge2[1]);
        tangent2[2] = f * (deltaUV2[1] * edge1[2] - deltaUV1[1] * edge2[2]);

        bitangent2[0] = f * (-deltaUV2[0] * edge1[0] + deltaUV1[1] * edge2[0]);
        bitangent2[1] = f * (-deltaUV2[0] * edge1[1] + deltaUV1[1] * edge2[1]);
        bitangent2[2] = f * (-deltaUV2[0] * edge1[2] + deltaUV1[1] * edge2[2]);

        let quad_position = new Float32Array([
            position[0], position[1], position[2],
            position[3], position[4], position[5],
            position[6], position[7], position[8],
    
            position[0], position[1], position[2],
            position[6], position[7], position[8],
            position[9], position[10], position[11]	
        ]);          	

        let quad_normals = new Float32Array([
            normals[0], normals[1], normals[2],
            normals[0], normals[1], normals[2],
            normals[0], normals[1], normals[2],
    
            normals[0], normals[1], normals[2],
            normals[0], normals[1], normals[2],
            normals[0], normals[1], normals[2]
        ]);

        let quad_texcoords = new Float32Array([
            texcoord[0], texcoord[1],
            texcoord[2], texcoord[3],
            texcoord[4], texcoord[5],
    
            texcoord[0], texcoord[1],
            texcoord[4], texcoord[5],
            texcoord[6], texcoord[7],
        ]);

        let quad_tangent = new Float32Array([
            tangent1[0], tangent1[1], tangent1[2],
            tangent1[0], tangent1[1], tangent1[2],
            tangent1[0], tangent1[1], tangent1[2],
    
            tangent2[0], tangent2[1], tangent2[2],
            tangent2[0], tangent2[1], tangent2[2],
            tangent2[0], tangent2[1], tangent2[2]
        ]);

        let quad_bitangent = new Float32Array([
            bitangent1[0], bitangent1[1], bitangent1[2],
            bitangent1[0], bitangent1[1], bitangent1[2],
            bitangent1[0], bitangent1[1], bitangent1[2],
    
            bitangent2[0], bitangent2[1], bitangent2[2],
            bitangent2[0], bitangent2[1], bitangent2[2],
            bitangent2[0], bitangent2[1], bitangent2[2]
        ]);

        console.log(quad_position);
        // console.log(quad_bitangent);

        // console.log(bitangent1);
        // console.log(bitangent2);
        
        this.vao_quad_n = gl.createVertexArray();
        gl.bindVertexArray(this.vao_quad);
        {
            this.vbo_quad_position_n = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_quad_position_n);
            gl.bufferData(gl.ARRAY_BUFFER, position, gl.STATIC_DRAW);
            gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
            gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);

            // this.vbo_quad_texcoord_n = gl.createBuffer();
            // gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_quad_texcoord_n);
            // gl.bufferData(gl.ARRAY_BUFFER, quad_texcoords, gl.STATIC_DRAW);
            // gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS, 2, gl.FLOAT, false, 0, 0);
            // gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS);
            // gl.bindBuffer(gl.ARRAY_BUFFER, null);

            // this.vbo_quad_normal_n = gl.createBuffer();
            // gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_quad_normal_n);
            // gl.bufferData(gl.ARRAY_BUFFER, quad_normals, gl.STATIC_DRAW);
            // gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
            // gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_NORMAL);
            // gl.bindBuffer(gl.ARRAY_BUFFER, null);

            // this.vbo_quad_tangent_n = gl.createBuffer();
            // gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_quad_tangent_n);
            // gl.bufferData(gl.ARRAY_BUFFER, quad_tangent, gl.STATIC_DRAW);
            // gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_TANGENTS, 3, gl.FLOAT, false, 0, 0);
            // gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_TANGENTS);
            // gl.bindBuffer(gl.ARRAY_BUFFER, null);

            // this.vbo_quad_bitangent_n = gl.createBuffer();
            // gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_quad_bitangent_n);
            // gl.bufferData(gl.ARRAY_BUFFER, quad_bitangent, gl.STATIC_DRAW);
            // gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_BITANGENTS, 3, gl.FLOAT, false, 0, 0);
            // gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_BITANGENTS);
            // gl.bindBuffer(gl.ARRAY_BUFFER, null);
        }
        gl.bindVertexArray(null);
    }

    drawCube()
    {
        gl.bindVertexArray(this.vao_cube);
        gl.drawArrays(gl.TRIANGLE_FAN, 0, 24);
        gl.bindVertexArray(null);
    }

    drawQuad()
    {
        gl.bindVertexArray(this.vao_quad);
        gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
        gl.bindVertexArray(null);
    }

    drawQuadNormalMapped()
    {
        gl.bindVertexArray(this.vao_quad_n);
        gl.drawArrays(gl.TRIANGLES, 0, 6);
        gl.bindVertexArray(null);
    }

    uninitializeCube()
    {
        if (this.vbo_cube_position)
        {
            gl.deleteBuffer(this.vbo_cube_position);
            this.vbo_cube_position = null;
        }
    
        if (this.vbo_cube_texcoord)
        {
            gl.deleteBuffer(this.vbo_cube_texcoord);
            this.vbo_cube_texcoord = null;
        }
    
        if (this.vao_cube)
        {
            gl.deleteVertexArray(this.vao_cube);
            this.vao_cube = null;
        }
    }
};