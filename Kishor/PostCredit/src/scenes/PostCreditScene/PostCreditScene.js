class PostCredits
{

    /*
        Scene outline : freedom fighter's wife sitting , facing to the wall and small lamp (panti) is glowing in the near corner
        Objects : 1.freedom fighter's wife
                  2.panti
                  3.house
                  4.wobble effect for flame
                  5.point light on flame
                  6.

    */
    constructor()
    {
        this.nmap = new NormalMapping();
        this.modelPlacer = new ModelPlacer();
        // this.photoFrame = new Triangle();

        this.quad;
        // this.texShader = new TextureShader();

        this.textureWall;
        this.textureWall_normal;

        this.textureRoof;
        this.textureRoof_normal;

        this.textureDoor;
        this.textureDoor_normal;

        this.textureFrame;

        this.time = 0.0;
    }

    async initialize()
    {
        this.nmap.initialize();

        this.textureWall = loadTextureFromFile('./assets/textures/house/floor.jpg');
        this.textureWall_normal = loadTextureFromFile('./assets/textures/house/wall_normal.png');

        this.texture = loadTextureFromFile('./assets/textures/brickwall.jpg');
        this.texture_nmap = loadTextureFromFile('./assets/textures/brickwall_normal.jpg');

        this.textureDoor = loadTextureFromFile('./assets/textures/house/wallhut.png');
        this.textureDoor_normal = loadTextureFromFile('./assets/textures/house/wallhut_normal.png');
        
        this.texture_flame = loadTextureFromFile('./assets/textures/flame/flame3.png');
        this.textureFrame = loadTextureFromFile('./assets/textures/house/Sir.png');

        // For Wobble
        this.quad = new Shapes();
        this.wobbleTex = new WobbleShader();
        this.wobbleTex.initialize();

        // for photoFrame
        this.photoFrame = new Triangle();
        this.photoFrame.initialize();
    }

    display(cam)
    {
        // draw room
        this.room(cam);
        // Door ================================================================================
        // this.door(cam);

        // updating
        this.time += 0.1;
    }

    room(cam)
    {
        var modelMatrix = mat4.create();
        var viewMatrix = mat4.create();
        var translationMatrix = mat4.create();
        var rotationMatrixY = mat4.create();
        var scaleMatrix = mat4.create();

        // back Wall =============================================================================
        this.backWall(cam);

        // Front Wall ============================================================================
        this.frontWall(cam);

        // right side wall ===========================================================================
        mat4.identity(translationMatrix);
        mat4.identity(rotationMatrixY);
        mat4.identity(scaleMatrix);
        mat4.identity(modelMatrix);

        // mat4.translate(translationMatrix, translationMatrix , [1.2 ,0.0 ,1.0]);
        mat4.translate(translationMatrix, translationMatrix , [3.6 ,0.0 ,2.0]);
        mat4.rotate(rotationMatrixY, rotationMatrixY,(Math.PI * -90.0) / 180.0, [0.0, 1.0, 0.0]);
        // mat4.scale(scaleMatrix, scaleMatrix, [2 ,1 ,1]);
        mat4.scale(scaleMatrix, scaleMatrix, [3 ,1 ,1]);

        mat4.multiply(modelMatrix, modelMatrix, translationMatrix);
        mat4.multiply(modelMatrix, modelMatrix, rotationMatrixY);
        mat4.multiply(modelMatrix, modelMatrix, scaleMatrix);
        // modelMatrix = this.modelPlacer.getModelMatrix();
        // this.nmap.display(modelMatrix, cam, this.texture, this.texture_nmap, 2.0);
        // let tileingArray = [2.5, 2.5, 2.5, 2.5, 0.4, 2.0, 0.4, 2.0];
        let tileingArray = [3.5, 2.5, 3.5, 2.5, 0.4, 2.0, 0.4, 2.0];
        this.nmap.displayCube(modelMatrix, cam, this.texture, this.texture_nmap, tileingArray);

        // middle wall ===========================================================================
        mat4.identity(translationMatrix);
        mat4.identity(rotationMatrixY);
        mat4.identity(scaleMatrix);
        mat4.identity(modelMatrix);

        mat4.translate(translationMatrix, translationMatrix , [-1.2 ,0.0 ,1.0]);
        mat4.rotate(rotationMatrixY, rotationMatrixY,(Math.PI * 90.0) / 180.0, [0.0, 1.0, 0.0]);
        mat4.scale(scaleMatrix, scaleMatrix, [2 ,1 ,1]);

        mat4.multiply(modelMatrix, modelMatrix, translationMatrix);
        mat4.multiply(modelMatrix, modelMatrix, rotationMatrixY);
        mat4.multiply(modelMatrix, modelMatrix, scaleMatrix);
        // modelMatrix = this.modelPlacer.getModelMatrix();
        // this.nmap.display(modelMatrix, cam, this.texture, this.texture_nmap, 2.0);
        tileingArray = [2.5, 2.5, 2.5, 2.5, 0.4, 2.0, 0.4, 2.0];
        this.nmap.displayCube(modelMatrix, cam, this.texture, this.texture_nmap, tileingArray);

        // left side wall ===========================================================================
        mat4.identity(translationMatrix);
        mat4.identity(rotationMatrixY);
        mat4.identity(scaleMatrix);
        mat4.identity(modelMatrix);

        mat4.translate(translationMatrix, translationMatrix , [-3.6 ,0.0 ,1.0]);
        mat4.rotate(rotationMatrixY, rotationMatrixY,(Math.PI * 90.0) / 180.0, [0.0, 1.0, 0.0]);
        mat4.scale(scaleMatrix, scaleMatrix, [2 ,1 ,1]);

        mat4.multiply(modelMatrix, modelMatrix, translationMatrix);
        mat4.multiply(modelMatrix, modelMatrix, rotationMatrixY);
        mat4.multiply(modelMatrix, modelMatrix, scaleMatrix);
        // modelMatrix = this.modelPlacer.getModelMatrix();
        // this.nmap.display(modelMatrix, cam, this.texture, this.texture_nmap, 2.0);
        tileingArray = [2.5, 2.5, 2.5, 2.5, 0.4, 2.0, 0.4, 2.0];
        this.nmap.displayCube(modelMatrix, cam, this.texture, this.texture_nmap, tileingArray);

        // floor ===========================================================================
        mat4.identity(translationMatrix);
        mat4.identity(rotationMatrixY);
        mat4.identity(scaleMatrix);
        mat4.identity(modelMatrix);

        mat4.translate(translationMatrix, translationMatrix , [0 ,-1 ,1]);
        mat4.rotate(rotationMatrixY, rotationMatrixY,(Math.PI * -90.0) / 180.0, [1.0, 0.0, 0.0]);
        // mat4.scale(scaleMatrix, scaleMatrix, [1.3 ,2 ,5.3]);
        mat4.scale(scaleMatrix, scaleMatrix, [5.3 ,5.3 ,5.3]);

        mat4.multiply(modelMatrix, modelMatrix, translationMatrix);
        mat4.multiply(modelMatrix, modelMatrix, rotationMatrixY);
        mat4.multiply(modelMatrix, modelMatrix, scaleMatrix);
        // modelMatrix = this.modelPlacer.getModelMatrix();
        this.nmap.display(modelMatrix, cam, this.textureWall, this.textureWall_normal, 5.3, 0.8);

        // Door ================================================================================
        this.door(cam);

        // photo frame
        this.drawPhotoFrame(cam);

        // Panti flame =========================================================================
        this.wobbleFlame(cam);
    }

    backWall(cam)
    {
        // code
        var modelMatrix = mat4.create();
        var viewMatrix = mat4.create();
        var translationMatrix = mat4.create();
        var rotationMatrixY = mat4.create();
        var scaleMatrix = mat4.create();

        

        // back wall 1 ===========================================================================
        mat4.translate(translationMatrix, translationMatrix , [0.0 ,0.0 ,-1.0]);
        mat4.rotate(rotationMatrixY, rotationMatrixY,(Math.PI * -90.0) / 180.0, [0.0, 1.0, 0.0]);
        mat4.scale(scaleMatrix, scaleMatrix, [1.2 ,1 ,1]);

        mat4.multiply(modelMatrix, modelMatrix, translationMatrix);
        // mat4.multiply(modelMatrix, modelMatrix, rotationMatrixY);
        mat4.multiply(modelMatrix, modelMatrix, scaleMatrix);
        // modelMatrix = this.modelPlacer.getModelMatrix();
        this.nmap.display(modelMatrix, cam, this.texture, this.texture_nmap, 1.5, 0.8);

        // back wall 2 - left ===========================================================================
        mat4.identity(translationMatrix);
        mat4.identity(rotationMatrixY);
        mat4.identity(scaleMatrix);
        mat4.identity(modelMatrix);

        mat4.translate(translationMatrix, translationMatrix , [-2.4 ,0.0 ,-1.0]);
        mat4.rotate(rotationMatrixY, rotationMatrixY,(Math.PI * -90.0) / 180.0, [0.0, 1.0, 0.0]);
        mat4.scale(scaleMatrix, scaleMatrix, [1.2 ,1 ,1]);

        mat4.multiply(modelMatrix, modelMatrix, translationMatrix);
        // mat4.multiply(modelMatrix, modelMatrix, rotationMatrixY);
        mat4.multiply(modelMatrix, modelMatrix, scaleMatrix);
        // modelMatrix = this.modelPlacer.getModelMatrix();
        this.nmap.display(modelMatrix, cam, this.texture, this.texture_nmap, 1.5, 0.8);

        // back wall 3 - right ===========================================================================
        mat4.identity(translationMatrix);
        mat4.identity(rotationMatrixY);
        mat4.identity(scaleMatrix);
        mat4.identity(modelMatrix);

        mat4.translate(translationMatrix, translationMatrix , [2.4 ,0.0 ,-1.0]);
        mat4.rotate(rotationMatrixY, rotationMatrixY,(Math.PI * -90.0) / 180.0, [0.0, 1.0, 0.0]);
        mat4.scale(scaleMatrix, scaleMatrix, [1.2 ,1 ,1]);

        mat4.multiply(modelMatrix, modelMatrix, translationMatrix);
        // mat4.multiply(modelMatrix, modelMatrix, rotationMatrixY);
        mat4.multiply(modelMatrix, modelMatrix, scaleMatrix);
        // modelMatrix = this.modelPlacer.getModelMatrix();
        this.nmap.display(modelMatrix, cam, this.texture, this.texture_nmap, 1.5, 0.8);
    }

    frontWall(cam)
    {
        // code
        var modelMatrix = mat4.create();
        var viewMatrix = mat4.create();
        var translationMatrix = mat4.create();
        var rotationMatrixY = mat4.create();
        var scaleMatrix = mat4.create();

        

        // back wall 1 ===========================================================================
        mat4.translate(translationMatrix, translationMatrix , [0.0 ,0.0 ,5.0]);
        mat4.rotate(rotationMatrixY, rotationMatrixY,(Math.PI * -90.0) / 180.0, [0.0, 1.0, 0.0]);
        mat4.scale(scaleMatrix, scaleMatrix, [1.2 ,-1 ,1]);

        mat4.multiply(modelMatrix, modelMatrix, translationMatrix);
        // mat4.multiply(modelMatrix, modelMatrix, rotationMatrixY);
        mat4.multiply(modelMatrix, modelMatrix, scaleMatrix);
        // modelMatrix = this.modelPlacer.getModelMatrix();
        this.nmap.display(modelMatrix, cam, this.texture, this.texture_nmap, 1.5, 0.8);

        // back wall 2 - left ===========================================================================
        mat4.identity(translationMatrix);
        mat4.identity(rotationMatrixY);
        mat4.identity(scaleMatrix);
        mat4.identity(modelMatrix);

        mat4.translate(translationMatrix, translationMatrix , [-2.4 ,0.0 ,5.0]);
        mat4.rotate(rotationMatrixY, rotationMatrixY,(Math.PI * -90.0) / 180.0, [0.0, 1.0, 0.0]);
        mat4.scale(scaleMatrix, scaleMatrix, [1.2 ,-1 ,1]);

        mat4.multiply(modelMatrix, modelMatrix, translationMatrix);
        // mat4.multiply(modelMatrix, modelMatrix, rotationMatrixY);
        mat4.multiply(modelMatrix, modelMatrix, scaleMatrix);
        // modelMatrix = this.modelPlacer.getModelMatrix();
        this.nmap.display(modelMatrix, cam, this.texture, this.texture_nmap, 1.5, 0.8);

        // back wall 3 - right ===========================================================================
        mat4.identity(translationMatrix);
        mat4.identity(rotationMatrixY);
        mat4.identity(scaleMatrix);
        mat4.identity(modelMatrix);

        mat4.translate(translationMatrix, translationMatrix , [2.4 ,0.0 ,5.0]);
        mat4.rotate(rotationMatrixY, rotationMatrixY,(Math.PI * -90.0) / 180.0, [0.0, 1.0, 0.0]);
        mat4.scale(scaleMatrix, scaleMatrix, [1.2 ,-1 ,1]);

        mat4.multiply(modelMatrix, modelMatrix, translationMatrix);
        // mat4.multiply(modelMatrix, modelMatrix, rotationMatrixY);
        mat4.multiply(modelMatrix, modelMatrix, scaleMatrix);
        // modelMatrix = this.modelPlacer.getModelMatrix();
        this.nmap.display(modelMatrix, cam, this.texture, this.texture_nmap, 1.5, 0.8);
    }

    wobbleFlame(cam)
    {
        gl.useProgram(this.wobbleTex.getShaderProgramObject());

        var modelMatrix = mat4.create();
        var viewMatrix = mat4.create();
        var scaleMatrix = mat4.create();
        var translationMatrix = mat4.create();

        mat4.translate(translationMatrix, translationMatrix , [0.6 ,-0.8 ,-0.7]);
        mat4.multiply(modelMatrix, modelMatrix, translationMatrix);

        mat4.scale(scaleMatrix, scaleMatrix, [0.03, 0.03 ,1]);
        mat4.multiply(modelMatrix, modelMatrix, scaleMatrix);
        
        // modelMatrix = this.modelPlacer.getModelMatrix();
        viewMatrix = cam.getViewMatrix();
        
        // mat4.multiply(modelViewMatrix, modelMatrix, viewMatrix);
        // mat4.multiply(modelViewProjection, perspectiveProjectionMatrix, modelViewMatrix);

        gl.uniformMatrix4fv(this.wobbleTex.modelMatrixUniform, false, modelMatrix);
        gl.uniformMatrix4fv(this.wobbleTex.viewMatrixUniform, false, viewMatrix);
        gl.uniformMatrix4fv(this.wobbleTex.projectionMatrixUniform, false, perspectiveProjectionMatrix);

        gl.uniform3fv(this.wobbleTex.LightPositionUniform, [0.0, 0.0, 2.0]);

        gl.uniform1f(this.wobbleTex.StartRadUniform, degToRad(this.time));
        

        gl.uniform2f(this.wobbleTex.FreqUniform, 0.8, 1.64);
        gl.uniform2f(this.wobbleTex.AmplitudeUniform, .04, -.07);

        gl.enable(gl.BLEND);
        gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);

        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, this.texture_flame);
        gl.uniform1i(this.wobbleTex.WobbleTexSamplerUniform, 0);
        this.quad.drawQuad();
        gl.bindTexture(gl.TEXTURE_2D, null);

        gl.disable(gl.BLEND);

        gl.useProgram(null);

        // console.log(this.time);
    }

    door(cam)
    {
        // code
        var modelMatrix = mat4.create();
        var viewMatrix = mat4.create();
        var translationMatrix = mat4.create();
        var rotationMatrixY = mat4.create();
        var scaleMatrix = mat4.create();

        // Door 1 ===========================================================================
        mat4.translate(translationMatrix, translationMatrix , [-3.5 ,0.0 ,3.0]);
        // mat4.rotate(rotationMatrixY, rotationMatrixY,(Math.PI * -90.0) / 180.0, [0.0, 1.0, 0.0]);
        mat4.rotate(rotationMatrixY, rotationMatrixY,(Math.PI * osc(this.time, -90.0, 0.0, 0.05)) / 180.0, [0.0, 1.0, 0.0]);
        mat4.scale(scaleMatrix, scaleMatrix, [0.5 ,1 ,1]);

        mat4.multiply(modelMatrix, modelMatrix, translationMatrix);
        mat4.multiply(modelMatrix, modelMatrix, rotationMatrixY);
        
        mat4.identity(translationMatrix);
        mat4.translate(translationMatrix, translationMatrix , [0.5 ,0.0 ,0.0]);
        mat4.multiply(modelMatrix, modelMatrix, translationMatrix);
        mat4.multiply(modelMatrix, modelMatrix, scaleMatrix);
        // modelMatrix = this.modelPlacer.getModelMatrix();
        this.nmap.display(modelMatrix, cam, this.textureDoor, this.textureDoor_normal, 1.5, 0.8);

        // Door 2 ===========================================================================
        mat4.identity(translationMatrix);
        mat4.identity(rotationMatrixY);
        mat4.identity(scaleMatrix);
        mat4.identity(modelMatrix);

        mat4.translate(translationMatrix, translationMatrix , [-3.5 ,0.0 ,5.0]);
        // mat4.rotate(rotationMatrixY, rotationMatrixY,(Math.PI * -90.0) / 180.0, [0.0, 1.0, 0.0]);
        mat4.rotate(rotationMatrixY, rotationMatrixY,(Math.PI * osc(this.time, 90.0, 0.0, 0.05)) / 180.0, [0.0, 1.0, 0.0]);
        mat4.scale(scaleMatrix, scaleMatrix, [0.5 ,1 ,1]);

        mat4.multiply(modelMatrix, modelMatrix, translationMatrix);
        mat4.multiply(modelMatrix, modelMatrix, rotationMatrixY);
        
        mat4.identity(translationMatrix);
        mat4.translate(translationMatrix, translationMatrix , [0.5 ,0.0 ,0.0]);
        mat4.multiply(modelMatrix, modelMatrix, translationMatrix);
        mat4.multiply(modelMatrix, modelMatrix, scaleMatrix);
        // modelMatrix = this.modelPlacer.getModelMatrix();
        this.nmap.display(modelMatrix, cam, this.textureDoor, this.textureDoor_normal, 1.5, 0.8);
    }

    drawPhotoFrame(cam)
    {
        // code
        var modelMatrix = mat4.create();
        var viewMatrix = mat4.create();
        var translationMatrix = mat4.create();
        var rotationMatrixY = mat4.create();
        var scaleMatrix = mat4.create();

        // frame 1 ===========================================================================
        mat4.translate(translationMatrix, translationMatrix , [2.3 ,0.4 ,-1.0]);
        mat4.scale(scaleMatrix, scaleMatrix, [0.18 ,0.26 ,1]);

        mat4.multiply(modelMatrix, modelMatrix, translationMatrix);
        mat4.multiply(modelMatrix, modelMatrix, scaleMatrix);
        // modelMatrix = this.modelPlacer.getModelMatrix();
        this.photoFrame.displayTexture(modelMatrix, cam, this.textureFrame);
    }

    keyEventModel(key)
    {
        // this.modelShader.keyEvent(key);
        this.modelPlacer.getKeyForTransformation(key);
    }

    uninitialize()
    {

    }
};