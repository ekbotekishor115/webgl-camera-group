class ModelShader {

    constructor() {
        // Shaders
        this.shaderProgramObject = null;

        // Uniforms
        this.mvpMatrixUniform;
        this.modelMatrixUniform;
        this.viewMatrixUniform;
        this.projectionMatrixUniform;

        this.vertexShaderObject;
        this.fragmentShaderObject;

        this.vao;
        this.vbo_pos;
        this.vbo_normal;
        this.vbo_tangent;
        this.vbo_texcoord;
        this.vbo_color;

        this.lightPosition = [100.0, 100.0, 100.0, 1.0];

        this.cameraPosition = [0.0, 0.0, 0.0];
        this.up = [0.0, 1.0, 0.0];

        this.time = 0.1;
        this.parts_ = [];
        this.obj;

        // this.modelPlacer = new ModelPlacer();
    }

    async initialize(filePath) {
        // vertex shader
        var vertexShaderSourceCode =
            `#version 300 es
            \n
            in vec4 a_position;
            in vec3 a_normal;
            in vec3 a_tangent;
            in vec2 a_texcoord;
            in vec4 a_color;

            uniform mat4 u_projection;
            uniform mat4 u_view;
            uniform mat4 u_world;
            uniform vec3 u_viewWorldPosition;
            
            out vec3 v_normal;
            out vec3 v_tangent;
            out vec3 v_surfaceToView;
            out vec2 v_texcoord;
            out vec4 v_color;
            
            void main() {
            vec4 worldPosition = u_world * a_position;
            gl_Position = u_projection * u_view * worldPosition;
            v_surfaceToView = u_viewWorldPosition - worldPosition.xyz;
            mat3 normalMat = mat3(u_view * u_world);
            
            v_normal = normalize(normalMat * a_normal);
            v_tangent = normalize(normalMat * a_tangent);
            
            v_texcoord = a_texcoord;
            v_color = a_color;
            }`;


        // vertex shader object
        // this.vertexShaderObject = this.createShaderObjects(vertexShaderSourceCode, gl.VERTEX_SHADER);

        // fragment shader
        var fragmentShaderSourceCode =
            `#version 300 es
            \n
            precision highp float;
            in vec3 v_normal;
            in vec3 v_tangent;
            in vec3 v_surfaceToView;
            in vec2 v_texcoord;
            in vec4 v_color;

            uniform vec3 u_diffuse;
            uniform sampler2D diffuseMap;
            uniform vec3 ambient;
            uniform vec3 emissive;
            uniform vec3 specular;
            uniform sampler2D specularMap;
            uniform float shininess;
            uniform sampler2D normalMap;
            uniform float opacity;
            uniform vec3 u_lightDirection;
            uniform vec3 u_ambientLight;

            uniform int ifGodRay;
            
            out vec4 FragColor;
            
            void main () 
            {
                if (ifGodRay == 1)
                {
                    vec3 normal = normalize(v_normal) * ( float( gl_FrontFacing ) * 2.0 - 1.0 );
                    vec3 tangent = normalize(v_tangent) * ( float( gl_FrontFacing ) * 2.0 - 1.0 );
                    vec3 bitangent = normalize(cross(normal, tangent));
                    mat3 tbn = mat3(tangent, bitangent, normal);
                    normal = texture(normalMap, v_texcoord).rgb * 2. - 1.;
                    normal = normalize(tbn * normal);
                    vec3 surfaceToViewDirection = normalize(v_surfaceToView);
                    vec3 halfVector = normalize(u_lightDirection + surfaceToViewDirection);
                    float fakeLight = dot(u_lightDirection, normal) * .5 + .5;
                    float specularLight = clamp(dot(normal, halfVector), 0.0, 1.0);
                    vec4 specularMapColor = texture(specularMap, v_texcoord);
                    vec3 effectiveSpecular = specular * specularMapColor.rgb;
                    vec4 diffuseMapColor = texture(diffuseMap, v_texcoord);
                    vec3 effectiveDiffuse = u_diffuse * diffuseMapColor.rgb * v_color.rgb;
                    float effectiveOpacity = opacity * diffuseMapColor.a * v_color.a;
                    
                    FragColor = vec4(emissive +
                    ambient * u_ambientLight +
                    effectiveDiffuse * fakeLight +
                    effectiveSpecular * pow(specularLight, shininess),
                    effectiveOpacity);
                }
                else
                {
                    FragColor = vec4(0.0, 0.0, 0.0, 1.0);
                }

            }`;


        // fragment shader object
        // this.fragmentShaderObject = this.createShaderObjects(fragmentShaderSourceCode, gl.FRAGMENT_SHADER);

        // // Attach and Link shaderObjects
        // this.shaderProgramObject = attachAndLinkShaderObjects(
        //     [this.vertexShaderObject, this.fragmentShaderObject],
        //     [VertexAttributeEnum.CAM_ATTRIBUTE_POSITION, VertexAttributeEnum.CAM_ATTRIBUTE_NORMAL, VertexAttributeEnum.CAM_ATTRIBUTE_TANGENTS, VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS, VertexAttributeEnum.CAM_ATTRIBUTE_COLOR],
        //     ["aPosition", "a_normal", "a_tangent", "a_texcoord", "aColor"]
        // );
        // if (this.shaderProgramObject == null) {
        //     alert("ShaderProgramObject failed");
        // }



        // compiles and links the shaders, looks up attribute and uniform locations
        this.meshProgramInfo = webglUtils.createProgramInfo(gl, [vertexShaderSourceCode, fragmentShaderSourceCode]);

        // uniforms
        this.modelMatrixUniform = gl.getUniformLocation(this.meshProgramInfo.program, "u_world");
        this.viewMatrixUniform = gl.getUniformLocation(this.meshProgramInfo.program, "u_view");
        this.projectionMatrixUniform = gl.getUniformLocation(this.meshProgramInfo.program, "u_projection");
        this.viewWorldUniform = gl.getUniformLocation(this.meshProgramInfo.program, "u_viewWorldPosition");
        this.lightDirUniform = gl.getUniformLocation(this.meshProgramInfo.program, "u_lightDirection");

        this.diffuseUniform = gl.getUniformLocation(this.meshProgramInfo.program, "u_diffuse");
        this.diffuseMapUniform = gl.getUniformLocation(this.meshProgramInfo.program, "diffuseMap");
        this.ambienteUniform = gl.getUniformLocation(this.meshProgramInfo.program, "ambient");
        this.emissiveUniform = gl.getUniformLocation(this.meshProgramInfo.program, "emissive");
        this.specularUniform = gl.getUniformLocation(this.meshProgramInfo.program, "specular");
        this.specularMapUniform = gl.getUniformLocation(this.meshProgramInfo.program, "specularMap");
        this.shininessUniform = gl.getUniformLocation(this.meshProgramInfo.program, "shininess");
        this.normalMapUniform = gl.getUniformLocation(this.meshProgramInfo.program, "normalMap");
        this.opacityUniform = gl.getUniformLocation(this.meshProgramInfo.program, "opacity");
        this.ambientLightUniform = gl.getUniformLocation(this.meshProgramInfo.program, "u_ambientLight");

        // for god rays
        this.godrayToggle = gl.getUniformLocation(this.meshProgramInfo.program, "ifGodRay");

        // const objHref = 'Assets/windmill2/windmill.obj';
        // initialize(objHref);

        const response = await fetch(filePath);
        const text = await response.text();
        this.obj = this.parseOBJ(text);

        // *********** for Material *********** //
        const baseHref = new URL(filePath, window.location.href);
        const matTexts = await Promise.all(this.obj.materialLibs.map(async filename => {
            const matHref = new URL(filename, baseHref).href;
            const response = await fetch(matHref);
            return await response.text();
        }));
        this.materials = this.parseMTL(matTexts.join('\n'));

        const textures = {
            defaultWhite: this.create1PixelTexture(gl, [255, 255, 255, 255]),
            defaultNormal: this.create1PixelTexture(gl, [127, 127, 255, 0]),
        };

        // load texture for materials
        for (const material of Object.values(this.materials)) {
            Object.entries(material)
                .filter(([key]) => key.endsWith('Map'))
                .forEach(([key, filename]) => {
                    let texture = textures[filename];
                    if (!texture) {
                        const textureHref = new URL(filename, baseHref).href;
                        texture = this.createTexture(gl, textureHref);
                        textures[filename] = texture;
                    }
                    material[key] = texture;
                });
        }

        // hack the materials so we can see the specular map
        Object.values(this.materials).forEach(m => {
            m.shininess = 25;
            m.specular = [3, 2, 1];
        });

        const defaultMaterial = {
            diffuse: [1, 1, 1],
            diffuseMap: textures.defaultWhite,
            normalMap: textures.defaultNormal,
            ambient: [0, 0, 0],
            specular: [1, 1, 1],
            specularMap: textures.defaultWhite,
            shininess: 400,
            opacity: 1,
        };

        // ***************** Model data *******************// 
        this.parts_ = this.obj.geometries.map(({ material, data }) => {
            if (data.color) {
                if (data.position.length === data.color.length) {
                    // it's 3. The our helper library assumes 4 so we need
                    // to tell it there are only 3.
                    data.color = { numComponents: 3, data: data.color };
                }
            } else {
                // there are no vertex colors so just use constant white
                data.color = { value: [1, 1, 1, 1] };
            }

            // generate tangents if we have the data to do so.
            if (data.texcoord && data.normal) {
                data.tangent = this.generateTangents_(data.position, data.texcoord);
            } else {
                // There are no tangents
                data.tangent = { value: [1, 0, 0] };
            }

            if (!data.texcoord) {
                data.texcoord = { value: [0, 0] };
            }

            if (!data.normal) {
                // we probably want to generate normals if there are none
                data.normal = { value: [0, 0, 1] };
            }

            // console.log(data);
            // create a buffer for each array by calling
            // gl.createBuffer, gl.bindBuffer, gl.bufferData
            const bufferInfo = webglUtils.createBufferInfoFromArrays(gl, data);

            this.vao = gl.createVertexArray();
            gl.bindVertexArray(this.vao);

            // vbo pos
            this.vbo_pos = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_pos);
            gl.bufferData(gl.ARRAY_BUFFER, data.position, gl.STATIC_DRAW);
            gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
            gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);

            // vbo normal
            this.vbo_normal = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_normal);
            gl.bufferData(gl.ARRAY_BUFFER, data.normal, gl.STATIC_DRAW);
            gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
            gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_NORMAL);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);

            // vbo tangent
            this.vbo_tangent = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_tangent);
            gl.bufferData(gl.ARRAY_BUFFER, data.tangent, gl.STATIC_DRAW);
            gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_TANGENTS, 3, gl.FLOAT, false, 0, 0);
            gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_TANGENTS);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);

            // vbo texcoord
            this.vbo_texcoord = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_texcoord);
            gl.bufferData(gl.ARRAY_BUFFER, data.texcoord, gl.STATIC_DRAW);
            gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS, 2, gl.FLOAT, false, 0, 0);
            gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);

            // vbo color
            this.vbo_color = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_color);
            gl.bufferData(gl.ARRAY_BUFFER, data.color, gl.STATIC_DRAW);
            gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
            gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_COLOR);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);

            this.vbo_element = gl.createBuffer();
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.vbo_element);
            gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, data.numElements, gl.STATIC_DRAW);
            // gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_COLOR, 3, gl.FLOAT, false, 0, 0);
            // gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_COLOR);
            gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

            gl.bindVertexArray(null);

            // console.log(this.vbo_pos);

            return {
                material: {
                    ...defaultMaterial,
                    ...this.materials[material],
                },
                bufferInfo,
                // vao,
            };
        });

        // console.log(this.parts.bufferInfo);
        // console.log(obj.geometries);



    }

    renderModel(modelMat, cam, DoGodray) {
        this.time += 0.003;  // convert to seconds
        var modelMatrix = mat4.create();
        var viewMatrix = mat4.create();
        // var camera = mat4.create();
        // var cameraTarget = [0, 0, 0];
        // mat4.lookAt(camera, this.cameraPosition, cameraTarget, this.up);
        inverse(viewMatrix, cam.getViewMatrix());

        let lightDir = [-1.0, 3.0, 4.0];

        let u_rotate = mat4.create();
        mat4.rotate(u_rotate, u_rotate, this.time, [0.0, 1.0, 0.0]);
        mat4.multiply(modelMatrix, modelMatrix, u_rotate);

        var modelViewMatrix = mat4.create();
        mat4.multiply(modelViewMatrix, modelViewMatrix, modelMatrix);
        // mat4.multiply(modelViewMatrix, modelViewMatrix, this.modelPlacer.getModelMatrix());
        mat4.multiply(modelViewMatrix, modelViewMatrix, viewMatrix);

        gl.useProgram(this.meshProgramInfo.program);

        // gl.enable(gl.BLEND);
        // gl.blendFunc(gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
        // calls gl.uniform
        // webglUtils.setUniforms(this.meshProgramInfo, sharedUniforms);



        gl.uniformMatrix4fv(this.viewMatrixUniform, false, cam.getViewMatrix());
        gl.uniformMatrix4fv(this.projectionMatrixUniform, false, perspectiveProjectionMatrix);
        gl.uniform3fv(this.lightDirUniform, lightDir);
        gl.uniform3fv(this.viewWorldUniform, cam.getEye());

        gl.uniform1i(this.godrayToggle, DoGodray);

        gl.bindVertexArray(this.vao);
        for (const { bufferInfo, material } of this.parts_) {
            // calls gl.bindBuffer, gl.enableVertexAttribArray, gl.vertexAttribPointer
            // console.log(vao);
            // console.log(bufferInfo);
            webglUtils.setBuffersAndAttributes(gl, this.meshProgramInfo, bufferInfo);
            // gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.vbo_element);
            // gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, bufferInfo.numElements, gl.STATIC_DRAW);
            
            // calls gl.uniform
            // gl.uniformMatrix4fv(this.modelMatrixUniform, false, modelMatrix);
            gl.uniformMatrix4fv(this.modelMatrixUniform, false, modelMat);

            gl.uniform3fv(this.diffuseUniform, material.diffuse);
            gl.activeTexture(gl.TEXTURE0);
            gl.bindTexture(gl.TEXTURE_2D, material.diffuseMap);
            gl.uniform1i(this.diffuseMapUniform, 0);

            gl.uniform3fv(this.specularUniform, material.specular);
            gl.activeTexture(gl.TEXTURE1);
            gl.bindTexture(gl.TEXTURE_2D, material.specularMap);
            gl.uniform1i(this.specularMapUniform, 1);

            gl.uniform3fv(this.ambienteUniform, material.ambient);
            gl.uniform3fv(this.emissiveUniform, material.emissive);

            gl.activeTexture(gl.TEXTURE2);
            gl.bindTexture(gl.TEXTURE_2D, material.normalMap);
            gl.uniform1i(this.normalMapUniform, 2);

            gl.uniform1f(this.opacityUniform, material.opacity);
            gl.uniform1f(this.shininessUniform, material.shininess);
            // gl.uniform3fv(this.ambientLightUniform, [1.0, 1.0, 0.0]);

            // webglUtils.setUniforms(this.meshProgramInfo,  material);
            // webglUtils.setUniforms(this.meshProgramInfo, {modelMatrix, }, material);

            // calls gl.drawArrays or gl.drawElements
            // webglUtils.drawBufferInfo(gl, bufferInfo);
            // gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, vbo_element);
            // gl.drawElements(gl.TRIANGLES, bufferInfo.numElements, gl.UNSIGNED_INT, 0);
            // gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
            gl.drawArrays(gl.TRIANGLES, 0, bufferInfo.numElements);
        }
        gl.bindVertexArray(null);

    }

    // for keyboard input
    keyEvent(key)
    {
        // this.modelPlacer.getKeyForTransformation(key);
    }

    createShaderObjects(shaderStr, shaderType) {
        let shaderObject = createAndCompileShaders(shaderStr, shaderType);
        if (shaderObject == null) {
            alert(shaderType + " Shader Compilation failed !!!");
            return null;
        }

        return shaderObject;
    }

    getUniformLocation(uniform) {
        return gl.getUniformLocation(this.shaderProgramObject, uniform);
    }

    getShaderProgramObject() {
        return this.shaderProgramObject;
    }

    uninitializeShader() {
        uninitializeShaders();
    }

    //======================== Model Loading code ================================//

    // see http://paulbourke.net/dataformats/obj/

    parseOBJ(text) {
        // because indices are base 1 let's just fill in the 0th data
        const objPositions = [[0, 0, 0]];
        const objTexcoords = [[0, 0]];
        const objNormals = [[0, 0, 0]];
        const objColors = [[0, 0, 0]];

        // same order as `f` indices
        const objVertexData = [
            objPositions,
            objTexcoords,
            objNormals,
            objColors,
        ];

        // same order as `f` indices
        let webglVertexData = [
            [],   // positions
            [],   // texcoords
            [],   // normals
            [],   // colors
        ];

        const materialLibs = [];
        const geometries = [];
        let geometry;
        let groups = ['default'];
        let material = 'default';
        let object = 'default';

        const noop = () => { };

        function newGeometry() {
            // If there is an existing geometry and it's
            // not empty then start a new one.
            if (geometry && geometry.data.position.length) {
                geometry = undefined;
            }
        }

        function setGeometry() {
            if (!geometry) {
                const position = [];
                const texcoord = [];
                const normal = [];
                const color = [];
                webglVertexData = [
                    position,
                    texcoord,
                    normal,
                    color,
                ];
                geometry = {
                    object,
                    groups,
                    material,
                    data: {
                        position,
                        texcoord,
                        normal,
                        color,
                    },
                };
                geometries.push(geometry);
            }
        }

        function addVertex(vert) {
            const ptn = vert.split('/');
            ptn.forEach((objIndexStr, i) => {
                if (!objIndexStr) {
                    return;
                }
                const objIndex = parseInt(objIndexStr);
                const index = objIndex + (objIndex >= 0 ? 0 : objVertexData[i].length);
                webglVertexData[i].push(...objVertexData[i][index]);
                // if this is the position index (index 0) and we parsed
                // vertex colors then copy the vertex colors to the webgl vertex color data
                if (i === 0 && objColors.length > 1) {
                    geometry.data.color.push(...objColors[index]);
                }
            });
        }

        const keywords = {
            v(parts) {
                // if there are more than 3 values here they are vertex colors
                if (parts.length > 3) {
                    objPositions.push(parts.slice(0, 3).map(parseFloat));
                    objColors.push(parts.slice(3).map(parseFloat));
                } else {
                    objPositions.push(parts.map(parseFloat));
                }
            },
            vn(parts) {
                objNormals.push(parts.map(parseFloat));
            },
            vt(parts) {
                // should check for missing v and extra w?
                objTexcoords.push(parts.map(parseFloat));
            },
            f(parts) {
                setGeometry();
                const numTriangles = parts.length - 2;
                for (let tri = 0; tri < numTriangles; ++tri) {
                    addVertex(parts[0]);
                    addVertex(parts[tri + 1]);
                    addVertex(parts[tri + 2]);
                }
            },
            s: noop,    // smoothing group
            mtllib(parts, unparsedArgs) {
                // the spec says there can be multiple filenames here
                // but many exist with spaces in a single filename
                materialLibs.push(unparsedArgs);
            },
            usemtl(parts, unparsedArgs) {
                material = unparsedArgs;
                newGeometry();
            },
            g(parts) {
                groups = parts;
                newGeometry();
            },
            o(parts, unparsedArgs) {
                object = unparsedArgs;
                newGeometry();
            },
        };

        const keywordRE = /(\w*)(?: )*(.*)/;
        const lines = text.split('\n');
        for (let lineNo = 0; lineNo < lines.length; ++lineNo) {
            const line = lines[lineNo].trim();
            if (line === '' || line.startsWith('#')) {
                continue;
            }
            const m = keywordRE.exec(line);
            if (!m) {
                continue;
            }
            const [, keyword, unparsedArgs] = m;
            const parts = line.split(/\s+/).slice(1);
            const handler = keywords[keyword];
            if (!handler) {
                console.warn('unhandled keyword:', keyword);  // eslint-disable-line no-console
                continue;
            }
            handler(parts, unparsedArgs);
        }

        // remove any arrays that have no entries.
        for (const geometry of geometries) {
            geometry.data = Object.fromEntries(
                Object.entries(geometry.data).filter(([, array]) => array.length > 0));
        }

        return {
            geometries,
            materialLibs,
        };
    }

    parseMapArgs(unparsedArgs) {
        // TODO: handle options
        return unparsedArgs;
    }

    parseMTL(text_) {
        const text = text_;
        const materials = {};
        let material;

        const keywords = {
            newmtl(parts, unparsedArgs) {
                material = {};
                materials[unparsedArgs] = material;
            },
            /* eslint brace-style:0 */
            Ns(parts) { material.shininess = parseFloat(parts[0]); },
            Ka(parts) { material.ambient = parts.map(parseFloat); },
            Kd(parts) { material.diffuse = parts.map(parseFloat); },
            Ks(parts) { material.specular = parts.map(parseFloat); },
            Ke(parts) { material.emissive = parts.map(parseFloat); },
            // map_Kd(parts, unparsedArgs) { material.diffuseMap = this.parseMapArgs(unparsedArgs); },
            // map_Ns(parts, unparsedArgs) { material.specularMap = this.parseMapArgs(unparsedArgs); },
            // map_Bump(parts, unparsedArgs) { material.normalMap = this.parseMapArgs(unparsedArgs); },
            map_Kd(parts, unparsedArgs) { material.diffuseMap = unparsedArgs; },
            map_Ns(parts, unparsedArgs) { material.specularMap = unparsedArgs; },
            map_Bump(parts, unparsedArgs) { material.normalMap = unparsedArgs; },
            Ni(parts) { material.opticalDensity = parseFloat(parts[0]); },
            d(parts) { material.opacity = parseFloat(parts[0]); },
            illum(parts) { material.illum = parseInt(parts[0]); },
        };

        const keywordRE = /(\w*)(?: )*(.*)/;
        const lines = text.split('\n');
        for (let lineNo = 0; lineNo < lines.length; ++lineNo) {
            const line = lines[lineNo].trim();
            if (line === '' || line.startsWith('#')) {
                continue;
            }
            const m = keywordRE.exec(line);
            if (!m) {
                continue;
            }
            const [, keyword, unparsedArgs] = m;
            const parts = line.split(/\s+/).slice(1);
            const handler = keywords[keyword];
            if (!handler) {
                console.warn('unhandled keyword:', keyword);  // eslint-disable-line no-console
                continue;
            }
            handler(parts, unparsedArgs);
        }

        return materials;
    }



    // isPowerOf2(value) {
    //     return (value & (value - 1)) === 0;
    // }

    create1PixelTexture(gl, pixel) {
        const texture = gl.createTexture();
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE,
            new Uint8Array(pixel));
        return texture;
    }

    createTexture(gl, url) {
        const texture = this.create1PixelTexture(gl, [128, 192, 255, 255]);
        // Asynchronously load an image
        const image = new Image();
        image.src = url;
        image.addEventListener('load', function () {
            // Now that the image has loaded make copy it to the texture.
            gl.bindTexture(gl.TEXTURE_2D, texture);
            gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);

            // Check if the image is a power of 2 in both dimensions.
            if (isPowerOf2(image.width) && isPowerOf2(image.height)) {
                // Yes, it's a power of 2. Generate mips.
                gl.generateMipmap(gl.TEXTURE_2D);
            } else {
                // No, it's not a power of 2. Turn of mips and set wrapping to clamp to edge
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
                gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
            }
        });
        return texture;
    }

    makeIndexIterator(indices) {
        let ndx = 0;
        const fn = () => indices[ndx++];
        fn.reset = () => { ndx = 0; };
        fn.numElements = indices.length;
        return fn;
    }

    makeUnindexedIterator(positions) {
        let ndx = 0;
        const fn = () => ndx++;
        fn.reset = () => { ndx = 0; };
        fn.numElements = positions.length / 3;
        return fn;
    }

    subtractVector2 = (a, b) => a.map((v, ndx) => v - b[ndx]);

    generateTangents(position, texcoord, indices) {
        const getNextIndex = indices ? this.makeIndexIterator(indices) : this.makeUnindexedIterator(position);
        const numFaceVerts = getNextIndex.numElements;
        const numFaces = numFaceVerts / 3;

        const tangents = [];
        for (let i = 0; i < numFaces; ++i) {
            const n1 = getNextIndex();
            const n2 = getNextIndex();
            const n3 = getNextIndex();

            const p1 = position.slice(n1 * 3, n1 * 3 + 3);
            const p2 = position.slice(n2 * 3, n2 * 3 + 3);
            const p3 = position.slice(n3 * 3, n3 * 3 + 3);

            const uv1 = texcoord.slice(n1 * 2, n1 * 2 + 2);
            const uv2 = texcoord.slice(n2 * 2, n2 * 2 + 2);
            const uv3 = texcoord.slice(n3 * 2, n3 * 2 + 2);

            // const dp12 = m4.subtractVectors(p2, p1);
            // const dp13 = m4.subtractVectors(p3, p1);
            const dp12 = (p2 - p1);
            const dp13 = (p3 - p1);

            const duv12 = this.subtractVector2(uv2, uv1);
            const duv13 = this.subtractVector2(uv3, uv1);

            const f = 1.0 / (duv12[0] * duv13[1] - duv13[0] * duv12[1]);
            // const tangent = Number.isFinite(f) ? m4.normalize(m4.scaleVector(m4.subtractVectors(m4.scaleVector(dp12, duv13[1]),m4.scaleVector(dp13, duv12[1]),), f)): [1, 0, 0];
            const tangent = Number.isFinite(f) ? m4.normalize(m4.scaleVector(m4.subtractVectors(m4.scaleVector(dp12, duv13[1]), m4.scaleVector(dp13, duv12[1]),), f)) : [1, 0, 0];

            mat4.nor

            tangents.push(...tangent, ...tangent, ...tangent);
        }

        return tangents;
    }

    generateTangents_(position, texcoord) {
        const tangents = [];
        let j = 0;
        for (let i = 0; i < position.length; i = i + 3) {
            var pos1 = new Float32Array([position[i], position[i + 1], position[i + 2]]); //vertex
            i = i + 3;
            var pos2 = new Float32Array([position[i], position[i + 1], position[i + 2]]);
            i = i + 3;
            var pos3 = new Float32Array([position[i], position[i + 1], position[i + 2]]);

            var uv1 = new Float32Array([texcoord[j], texcoord[j + 1]]); //texcoords
            j = j + 2;
            var uv2 = new Float32Array([texcoord[j], texcoord[j + 1]]);
            j = j + 2;
            var uv3 = new Float32Array([texcoord[j], texcoord[j + 1]]);
            j = j + 2;

            var edge1 = new Float32Array([pos2[0] - pos1[0], pos2[1] - pos1[1], pos2[2] - pos1[2]]);
            var edge2 = new Float32Array([pos3[0] - pos1[0], pos3[1] - pos1[1], pos3[2] - pos1[2]]);

            var deltaUV1 = new Float32Array([uv2[0] - uv1[0], uv2[1] - uv1[1]]);
            var deltaUV2 = new Float32Array([uv3[0] - uv1[0], uv3[1] - uv1[1]]);

            var f = 1.0 / (deltaUV1[0] * deltaUV2[1] - deltaUV2[0] * deltaUV1[1]);

            //normalize this inside shader
            tangents.push(f * (deltaUV2[1] * edge1[0] - deltaUV1[1] * edge2[0]));
            tangents.push(f * (deltaUV2[1] * edge1[1] - deltaUV1[1] * edge2[1]));
            tangents.push(f * (deltaUV2[1] * edge1[2] - deltaUV1[1] * edge2[2]));

            tangents.push(f * (deltaUV2[1] * edge1[0] - deltaUV1[1] * edge2[0]));
            tangents.push(f * (deltaUV2[1] * edge1[1] - deltaUV1[1] * edge2[1]));
            tangents.push(f * (deltaUV2[1] * edge1[2] - deltaUV1[1] * edge2[2]));

            tangents.push(f * (deltaUV2[1] * edge1[0] - deltaUV1[1] * edge2[0]));
            tangents.push(f * (deltaUV2[1] * edge1[1] - deltaUV1[1] * edge2[1]));
            tangents.push(f * (deltaUV2[1] * edge1[2] - deltaUV1[1] * edge2[2]));
        }

        return tangents;
    }

};

function isPowerOf2(value) {
    return (value & (value - 1)) === 0;
}