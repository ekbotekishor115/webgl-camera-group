class TextureShader
{
    constructor()
    {
        this.shaderProgramObject;
        this.vao;
        this.vbo_position;
        this.vbo_texcoord;
        this.mvpMatrixUniform;
        this.perspectiveProjectionMatrix;

        this.textureSamplerUniform;
        
        // vertex shader
        var vertexShaderSourceCode = 
        `#version 300 es
        \n
        in vec4 a_position;
        in vec2 a_texcoord;
        uniform mat4 u_mvpMatrix;
        out vec2 a_texcoord_out;
        void main(void)
        {
            a_texcoord_out = a_texcoord;
            gl_Position = u_mvpMatrix * a_position;
        }`;

        // vertex shader object
        this.vertexShaderObject = this.createShaderObjects(vertexShaderSourceCode, gl.VERTEX_SHADER);

        // fragment shader
        var fragmentShaderSourceCode = 
        `#version 300 es
        \n
        precision highp float;
        in vec2 a_texcoord_out;
        out vec4 FragColor;
        uniform highp sampler2D u_texturesampler;
        void main(void)
        {
        FragColor = texture(u_texturesampler, a_texcoord_out);
        }`;

        // fragment shader object
        this.fragmentShaderObject = this.createShaderObjects(fragmentShaderSourceCode, gl.FRAGMENT_SHADER);

        // Attach and Link shaderObjects
        this.shaderProgramObject = attachAndLinkShaderObjects(
            [this.vertexShaderObject, this.fragmentShaderObject],
            [VertexAttributeEnum.CAM_ATTRIBUTE_POSITION, VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS],
            ["a_position", "a_texcoord"]
        );
        if (this.shaderProgramObject == null) {
            alert("ShaderProgramObject failed");
        }

        // uniforms
        this.mvpMatrixUniform = gl.getUniformLocation(this.shaderProgramObject, "u_mvpMatrix");
        this.textureSamplerUniform = gl.getUniformLocation(this.shaderProgramObject, "u_texturesampler");

    }

    initialize()
    {
        
    }

    createShaderObjects(shaderStr, shaderType)
    {
        let shaderObject = createAndCompileShaders(shaderStr, shaderType);
        if (shaderObject == null) 
        {
            alert(shaderType + " Shader Compilation failed !!!");
            return null;
        }

        return shaderObject;
    }

    getUniformLocation(uniform)
    {
        return gl.getUniformLocation(this.shaderProgramObject, uniform);
    }

    getShaderProgramObject()
    {
        return this.shaderProgramObject;
    }

    uninitializeShader()
    {
        uninitializeShaders();
    }
};