class WobbleShader
{
    constructor()
    {
        // Shaders
        this.shaderProgramObject = null;

        // Uniforms
        this.mvpMatrixUniform;
        this.modelMatrixUniform;
        this.viewMatrixUniform;
        this.projectionMatrixUniform;

        this.vertexShaderObject;
        this.fragmentShaderObject;
    }

    async initialize()
    {
        var vertexShaderSourceCode =
        `#version 300 es
        \n
        in vec4 a_position;
        in vec2 texcoord;
        in vec3 a_normal;

        uniform mat4 u_modelMatrix;
        uniform mat4 u_viewMatrix;
        uniform mat4 u_projectionMatrix;

        uniform vec3 LightPosition;

        out vec2 texcoord_out;
        out float LightIntensity;

        const float specularContribution = 0.1;
        const float diffuseContribution = 1.0 - specularContribution;

        void main() 
        {
            vec3 eyeCoordinates = vec3(u_viewMatrix * u_modelMatrix * a_position);
            mat3 normalMatrix = mat3(u_viewMatrix * u_modelMatrix);
            vec3 transformedNormals = normalize(normalMatrix * a_normal);
            vec3 lightVec = normalize(LightPosition - eyeCoordinates);
            vec3 reflectVec = reflect(-lightVec, transformedNormals);
            vec3 viewerVector = normalize(-eyeCoordinates);

            float spec = clamp(dot(reflectVec, viewerVector), 0.0, 1.0);
            spec = pow(spec, 16.0);

            LightIntensity = diffuseContribution * max(dot(lightVec, transformedNormals), 0.0) + specularContribution * spec;
            texcoord_out = texcoord;

            gl_Position = u_projectionMatrix * u_viewMatrix * u_modelMatrix * a_position;
        }`;

        // vertex shader object
        this.vertexShaderObject = this.createShaderObjects(vertexShaderSourceCode, gl.VERTEX_SHADER);


        var fragmentShaderSourceCode =
        `#version 300 es
        \n
        precision highp float;

        const float C_PI = 3.1415;
        const float C_2PI = 2.0 * C_PI;
        const float C_2PI_I = 1.0 / (2.0 * C_PI);
        const float C_PI_2 = C_PI / 2.0;

        in vec2 texcoord_out;
        in float LightIntensity;

        uniform float StartRad;
        uniform vec2 Freq;
        uniform vec2 Amplitude;
        uniform sampler2D WobbleTex;

        out vec4 FragColor;

        void main()
        {	
            vec2 perturb;
            float rad;
            vec4 color;

            // Compute a perturbation factor for the x direction
            rad = (texcoord_out.s + texcoord_out.t - 1.0 + StartRad) * Freq.x;

            // Wrap to -2.0*PI, 2*PI
            rad = rad * C_2PI_I;
            rad = fract(rad);
            rad = rad * C_2PI;
            
            // Center in -PI, PI
            if (rad > C_PI) 
                rad = rad - C_2PI;
            if (rad < -C_PI) 
                rad = rad + C_2PI;
            
            // Center in -PI/2, PI/2
            if (rad > C_PI_2) 
                rad = C_PI - rad;
            if (rad < -C_PI_2) 
                rad = -C_PI - rad;
            perturb.x = (rad - (rad * rad * rad / 6.0)) * Amplitude.x;

            // Now compute a perturbation factor for the y direction
            rad = (texcoord_out.s - texcoord_out.t + StartRad) * Freq.y;

            // Wrap to -2*PI, 2*PI
            rad = rad * C_2PI_I;
            rad = fract(rad);
            rad = rad * C_2PI;
            
            // Center in -PI, PI
            if (rad > C_PI) 
                rad = rad - C_2PI;
            if (rad < -C_PI) 
                rad = rad + C_2PI;
            
            // Center in -PI/2, PI/2
            if (rad > C_PI_2) 
                rad = C_PI - rad;

            if (rad < -C_PI_2) 
                rad = -C_PI - rad;
            perturb.y = (rad - (rad * rad * rad / 6.0)) * Amplitude.y;
            color = (texture(WobbleTex, perturb + texcoord_out.st));
            
            FragColor = vec4(color.rgb * LightIntensity, color.a);
        }`;

        // fragment shader object
        this.fragmentShaderObject = this.createShaderObjects(fragmentShaderSourceCode, gl.FRAGMENT_SHADER);

        // Attach and Link shaderObjects
        this.shaderProgramObject = attachAndLinkShaderObjects(
            [this.vertexShaderObject, this.fragmentShaderObject],
            [VertexAttributeEnum.CAM_ATTRIBUTE_POSITION, VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS, VertexAttributeEnum.CAM_ATTRIBUTE_NORMAL],
            ["a_position", "texcoord", "a_normal"]
        );
        if (this.shaderProgramObject == null) {
            alert("ShaderProgramObject failed");
        }

        // uniforms
        this.modelMatrixUniform = this.getUniformLocation("u_modelMatrix");
        this.viewMatrixUniform = this.getUniformLocation("u_viewMatrix");
        this.projectionMatrixUniform = this.getUniformLocation("u_projectionMatrix");
        this.LightPositionUniform = this.getUniformLocation("LightPosition");

        this.StartRadUniform = this.getUniformLocation("StartRad");
        this.FreqUniform = this.getUniformLocation("Freq");
        this.AmplitudeUniform = this.getUniformLocation("Amplitude");
        this.WobbleTexSamplerUniform = this.getUniformLocation("ColorMapSampler");
    }


    createShaderObjects(shaderStr, shaderType)
    {
        let shaderObject = createAndCompileShaders(shaderStr, shaderType);
        if (shaderObject == null) 
        {
            alert(shaderType + " Shader Compilation failed !!!");
            return null;
        }

        return shaderObject;
    }

    getUniformLocation(uniform)
    {
        return gl.getUniformLocation(this.shaderProgramObject, uniform);
    }

    getShaderProgramObject()
    {
        return this.shaderProgramObject;
    }

    uninitializeShader()
    {
        uninitializeShaders();
    }
};