class GodraysShader
{
    constructor()
    {
        // Shaders
        this.shaderProgramObject = null;

        // Uniforms
        this.mvpMatrixUniform;
        this.modelMatrixUniform;
        this.viewMatrixUniform;
        this.projectionMatrixUniform;

        this.vertexShaderObject;
        this.fragmentShaderObject;
    }

    async initialize()
    {
        var vertexShaderSourceCode =
        `#version 300 es
        \n
        in vec4 a_position;
        in vec2 texcoord;

        // uniform mat4 u_modelMatrix;
        // uniform mat4 u_viewMatrix;
        // uniform mat4 u_projectionMatrix;

        out vec2 texcoord_out;

        void main() 
        {
            texcoord_out = texcoord;
            gl_Position = a_position;
        }`;

        // vertex shader object
        this.vertexShaderObject = this.createShaderObjects(vertexShaderSourceCode, gl.VERTEX_SHADER);


        var fragmentShaderSourceCode =
        `#version 300 es
        \n
        precision highp float;
        in vec2 texcoord_out;

        uniform float exposure;
        uniform float decay;
        uniform float density;
        uniform float weight;
        uniform float u_illuminationDecay;
        uniform vec2 lightPositionOnScreen[2];

        uniform sampler2D UserMapSampler;
        uniform sampler2D ColorMapSampler;

        const int NUM_SAMPLES = 500;

        out vec4 FragColor;

        void main()
        {	
            float decay_s=0.97815;
            float exposure_s=1.8;
            float density_s=1.5926;
            float weight_s=0.58767 * 5.0;

            vec4 color;
            vec4 realColor = texture(UserMapSampler, texcoord_out);
            color = texture(ColorMapSampler, texcoord_out)*0.4;
            for (int clpos=0; clpos < 1; clpos++)
            {
                vec2 textCoo = texcoord_out;
                vec2 deltaTextCoord = vec2( textCoo - lightPositionOnScreen[clpos] );
                deltaTextCoord *= 1.0 /  float(NUM_SAMPLES) * density_s;
                float illuminationDecay = u_illuminationDecay;

                for(int i=0; i < NUM_SAMPLES ; i++)
                {
                        textCoo -= deltaTextCoord;
                        vec4 sampleColor = texture(ColorMapSampler, textCoo )*0.4;
                        sampleColor *= illuminationDecay * weight_s;
                        color += sampleColor;
                        illuminationDecay *= decay_s;
                }
            }
            FragColor = ((vec4((vec3(color.r, color.g, color.b) * exposure_s), 1.0)) + (realColor * (1.1)));
        }`;

        // fragment shader object
        this.fragmentShaderObject = this.createShaderObjects(fragmentShaderSourceCode, gl.FRAGMENT_SHADER);

        // Attach and Link shaderObjects
        this.shaderProgramObject = attachAndLinkShaderObjects(
            [this.vertexShaderObject, this.fragmentShaderObject],
            [VertexAttributeEnum.CAM_ATTRIBUTE_POSITION, VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS],
            ["a_position", "texcoord"]
        );
        if (this.shaderProgramObject == null) {
            alert("ShaderProgramObject failed");
        }

        // uniforms
        this.mvpMatrixUniform = this.getUniformLocation("uMVPMatrix");

        this.lightPositionOnscreenUniform = [2];

        this.exposureUniform = this.getUniformLocation("exposure");
        this.decayUniform = this.getUniformLocation("decay");
        this.densityUniform = this.getUniformLocation("density");
        this.weightUniform = this.getUniformLocation("weight");
        this.illuminationDecayUniform = this.getUniformLocation("u_illuminationDecay");
        this.lightPositionOnscreenUniform[0] = this.getUniformLocation("lightPositionOnScreen[0]");
        this.lightPositionOnscreenUniform[1] = this.getUniformLocation("lightPositionOnScreen[1]");
        this.userMapSamplerUniform = this.getUniformLocation("UserMapSampler");
        this.colorMapSamplerUniform = this.getUniformLocation("ColorMapSampler");
    }

    setUniformsForGodrays(texUserMap, texColorMap)
    {
        var Exposure = 0.2;
        var Decay = 0.97815;
        var Density = 0.926;
        var Weight = 0.98767;
        gl.uniform1f(this.exposureUniform, Exposure);
        gl.uniform1f(this.decayUniform, Decay);
        gl.uniform1f(this.densityUniform, Density);
        gl.uniform1f(this.weightUniform, Weight);
        gl.uniform1f(this.illuminationDecayUniform, 0.7);
        gl.uniform2f(this.lightPositionOnscreenUniform[0], 0.5, 0.6);
        gl.uniform2f(this.lightPositionOnscreenUniform[1], -0.5, -0.6);

        gl.activeTexture(gl.TEXTURE1);
        gl.bindTexture(gl.TEXTURE_2D, texUserMap);
        gl.uniform1i(this.userMapSamplerUniform, 1);

        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, texColorMap);
        gl.uniform1i(this.colorMapSamplerUniform, 0);
    }

    createShaderObjects(shaderStr, shaderType)
    {
        let shaderObject = createAndCompileShaders(shaderStr, shaderType);
        if (shaderObject == null) 
        {
            alert(shaderType + " Shader Compilation failed !!!");
            return null;
        }

        return shaderObject;
    }

    getUniformLocation(uniform)
    {
        return gl.getUniformLocation(this.shaderProgramObject, uniform);
    }

    getShaderProgramObject()
    {
        return this.shaderProgramObject;
    }

    uninitializeShader()
    {
        uninitializeShaders();
    }
};