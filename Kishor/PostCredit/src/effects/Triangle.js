class Triangle
{
    constructor()
    {
        this.shader = new TriangleShader();
        this.shader_texture = new TextureShader();
        this.shaderProgramObject;
        this.vao;
        this.vbo;
        this.wall;
    }

    initialize()
    {
        this.shaderProgramObject = this.shader.getShaderProgramObject();
        this.shaderProgramObject_texture = this.shader_texture.getShaderProgramObject();

        // geometry attribute declaration
        var trianglePosition = new Float32Array(
            [
                0.0, 1.0, 0.0,
                -1.0, -1.0, 0.0,
                1.0, -1.0, 0.0
            ]
        );

        var texcoord = new Float32Array([
            0.5, 1.0,
            0.0, 0.0,
            1.0, 0.0
        ]);

        // vao
        this.vao = gl.createVertexArray();
        gl.bindVertexArray(this.vao);

        // vbo pos
        this.vbo = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);
        gl.bufferData(gl.ARRAY_BUFFER, trianglePosition, gl.STATIC_DRAW);
        gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        // vbo texcoord
        this.vbo_texcoord = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_texcoord);
        gl.bufferData(gl.ARRAY_BUFFER, texcoord, gl.STATIC_DRAW);
        gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS, 2, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        gl.bindVertexArray(null);

        this.wall = new Shapes();
        // this.wall.initQuadForNormalMapping();

    }

    display(cam)
    {
        gl.useProgram(this.shaderProgramObject);

        // transformation
        var modelMatrix = mat4.create();
        var viewMatrix = mat4.create();
        var modelViewMatrix = mat4.create();
        var modelViewProjection = mat4.create();

        viewMatrix = cam.getViewMatrix();
        //  mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -3.0]);
        
        mat4.multiply(modelViewMatrix, modelMatrix, viewMatrix);
        mat4.multiply(modelViewProjection, perspectiveProjectionMatrix, modelViewMatrix);
        gl.uniformMatrix4fv(this.shader.mvpMatrixUniform, false, modelViewProjection);

        // gl.bindVertexArray(this.vao);
        // gl.drawArrays(gl.TRIANGLES, 0, 3);
        // gl.bindVertexArray(null);

        this.wall.drawQuadNormalMapped();
        // this.wall.drawQuad();

        gl.useProgram(null);
    }

    displayTexture(modelMat, cam, texture)
    {
        // gl.clearColor(0.5, 0.3, 0.5, 1.0);
        // gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        gl.useProgram(this.shaderProgramObject_texture);

        // transformation
        var modelMatrix = mat4.create();
        var viewMatrix = mat4.create();
        var modelViewMatrix = mat4.create();
        var modelViewProjection = mat4.create();

        viewMatrix = cam.getViewMatrix();
        // mat4.lookAt(viewMatrix,
        //             [0.0, 0.0, 1.0],
        //             [0.0, 0.0 , 0.0],
        //             [0.0, 1.0 , 0.0]);
        
        mat4.multiply(modelViewMatrix, viewMatrix, modelMat);
        mat4.multiply(modelViewProjection, perspectiveProjectionMatrix, modelViewMatrix);
        
        gl.uniformMatrix4fv(this.shader_texture.mvpMatrixUniform, false, modelViewProjection);
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, texture);
        gl.uniform1i(this.shader_texture.textureSamplerUniform, 0);

        // gl.bindVertexArray(this.vao);
        // gl.drawArrays(gl.TRIANGLES, 0, 3);
        // gl.bindVertexArray(null);
        this.wall.drawQuad();
        
        gl.bindTexture(gl.TEXTURE_2D, null);

        gl.useProgram(null);
        
    }

    update()
    {
        // code
    }

    uninitializeEffect()
    {
        if (this.shader)
        {
            this.shader.uninitializeShader();
            this.shader = null;
        }

        if (this.vbo)
        {
            gl.deleteBuffer(this.vbo);
            this.vbo = null;
        }
        
        if (this.vao)
        {
            gl.deleteVertexArray(this.vao);
            this.vao = null;
        }
    }
}