var canvas = null;
var gl = null;
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;

// sample
var triangle;

var shaderProgramObject = null;

var vao = null;
var vbo = null;
var mvpMatrixUniform;
var perspectiveProjectionMatrix;

/// CAMERA
var camera = null;

/// Audio
var audio = new AudioHelper();

// model
// var model = new ModelLoader();
// var nmap = new NormalMapping();
var postCredits = null;

var godrays;

var requestAnimationFrame =
    window.requestAnimationFrame ||         // Chrome
    window.webkitRequestAnimationFrame ||   // for safari
    window.mozRequestAnimationFrame ||      // mozila
    window.oRequestAnimationFrame ||        // Opera
    window.msRequestAnimationFrame;         // Edge

// analogus to entry point 
async function main()
{
    // get canvas
    canvas = document.getElementById("CAMERA");
    if (canvas == null)
        console.log("Getting Canvas Failed...!\n");
    else
        console.log("Getting Canvas Succeded...!\n");

    // set canvas width and height for future use
    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;
    
    // register for keyboard events
    window.addEventListener("keydown", keyDown, false);

    // register for mouse events
    window.addEventListener("click", mouseDown, false);

    // register for resizong the canvas
    window.addEventListener("resize", resize, false);

    window.addEventListener("keyup", keyUp, false);
    canvas.addEventListener("mousemove", mouseMove, false);
    canvas.addEventListener("wheel", wheelMove, false);

    await initialize();

    resize();

    display();
}

function keyDown(event)
{
    // code
    // Passing key input to camera
    camera.keyboardInputs(event);

    switch (event.keyCode)
    {
        // browser exit is not generic so behavoiur us unpredicted
        case 81:    // Q
        case 113:  // q 
            uninitialize();
            window.close();
            break;
        case 70:
        case 102:
            toggleFullscreen();
            break;
    }

    switch (event.key)
    {
        
        
    }
    audio.audioControls(event.key)
    // model.keyEventModel(event.key);
    // godrays.model.keyEventModel(event.key);
    postCredits.keyEventModel(event.key);
    console.log(event);
}

function keyUp(event) {
    camera.inputOnKeyUp(event);
}

// Passing mouse input to camera
function mouseMove(event) {
    camera.mouseInputs(event);
}

// Passing mouse input to camera
function wheelMove(event) {
    camera.mouseScroll(event);
}

// Mouse handler
function mouseDown()
{

}


function toggleFullscreen()
{
    var fullscreen_element = 
        document.fullscreenElement || 
        document.webkitFullscreenElement ||
        document.mozFullScreenElement ||
        document.msFullscreenElement ||
        null;

    if (fullscreen_element == null)
    {
        if (canvas.RequestFullscreen)
            canvas.RequestFullscreen();
        else if (canvas.webkitRequestFullscreen)
            canvas.webkitRequestFullscreen();
        else if (canvas.mozRequestFullScreen)
            canvas.mozRequestFullScreen();
        else if (canvas.msRequestFullscreen)
            canvas.msRequestFullscreen();

        bFullScreen = true;
    }
    else // if already fullscreen
    {
        if (document.exitFullscreen)
            document.exitFullscreen();
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if (document.msExitFullscreen)
            document.msExitFullscreen();

        bFullScreen = false;
    }
}

async function initialize()
{
    // code
    // get context from above canvas
    gl = canvas.getContext("webgl2");
    if (gl == null)
        console.log("Getting WebGL 2 context Failed...!\n");
    else
        console.log("Getting WebGL 2 context Succeded...!\n");

    // set webgl2 context's view width and view height properties
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // effect / scene / object
    // triangle = new Triangle();
    // triangle.initialize();

    // godrays = new Godrays();

    postCredits = new PostCredits();
    // model
     ////////////////////////////// initializing Object
     await Promise.all([
        // godrays.initialize(),
        // model.initialize(),
        // nmap.initialize(),
        postCredits.initialize(),
    ]);
    

    // depth
    gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);

    // set clear color
    gl.clearColor(0.2, 0.2, 0.2, 1.0);

    camera = new Camera(canvas.width, canvas.height, [0.0, 0.0, 2.0]);

    // initialize matrix
    perspectiveProjectionMatrix = mat4.create();

    // audio 
    audio.play('./assets/audio/Version01.wav');
}

function resize()
{
    // code
    if (bFullScreen == true)
    {
        canvas.width = window.innerWidth;
        canvas.height = window.innerHeight;
    }
    else
    {
        canvas.width = canvas_original_width;
        canvas.height = canvas_original_height;
    }

    camera.updateResolution(canvas.width, canvas.height);

    // set viewport
    gl.viewport(0, 0, canvas.width, canvas.height);

    // seet perspective projection
    mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 1000.0);

}

function display(now)
{
    // code
    displayFPS(now);
    audio.displaySongTime();
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // triangle.display(camera);
    // model.display(camera, 1);
    // godrays.displayGodrays(camera, 1);
    // nmap.displayNMap(camera);
    postCredits.display(camera);

    // update the animation
    update();
    
    // double buffering
    requestAnimationFrame(display, canvas);
}

function update()
{
    // code
    // triangle.update();
}

function uninitialize()
{
    // code
    if (postCredits)
    {
        postCredits.uninitialize();
        postCredits = null;
    }

    if (triangle)
    {
        triangle.uninitializeEffect();
        triangle = null;
    }

    // if (model)
    // {
    //     model.uninitialize();
    //     model = null;
    // }
}

//==================== FPS ======================
const fpsElem = document.querySelector("#fps");

let then = 0; // Used to display FPS
function displayFPS(now) {
    now *= 0.001;                          // convert to seconds
    const deltaTime = now - then;          // compute time since last frame
    then = now;                            // remember time for next frame
    const fps = 1 / deltaTime;             // compute frames per second
    fpsElem.textContent = `${fps.toFixed(1)}  FPS`;
    
    
    // variables.textContent = `${audio.playbackRate.toFixed(1)} Secs`;
}
