/****************************** 2DTexture Smiley*************************/
// global variable
var canvas = null;
var gl = null;
var bFullScreen = false;
var canvas_original_width;
var canvas_original_height;

// WebGL related variables
var shaderProgramObject = null;

const VertexAttributeEnum =
{
	AMC_ATTRIBUTE_POSITION: 0,
	AMC_ATTRIBUTE_COLOR: 1,
	AMC_ATTRIBUTE_TEXCOORD: 2
};

var vao_Square = 0;
var vbo_position_Square = 0;
var vbo_texcoord_Square = 0;

var mvpMatrixUniform;
var textureSamplerUniform;

// gray scale conversion
var conversionFactorUniform = 0;

var animationFactorUniform = 0;
var viewPortHeightUniform = 0;

// variable for gray scale conversion factor
var grayScaleConversionFactor = 0.0;

// variable for scale 
var animationFactor = 0.0;
var viewPortHeight = 0.0;

// gray scale animation
var greyScaleAnimation = false;
var greyScaleAnimationToggle = false;

var perspectiveProjectionMatrix;

// Texture object variable
var texture_smiley = 0;

var requestAnimationFrame =
	window.requestAnimationFrame || // for chrome
	window.webkitRequestAnimationFrame || // for safari
	window.mozRequestAnimationFrame || // for mozilla firbox
	window.oRequestAnimationFrame || // for opera
	window.msRequestAnimationFrame; // for microsoft edge

// main function
function main()
{
	// get canvas
	canvas = document.getElementById("DBB");
	if (canvas == null)
	{
		console.log("getting canvas failed\n");
	}
	else
	{
		console.log("getting canvas succeded\n");
	}

	// set canvas width and height for future use
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;
		
	// register for keyboard events
	window.addEventListener("keydown", keyDown, false);

	// register for mouse events
	window.addEventListener("click", mouseDown, false);

	window.addEventListener("resize", resize, false);

	initialise();

	resize();

	display();
}

function keyDown(event)
{
	// code
	switch (event.keyCode)
	{
		// quit 81=Q
		case 81:		
			uninitialise();
			window.close();
		break;

		// togglefull screen
		case 70:		
			toggleFullscreen();
		break;
		
		// A/a= 65
		case 65:		
			if (greyScaleAnimation == false)
			{
				greyScaleAnimation = true;
				console.log("A key press \n");
			}
			else
			{
				greyScaleAnimation = false;
				console.log("A key not press \n");
			}
		break;

		//T/t = 84
		case 84:		
			if (greyScaleAnimationToggle == false)
			{
				greyScaleAnimationToggle = true;
				console.log("T key press \n");
			}
			else
			{
				greyScaleAnimationToggle = false;
				console.log("T key not press \n");
			}
		break;
	}
}

function mouseDown()
{
	// code

}

function toggleFullscreen()
{
	var fullScreen_element =
		document.fullscreenElement ||
		document.webkitFullscreenElement ||
		document.mozFullScreenElement ||
		document.msFullscreenElement ||
		null;

	// if not full screen
	if (fullScreen_element == null)
	{
		if (canvas.requestFullscreen)
		{
			canvas.requestFullscreen();
		}
		else if (canvas.webkitRequestFullscreen)
		{
			canvas.webkitRequestFullscreen();
		}
		else if (canvas.mozRequestFullScreen)
		{
			canvas.mozRequestFullScreen();
		}
		else if (canvas.msRequestFullscreen)
		{
			canvas.msRequestFullscreen();
		}

		// set bFullScreen bit to true
		bFullScreen = true;
	}
	else // if already full screen
	{
		if (document.exitFullscreen)
		{
			document.exitFullscreen();
		}
		else if (documet.webkitExitFullscreen)
		{
			documet.webkitExitFullscreen();
		}
		else if (document.mozCancleFullScreen)
		{
			document.mozCancleFullScreen();
		}
		else if (document.msExitFullscreen)
		{
			document.msExitFullscreen();
		}

		// set bFullScreen bit to false
		bFullScreen = false;
	}
}

function initialise()
{
	var bResult;

	// code
	// get context(gl) from canvas
	gl = canvas.getContext("webgl2");
	if (gl == null)
	{
		console.log("getting WebGL2 context failed\n");
	}
	else
	{
		console.log("getting WebGL2 context succeded\n");
	}

	// set WebGL2 contex's view width and view height properties
	gl.viewportWidth = canvas.width;
	gl.viewportHight = canvas.height;

	// Vertex Shader
	var vertexShaderSourceCode =
		(
			"#version 300 es"+
			"\n"+
			"in vec4 aPosition;"+
			"uniform mat4 uMVPMatrix;"+
			"in vec2 aTexCoord;" +
			"out vec2 oTexCoord;" +			
			"void main(void)"+
			"{"+
				"gl_Position= uMVPMatrix * aPosition;"+
				"oTexCoord = aTexCoord;"+
			"}"
		);

	var vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);
	if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if (error.length > 0)
		{
			var log = "Vertex Shader compilation error : " + error;
			alert(log);
			uninitialise();
		}
	}
	else
	{
		console.log("Vertex Shader complied successfully.\n");
	}

	// Fragment shader
	var fragmentShaderSourceCode =
	(
		"#version 300 es"+
		"\n"+
		"precision highp float;"+
		"in vec2 oTexCoord;" +
		"uniform sampler2D uTextureSampler;" +
		"uniform float uConversionFactor;" +
		"uniform float uViewPortHeight;" +
		"uniform float uAnimationFactor;" +		
		"out vec4 FragColor;"+
		"void main(void)"+
		"{" +
			"vec4 color = texture(uTextureSampler, oTexCoord);" +
			"if ((gl_FragCoord.y / uViewPortHeight) < uAnimationFactor)" +
			"{" +
				"float greyScaleFactor = (color.r * 0.3) + (color.g * 0.59) + (color.b * 0.11);" +
				"vec4 greyScaleColor = vec4 (greyScaleFactor, greyScaleFactor, greyScaleFactor, 1.0);" +
				"FragColor = mix(color, greyScaleColor, uConversionFactor);" +
			"}" +
			"else" +
			"{" +
				"FragColor = color;" +
			"}" +
		"}"
	);

	var fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);
	if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false)
	{
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if (error.length > 0)
		{
			var log = "Fragment Shader compilation error : " + error;
			alert(log);
			uninitialise();
		}
		else
		{
			console.log("Fragment Shader complied successfully.\n");
		}	
	}

	// shader program
	shaderProgramObject = gl.createProgram();
	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject, VertexAttributeEnum.AMC_ATTRIBUTE_POSITION, "aPosition");
	gl.bindAttribLocation(shaderProgramObject, VertexAttributeEnum.AMC_ATTRIBUTE_TEXCOORD, "aTexCoord");

	gl.linkProgram(shaderProgramObject);

	if (gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS) == false)
	{
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if (error.length > 0)
		{
			var log = "Shader Program linking error : " + error;
			alert(log);
			uninitialise();
		}
	}
	else
	{
		console.log("Shader Program link successfully \n");
	}

	// uniform
	mvpMatrixUniform = gl.getUniformLocation(shaderProgramObject, "uMVPMatrix");
	textureSamplerUniform = gl.getUniformLocation(shaderProgramObject, "uTextureSampler");
	conversionFactorUniform = gl.getUniformLocation(shaderProgramObject, "uConversionFactor");
	animationFactorUniform = gl.getUniformLocation(shaderProgramObject, "uAnimationFactor");
	viewPortHeightUniform = gl.getUniformLocation(shaderProgramObject, "uViewPortHeight");

	// geometry attribute declarations
	var square_position = new Float32Array([
		1.0, 1.0, 0.0,
		-1.0, 1.0, 0.0,
		-1.0, -1.0, 0.0,
		1.0, -1.0, 0.0
	]);

	var square_texcoord = new Float32Array([
		1.0, 1.0,
		0.0, 1.0,
		0.0, 0.0,
		1.0, 0.0
	]);

	// vao
	vao_Square = gl.createVertexArray();
	gl.bindVertexArray(vao_Square);

	// vbo for position
	vbo_position_Square = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_Square);
	gl.bufferData(gl.ARRAY_BUFFER, square_position, gl.STATIC_DRAW);
	gl.vertexAttribPointer(VertexAttributeEnum.AMC_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(VertexAttributeEnum.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	
	// vbo for texcoord
	vbo_texcoord_Square = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texcoord_Square);
	gl.bufferData(gl.ARRAY_BUFFER, square_texcoord, gl.STATIC_DRAW);
	gl.vertexAttribPointer(VertexAttributeEnum.AMC_ATTRIBUTE_TEXCOORD, 2, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(VertexAttributeEnum.AMC_ATTRIBUTE_TEXCOORD);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindVertexArray(null);

	// depth initialisation
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

	// set clear color to black
	gl.clearColor(0.0, 0.0, 0.0, 1.0); //WebGL API=>(clearColor)

	// initialise projection matrix
	perspectiveProjectionMatrix = mat4.create();

	bResult = loadGLTexture();
	//error checking
	if (bResult == false)
	{
		console.log("Loading of texture_smiley() Failed \n");
	}
	else
	{
		console.log("Loading of texture_smiley() Successfull \n");
	}
}

function loadGLTexture()
{
	texture_smiley = gl.createTexture();

	texture_smiley.image = new Image();

	texture_smiley.image.src = "Smiley.png";

	texture_smiley.image.onload = function ()
	{
		gl.bindTexture(gl.TEXTURE_2D, texture_smiley);
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture_smiley.image);
		gl.generateMipmap(gl.TEXTURE_2D);
		gl.bindTexture(gl.TEXTURE_2D, null);
	}
}

function resize()
{
	// code
	if (bFullScreen == true)
	{
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else
	{
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	// set view port
	gl.viewport(0, 0, canvas.width, canvas.height);// WebGL API

	// setting viewport height for gray scale animation
	viewPortHeight =  canvas.height;

	// set perspective position
	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);
}

function display()
{
	// code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	gl.useProgram(shaderProgramObject);

	// transformation
	var modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();

	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -3.0]);
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);

	gl.uniformMatrix4fv(mvpMatrixUniform, false, modelViewProjectionMatrix);
	//push conversion factor in to vertex shadre's  uniform
	gl.uniform1f(conversionFactorUniform, grayScaleConversionFactor);
	gl.uniform1f(viewPortHeightUniform, viewPortHeight);
	gl.uniform1f(animationFactorUniform, animationFactor);

	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_2D, texture_smiley);
	gl.uniform1i(textureSamplerUniform, 0);

	gl.bindVertexArray(vao_Square);

	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.bindVertexArray(null);

	// update  call
	update();

	gl.useProgram(null);
	
	// do the double buffering
	requestAnimationFrame(display, canvas);
}

function update()
{
	// code
	if (greyScaleAnimationToggle == false)
		{
			grayScaleConversionFactor = grayScaleConversionFactor + 0.005;
			if (grayScaleConversionFactor > 1.0)
			{
				grayScaleConversionFactor = 0.0;
			}
		}
		else
		{
			if (greyScaleAnimation == true)
			{
				if (animationFactor < 1.0)
				{
					animationFactor = animationFactor + 0.005;
				}
				else
				{
					animationFactor = 0.0;
				}
			}
			
		}

}

function uninitialise()
{
	// code
	if (shaderProgramObject) // this shaderProgramObject != NULL
	{
		gl.useProgram(shaderProgramObject);
		var shaderObjects = gl.getAttachedShaders(shaderProgramObject);
		if (shaderObjects && shaderObjects.length > 0)
		{
			for (let i = 0; i < shaderObjects.length; i++)
			{
				gl.detachShader(shaderProgramObject, shaderObjects[i]);
				gl.deleteShader(shaderObjects[i]);
				shaderObjects[i] = null;
			}
		}
		gl.useProgram(null);
		gl.deleteProgram(shaderProgramObject);
		shaderObjects = null;
	}

	if (vbo_texcoord_Square)
	{
		gl.deleteBuffer(vbo_texcoord_Square);
		vbo_texcoord_Square = null;
	}

	if (vbo_position_Square)
	{
		gl.deleteBuffer(vbo_position_Square);
		vbo_position_Square = null;
	}	

	if (vao_Square)
	{
		gl.deleteVertexArray(vao_Square);
		vao_Square = null;
	}
}
