class Scene2
{
    constructor()
    {
        // this.triangleScene2 = new Triangle();
        this.cubeScene2 = new Shapes();       
        // this.shader = new TriangleShader();
        // this.shader_texture = new TextureShader();
        this.adslight = new PointLightShader();
        this.shaderProgramObject;
        this.shaderProgramObject_texture;

        this.modelShader = new ModelShader();

        this.modelPlacer = new ModelPlacer();

        this.smiley_texture = null;
        this.floor_texture = null;
        this.roof_texture = null;


    }

    initialize()
    {
        // this.shaderProgramObject = this.shader.getShaderProgramObject();


        // this.triangleScene2.initialize();

        this.cubeScene2.initialize();        


        this.smiley_texture = loadTextureFromFile('./assets/textures/stone.png');
        if (this.smiley_texture < 1)
        {
            alert("Texture loading failed");
        }

        this.floor_texture = loadTextureFromFile('./assets/textures/floorJail.png');
        if (this.floor_texture < 1)
        {
            alert("Texture loading failed");
        }

        this.roof_texture = loadTextureFromFile('./assets/textures/roofjail.png');
        if (this.floor_texture < 1)
        {
            alert("Texture loading failed");
        }


        //for model loading

        // this.modelShader.initialize('./assets/models/Tree1/tree_small_02_4k.obj');
        // this.modelShader.initialize('./assets/models/Tree3/jacaranda_treeV7.obj');
        this.modelShader.initialize('./assets/models/mashal/mashal.obj');


    }

    drawCubeForScene2(cam )
    {
        gl.useProgram(this.shaderProgramObject);

        // transformation
        var modelMatrix = mat4.create();
        var viewMatrix = mat4.create();
        var modelViewMatrix = mat4.create();
        var modelViewProjection = mat4.create();

        mat4.identity(modelMatrix);
        mat4.identity(viewMatrix);
        mat4.identity(modelViewMatrix);
        mat4.identity(modelViewProjection);

        mat4.translate(modelMatrix , modelMatrix , [1.0, 0.0 , -60.0] );
        mat4.scale(modelMatrix , modelMatrix , [15.0 , 15.0 , 1.0]);
        
        // mat4.rotate(modelViewMatrix , modelViewMatrix , angle_Rectangle , [1.0, 0.0 , 0.0]); // source , target , values

        viewMatrix = cam.getViewMatrix();
        
        mat4.multiply(modelViewMatrix, modelMatrix, viewMatrix);
        mat4.multiply(modelViewProjection, perspectiveProjectionMatrix, modelViewMatrix);
        gl.uniformMatrix4fv(this.shader.mvpMatrixUniform, false, modelViewProjection);

        this.cubeScene2.drawCube();




        gl.useProgram(null);

    }

    drawCubeTextureScene2(cam , translateWall , angleRotate , rotateValues , textureVar  , scaleValue)
    {
        
        gl.useProgram(this.adslight.getShaderProgramObject());

        // transformation
        var modelMatrix = mat4.create();
        var viewMatrix = mat4.create();
        var modelViewMatrix = mat4.create();
        var modelViewProjection = mat4.create();

        var translateMatrix = mat4.create();
        var scaleMatrix = mat4.create();
        var rotateMatrix = mat4.create();

        mat4.identity(modelMatrix);
        mat4.identity(viewMatrix);
        mat4.identity(modelViewMatrix);
        mat4.identity(modelViewProjection);
        mat4.identity(translateMatrix);
        mat4.identity(scaleMatrix);

        viewMatrix = cam.getViewMatrix();

        mat4.translate(translateMatrix , translateMatrix , translateWall );
        mat4.rotate(rotateMatrix , rotateMatrix , angleRotate , rotateValues); // source , target , valuessssssss

        mat4.scale(scaleMatrix , scaleMatrix  , scaleValue);    
        
        mat4.multiply(modelMatrix, modelMatrix, translateMatrix);
        mat4.multiply(modelMatrix, modelMatrix, rotateMatrix);
        mat4.multiply(modelMatrix, modelMatrix, scaleMatrix);


        
        gl.uniformMatrix4fv(this.adslight.modelMatrixUniform, false, modelMatrix);
        gl.uniformMatrix4fv(this.adslight.viewMatrixUniform, false, viewMatrix);
        gl.uniformMatrix4fv(this.adslight.projectionMatrixUniform, false, perspectiveProjectionMatrix);

        gl.uniform1f(this.adslight.constant, 1.0);
        gl.uniform1f(this.adslight.linear, 0.09);
        gl.uniform1f(this.adslight.quadric, 0.032);

        // gl.uniform1i(this.adslight.lightingEnabledUniform, 1);

        // gl.uniform3fv(this.adslight.lightPositionUniform, this.adslight.lightPosition);
        
        gl.uniform3fv(this.adslight.lightPositionUniform, cam.getEye());
        gl.uniform1f(this.adslight.materialShinessUniform, this.adslight.materialShininess);



        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, textureVar);
        gl.uniform1i(this.adslight.textureSamplerUniform, 0);

        this.cubeScene2.drawLitCube();

        gl.bindTexture(gl.TEXTURE_2D, null);

        gl.useProgram(null);

    }

    displayModel(camera, doGodray , translationValues , scaleValues , angleForX , angleForY , angleForZ , rotateValuesX , rotateValuesY, rotateValuesZ )
    {
                // transformation
        var modelMatrix = mat4.create();
        var translationMatrix = mat4.create();
        var scaleMatrix = mat4.create();
        var rotateMatrix = mat4.create();
        var rotationMatrixX = mat4.create();
        var rotationMatrixY = mat4.create();
        var rotationMatrixZ = mat4.create();

        mat4.identity(modelMatrix);
        mat4.identity(rotateMatrix);
        mat4.identity(rotationMatrixX);
        mat4.identity(rotationMatrixY);
        mat4.identity(rotationMatrixZ);
        mat4.identity(translationMatrix);
        mat4.identity(scaleMatrix);

        mat4.translate(translationMatrix, translationMatrix , translationValues); 
        mat4.rotate( rotationMatrixX,  rotationMatrixX, degToRad(angleForX) , rotateValuesX); 
        mat4.rotate( rotationMatrixY,  rotationMatrixY, degToRad(angleForY) , rotateValuesY); 
        mat4.rotate( rotationMatrixZ,  rotationMatrixZ, degToRad(angleForZ) , rotateValuesZ); 
        mat4.scale( scaleMatrix,  scaleMatrix, scaleValues);

        mat4.multiply(modelMatrix, modelMatrix, translationMatrix); 
        mat4.multiply(modelMatrix, modelMatrix, rotationMatrixX); 
        mat4.multiply(modelMatrix, modelMatrix, rotationMatrixY); 
        mat4.multiply(modelMatrix, modelMatrix, rotationMatrixZ); 
        mat4.multiply(modelMatrix, modelMatrix, scaleMatrix);

        
        // modelMatrix = this.modelPlacer.getModelMatrix();
        this.modelShader.renderModel(modelMatrix, camera, doGodray);

        // this.modelShader.renderModel(this.modelPlacer.getModelMatrix(), camera, doGodray);

    }

    drawJail(Camera)
    {
        //jail 1 right

        // wall left
        this.drawCubeTextureScene2(Camera , [20.0, 0.0 , 10.0] , (Math.PI * 180.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 1.0]);
        // wall right
        this.drawCubeTextureScene2(Camera , [20.0, 0.0 , -10.0] , (Math.PI * 180.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 1.0]);
        // behind wall
        this.drawCubeTextureScene2(Camera , [30.0, 0.0 , 0.0] , (Math.PI * 90.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 1.0]);
        // base
        this.drawCubeTextureScene2(Camera , [20.0, -10.0 , 0.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.floor_texture , [10.0 , 0.2 , 10.0]);
        // roof
        this.drawCubeTextureScene2(Camera , [20.0, 10.0 , 0.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.roof_texture , [10.0 , 0.2 , 10.0]);
        
        // wall between tunnel and right jail 1
        this.drawCubeTextureScene2(Camera , [10.0, 0.0 , -15.5] , (Math.PI * 90.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [4.5 , 10.0 , 0.5]);
        


        //jail 1 left 

        // wall left
        this.drawCubeTextureScene2(Camera , [-20.0, 0.0 , 10.0] , (Math.PI * 180.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 1.0]);
        // wall right
        this.drawCubeTextureScene2(Camera , [-20.0, 0.0 , -10.0] , (Math.PI * 180.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 1.0]);
        // base
        this.drawCubeTextureScene2(Camera , [-20.0, -10.0 , 0.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.floor_texture , [10.0 , 0.2 , 10.0]);
        // roof
        this.drawCubeTextureScene2(Camera , [-20.0, 10.0 , 0.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.roof_texture , [10.0 , 0.2 , 10.0]);
        // behind wall
        this.drawCubeTextureScene2(Camera , [-30.0, 0.0 , 0.0] , (Math.PI * 90.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 1.0]);
        
        // wall between tunnel and left jail 1
        this.drawCubeTextureScene2(Camera , [-10.0, 0.0 , -15.5] , (Math.PI * 90.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [4.5 , 10.0 , 0.5]);


        //jail 2 right

        // wall left
        this.drawCubeTextureScene2(Camera , [20.0, 0.0 , 20.0] , (Math.PI * 180.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 1.0]);
        // wall right
        this.drawCubeTextureScene2(Camera , [20.0, 0.0 , 40.0] , (Math.PI * 180.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 1.0]);
        // behind wall
        this.drawCubeTextureScene2(Camera , [30.0, 0.0 , 30.0] , (Math.PI * 90.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 1.0]);
        // base
        this.drawCubeTextureScene2(Camera , [20.0, -10.0 , 30.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.floor_texture , [10.0 , 0.2 , 10.0]);
        // roof
        this.drawCubeTextureScene2(Camera , [20.0, 10.0 , 30.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.roof_texture , [10.0 , 0.2 , 10.0]);
        
        // wall between tunnel and right jail 2
        this.drawCubeTextureScene2(Camera , [10.0, 0.0 , 15.5] , (Math.PI * 90.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [4.5 , 10.0 , 0.5]);


        //jail 2 left

        // wall left
        this.drawCubeTextureScene2(Camera , [-20.0, 0.0 , 20.0] , (Math.PI * 180.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 1.0]);
        // wall right
        this.drawCubeTextureScene2(Camera , [-20.0, 0.0 , 40.0] , (Math.PI * 180.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 1.0]);
        // behind wall
        this.drawCubeTextureScene2(Camera , [-30.0, 0.0 , 30.0] , (Math.PI * 90.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 1.0]);
        // base
        this.drawCubeTextureScene2(Camera , [-20.0, -10.0 , 30.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.floor_texture , [10.0 , 0.2 , 10.0]);
        // roof
        this.drawCubeTextureScene2(Camera , [-20.0, 10.0 , 30.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.roof_texture , [10.0 , 0.2 , 10.0]);
 
        // wall between tunnel and left jail 2
        this.drawCubeTextureScene2(Camera , [-10.0, 0.0 , 15.5] , (Math.PI * 90.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [4.5 , 10.0 , 0.5]);

        //jail 3 right

        // wall left
        this.drawCubeTextureScene2(Camera , [20.0, 0.0 , 70.0] , (Math.PI * 180.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 1.0]);
        // wall right
        this.drawCubeTextureScene2(Camera , [20.0, 0.0 , 50.0] , (Math.PI * 180.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 1.0]);
        // behind wall
        this.drawCubeTextureScene2(Camera , [30.0, 0.0 , 60.0] , (Math.PI * 90.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 1.0]);
        // base
        this.drawCubeTextureScene2(Camera , [20.0, -10.0 , 60.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.floor_texture , [10.0 , 0.2 , 10.0]);
        // roof
        this.drawCubeTextureScene2(Camera , [20.0, 10.0 , 60.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.roof_texture , [10.0 , 0.2 , 10.0]);

        // wall between tunnel and right jail 3
        this.drawCubeTextureScene2(Camera , [10.0, 0.0 , 45.5] , (Math.PI * 90.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [4.5 , 10.0 , 0.5]);


        //jail 3 left

        // wall left
        this.drawCubeTextureScene2(Camera , [-20.0, 0.0 , 70.0] , (Math.PI * 180.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 1.0]);
        // wall right
        this.drawCubeTextureScene2(Camera , [-20.0, 0.0 , 50.0] , (Math.PI * 180.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 1.0]);
        // behind wall
        this.drawCubeTextureScene2(Camera , [-30.0, 0.0 , 60.0] , (Math.PI * 90.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 1.0]);
        // base
        this.drawCubeTextureScene2(Camera , [-20.0, -10.0 , 60.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.floor_texture , [10.0 , 0.2 , 10.0]);
        // roof
        this.drawCubeTextureScene2(Camera , [-20.0, 10.0 , 60.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.roof_texture , [10.0 , 0.2 , 10.0]);

        // wall between tunnel and left jail 3
        this.drawCubeTextureScene2(Camera , [-10.0, 0.0 , 45.5] , (Math.PI * 90.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [4.5 , 10.0 , 0.5]);


    }

    keyEventModel(key)
    {
        // this.modelShader.keyEvent(key);
        this.modelPlacer.getKeyForTransformation(key);

    }

    drawScene(Camera)
    {

        // jail
        this.drawJail(Camera);

        // walls of tunnel
        this.drawWallsForTunnel(Camera);
       
        // floor 
        this.drawFloorAndRoofForJail(Camera);

        // mashal model
        this.drawModelInTunnel(Camera);   

             
       
        

    }

    drawWallsForTunnel(Camera)
    {

        this.setAmbient([1.0 , 1.0 , 1.0], [0.1 , 0.1 , 0.1]);
        this.setDiffuse([1.0 , 1.0 , 1.0], [0.1 , 0.1 , 0.1]);
        this.setSpecular([0.3, 0.3, 0.3], [0.7, 0.7, 0.7]);

        // left 1
        this.drawCubeTextureScene2(Camera , [-10.0, 0.0 , -30.0] , (Math.PI * 90.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 0.5]);
        // left 2
        this.drawCubeTextureScene2(Camera , [-11.0, 0.0 , -50.0] , -(Math.PI * 85.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 0.5]);
        // 3
        this.drawCubeTextureScene2(Camera , [-13.5, 0.0 , -70.0] , -(Math.PI * 80.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 0.5]);
        // 4
        this.drawCubeTextureScene2(Camera , [-18.0, 0.0 , -88.0] , -(Math.PI * 75.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 0.5]);
        // 5
        this.drawCubeTextureScene2(Camera , [-24.1, 0.0 , -107.0] , -(Math.PI * 70.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 0.5]);
        // 6
        this.drawCubeTextureScene2(Camera , [-31.5, 0.0 , -125.0] , -(Math.PI * 65.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 0.5]);
        // 7
        this.drawCubeTextureScene2(Camera , [-40.5, 0.0 , -142.0] , -(Math.PI * 60.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 0.5]);
        // 8
        this.drawCubeTextureScene2(Camera , [-50.0, 0.0 , -157.0] , -(Math.PI * 55.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 0.5]);
        // 9
        this.drawCubeTextureScene2(Camera , [-60.1, 0.0 , -170.0] , -(Math.PI * 50.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 0.5]);
        // 10
        this.drawCubeTextureScene2(Camera , [-73.0, 0.0 , -184.0] , -(Math.PI * 45.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 0.5]);


        // right 1
        this.drawCubeTextureScene2(Camera , [10.0, 0.0 , -30.0] , (Math.PI * 90.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 0.5]);
        // right 2
        this.drawCubeTextureScene2(Camera , [9.0, 0.0 , -50.0] , -(Math.PI * 85.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 0.5]);
        // 3
        this.drawCubeTextureScene2(Camera , [6.5, 0.0 , -70.0] , -(Math.PI * 80.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 0.5]);
        // 4
        this.drawCubeTextureScene2(Camera , [2.0, 0.0 , -90.0] , -(Math.PI * 75.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 0.5]);
        // 5
        this.drawCubeTextureScene2(Camera , [-4.0, 0.0 , -108.5] , -(Math.PI * 70.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 0.5]);
        // 6
        this.drawCubeTextureScene2(Camera , [-12.0, 0.0 , -126.8] , -(Math.PI * 65.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 0.5]);
        // 7
        this.drawCubeTextureScene2(Camera , [-21.0, 0.0 , -144.0] , -(Math.PI * 60.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 0.5]);
        // 8
        this.drawCubeTextureScene2(Camera , [-31.5, 0.0 , -160.0] , -(Math.PI * 55.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 0.5]);
        // 9
        this.drawCubeTextureScene2(Camera , [-44.2, 0.0 , -176.0] , -(Math.PI * 50.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [10.0 , 10.0 , 0.5]);
        // 10
        this.drawCubeTextureScene2(Camera , [-60.1, 0.0 , -193.0] , -(Math.PI * 45.0) / 180.0 , [0.0,1.0,0.0] , this.smiley_texture , [15.0 , 10.0 , 0.5]); 
    }

    drawFloorAndRoofForJail(Camera)
    {
        // draw floor 1
        this.drawCubeTextureScene2(Camera , [-0.0, -10.0 , -50.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.floor_texture , [20.0 , 0.2 , 30.0]);
        // draw floor 2
        this.drawCubeTextureScene2(Camera , [-10.0, -10.0 , -110.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.floor_texture , [20.0 , 0.2 , 30.0]);
        // draw floor 3
        this.drawCubeTextureScene2(Camera , [-30.0, -10.2 , -150.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.floor_texture , [20.0 , 0.2 , 30.0]);
        // draw floor 4
        this.drawCubeTextureScene2(Camera , [-60.0, -10.0 , -190.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.floor_texture , [20.0 , 0.2 , 30.0]);
        // draw floor 5
        this.drawCubeTextureScene2(Camera , [-100.0, -10.2 , -220.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.floor_texture , [30.0 , 0.2 , 30.0]);
        
        // draw Roof 1
        this.drawCubeTextureScene2(Camera , [-0.0, 10.0 , -50.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.roof_texture , [20.0 , 0.2 , 30.0]);
        // draw Roof 2
        this.drawCubeTextureScene2(Camera , [-10.0, 10.0 , -110.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.roof_texture , [20.0 , 0.2 , 30.0]);
        // draw Roof 3
        this.drawCubeTextureScene2(Camera , [-30.0, 10.2 , -150.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.roof_texture , [20.0 , 0.2 , 30.0]);
        // draw floor 4
        this.drawCubeTextureScene2(Camera , [-60.0, 10.0 , -190.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.roof_texture , [20.0 , 0.2 , 30.0]);
       

        // floor before tunnel
        // 1
        this.drawCubeTextureScene2(Camera , [-0.0, -10.0 , 0.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.floor_texture , [10.0 , 0.2 , 20.0]);
        //2
        this.drawCubeTextureScene2(Camera , [-0.0, -10.0 , 40.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.floor_texture , [10.0 , 0.2 , 20.0]);
        //3
        this.drawCubeTextureScene2(Camera , [-0.0, -10.0 , 80.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.floor_texture , [10.0 , 0.2 , 20.0]);
        
        // roof before tunnel
        // 1
        this.drawCubeTextureScene2(Camera , [-0.0, 10.0 , 0.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.roof_texture , [10.0 , 0.2 , 20.0]);
        //2
        this.drawCubeTextureScene2(Camera , [-0.0, 10.0 , 40.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.roof_texture , [10.0 , 0.2 , 20.0]);
        //3
        this.drawCubeTextureScene2(Camera , [-0.0, 10.0 , 80.0] , -(Math.PI * 0.0) / 180.0 , [0.0,1.0,0.0] , this.roof_texture , [10.0 , 0.2 , 20.0]);


    }

    drawModelInTunnel(Camera)
    {
        // right 1
        this.displayModel(Camera , 1 , [9.5 , 2 ,-35] , [5 ,5 ,5] , 0 , -90 , 0 , [1.0, 0.0, 0.0] , [0.0, 1.0, 0.0] , [0.0, 0.0, 1.0] );

        // right 2
        this.displayModel(Camera , 1 , [5.5 , 2 ,-74] , [5 ,5 ,5] , 0 , -90 , 0 , [1.0, 0.0, 0.0] , [0.0, 1.0, 0.0] , [0.0, 0.0, 1.0] );

        // right 3
        this.displayModel(Camera , 1 , [-3.5 , 2 ,-104] , [5 ,5 ,5] , 10 , -70 , 10 , [1.0, 0.0, 0.0] , [0.0, 1.0, 0.0] , [0.0, 0.0, 1.0] );

        // right 4
        this.displayModel(Camera , 1 , [-20 , 2 ,-141] , [5 ,5 ,5] , 10 , -70 , 10 , [1.0, 0.0, 0.0] , [0.0, 1.0, 0.0] , [0.0, 0.0, 1.0] );


        // left 1
        this.displayModel(Camera , 1 , [-9.5 , 2 ,-35] , [5 ,5 ,5] , 0 , 90 , 0 , [1.0, 0.0, 0.0] , [0.0, 1.0, 0.0] , [0.0, 0.0, 1.0] );

        // left 2
        this.displayModel(Camera , 1 , [-13.5 , 2 ,-74] , [5 ,5 ,5] , 0 , 90 , 0 , [1.0, 0.0, 0.0] , [0.0, 1.0, 0.0] , [0.0, 0.0, 1.0] );

        // left 3
        this.displayModel(Camera , 1 , [-22.5 , 2 ,-104] , [5 ,5 ,5] , 0 , 90 , 0 , [1.0, 0.0, 0.0] , [0.0, 1.0, 0.0] , [0.0, 0.0, 1.0] );

        // left 4
        this.displayModel(Camera , 1 , [-39.0 , 2 ,-141] , [5 ,5 ,5] , 0 , 90 , 0 , [1.0, 0.0, 0.0] , [0.0, 1.0, 0.0] , [0.0, 0.0, 1.0] );

    }

    uninitialize()
    {
        // code
        if (triangleScene2)
        {
            triangleScene2.uninitializeEffect();
            triangleScene2 = null;
        }
        if (this.modelShader)
        {
            delete this.modelShader;
        }

        if (this.modelPlacer)
        {
            delete this.modelPlacer;
        }
    }

    // custom ambient light and material
    setAmbient(ambientLight, ambientMaterial)
    {
        gl.useProgram(this.adslight.getShaderProgramObject());
        if (ambientLight === undefined && ambientMaterial === undefined)
        {
            gl.uniform3fv(this.adslight.laUniform, this.adslight.lightAmbient);
            gl.uniform3fv(this.adslight.kaUniform, this.adslight.materialAmbient);
        }
        else
        {
            gl.uniform3fv(this.adslight.laUniform, ambientLight);
            gl.uniform3fv(this.adslight.kaUniform, ambientMaterial);
        }
        gl.useProgram(null);
    }

    // custom diffuse light and material
    setDiffuse(diffuseLight, diffuseMaterial)
    {
        gl.useProgram(this.adslight.getShaderProgramObject());
        if (diffuseLight === undefined && diffuseMaterial === undefined)
        {
            // console.log(this.adslight.lightDiffuse);
            gl.uniform3fv(this.adslight.ldUniform, this.adslight.lightDiffuse);
            gl.uniform3fv(this.adslight.kdUniform, this.adslight.materialDiffuse);
        }
        else
        {
            gl.uniform3fv(this.adslight.ldUniform, diffuseLight);
            gl.uniform3fv(this.adslight.kdUniform, diffuseMaterial);
        }
        gl.useProgram(null);
    }

    // custom specular light and material
    setSpecular(specularLight, specularMaterial)
    {
        gl.useProgram(this.adslight.getShaderProgramObject());
        if (specularLight === undefined && specularMaterial === undefined)
        {
            gl.uniform3fv(this.adslight.lsUniform, this.adslight.lightSpecular);
            gl.uniform3fv(this.adslight.ksUniform, this.adslight.materialSpecular);
        }
        else
        {
            gl.uniform3fv(this.adslight.lsUniform, specularLight);
            gl.uniform3fv(this.adslight.ksUniform, specularMaterial);
        }
        gl.useProgram(null);
    }


}