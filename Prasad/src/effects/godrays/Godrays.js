class Godrays
{
    constructor()
    {
        this.sphere = new Mesh();
        this.fbo_colorMap = new Framebuffer();
        this.fbo_userMap = new Framebuffer();
        this.shader = new GodraysShader();
        this.colorShader = new ColorShader();
        this.model = new ModelLoader();

        // translate sun
        this.translateY = 0.0;

    }
    
    async initialize()
    {
        makeSphere(this.sphere, 2.0, 30, 30);
        this.screenQuad = new Shapes();

        // create fbo of required resolution
        this.fbo_colorMap.createFBO(1024, 1024);
        this.fbo_userMap.createFBO(1024, 1024);

        this.shader.initialize();
        this.model.initialize();
    }

    displayGodrays(cam)
    {
        // user map
        this.fbo_userMap.bindFBO(1024, 1024);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        {
            this.model.display(cam, 1);
        }
        this.fbo_userMap.unbindFBO();

        // color map
        this.fbo_colorMap.bindFBO(1024, 1024);
        gl.clearColor(0.0, 0.0, 0.0, 1.0);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        {
            gl.useProgram(this.colorShader.getShaderProgramObject());
            gl.uniform3fv(this.colorShader.colorOutUniform, [0.96, 0.84, 0.37]);

            let modelMatrix = mat4.create();
            // mat4.translate(modelMatrix, modelMatrix, [0.0, 10.0, -16.0]);
            mat4.translate(modelMatrix, modelMatrix, [0.0, this.translateY, -16.0]);
            let viewMatrix = mat4.create();
            viewMatrix = cam.getViewMatrix();
            // mat4.lookAt(viewMatrix, [0.0, 0.0, 6.0], [0.0, 0.0, 0.0], [0.0, 1.0, 0.0]);
            
            gl.uniformMatrix4fv(this.colorShader.modelMatrixUniform, false, modelMatrix);
            gl.uniformMatrix4fv(this.colorShader.viewMatrixUniform, false, viewMatrix);
            gl.uniformMatrix4fv(this.colorShader.projectionMatrixUniform, false, perspectiveProjectionMatrix);
            
            /* Display light source in expected color */
            this.sphere.draw();
            gl.useProgram(null);
            
            /* Display the scene from user map again here along with the light 
            source, but in black color which will occlude the light source */
            this.model.display(cam, 0);
        }
        this.fbo_colorMap.unbindFBO();

        // final pass
        gl.viewport(0, 0, canvas.width, canvas.height);
        gl.useProgram(this.shader.getShaderProgramObject());
        
        gl.enable(gl.BLEND);
        gl.blendFunc(gl.SRC_ALPHA, gl.ONE);

        this.shader.setUniformsForGodrays(this.fbo_userMap.getFBOTexture(), this.fbo_colorMap.getFBOTexture());
        this.screenQuad.drawQuad();

        gl.disable(gl.BLEND);

        gl.useProgram(null);

        this.update();
    }

    update()
    {
        this.translateY += 0.01;
        if (this.translateY > 15.0)
        {
            this.translateY = -10.0;
        }
        // else if (this.translateY < -15.0)
        // {
        //     this.translateY = 
        // }
    }

    uninitialize()
    {
        if (this.sphere)
        {
            delete this.sphere;
            this.sphere = null;
        }

        if (this.fbo_colorMap)
        {
            delete this.fbo_colorMap;
            this.fbo_colorMap = null;
        }

        if (this.fbo_userMap)
        {
            delete this.fbo_userMap;
            this.fbo_userMap = null;
        }

        if (this.shader)
        {
            this.shader.uninitializeShader();
            this.shader = null;
        }

        if (this.colorShader)
        {
            this.colorShader.uninitializeShader();
            this.colorShader = null;
        }

        if (this.model)
        {
            this.model.uninitialize();
            this.model = null;
        }
    }
};