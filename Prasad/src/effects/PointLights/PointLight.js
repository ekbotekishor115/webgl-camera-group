class PointLight
{
    constructor()
    {
        this.adslight;
        this.shape;
    }

    async initialize()
    {
        this.adslight = new PointLightShader();
        this.shape = new Shapes();

        this.shape.initialize();

    }   

    display(cam)
    {
        // variables
        let modelMatrix = mat4.create();
        let viewMatrix = mat4.create();

        // code
        mat4.identity(modelMatrix);
        mat4.rotateX(modelMatrix, modelMatrix, degToRad(time));
        mat4.rotateY(modelMatrix, modelMatrix, degToRad(time));
        mat4.rotateZ(modelMatrix, modelMatrix, degToRad(time));

        viewMatrix = cam.getViewMatrix();

        gl.useProgram(this.adslight.getShaderProgramObject());

        gl.uniformMatrix4fv(this.adslight.modelMatrixUniform, false, modelMatrix);
        gl.uniformMatrix4fv(this.adslight.viewMatrixUniform, false, viewMatrix);
        gl.uniformMatrix4fv(this.adslight.projectionMatrixUniform, false, perspectiveProjectionMatrix);

        gl.uniform1i(this.adslight.lightingEnabledUniform, 1);

        // gl.uniform3fv(this.adslight.lightPositionUniform, this.adslight.lightPosition);
        
        gl.uniform3fv(this.adslight.lightPositionUniform, cam.getEye());
        gl.uniform1f(this.adslight.materialShinessUniform, this.adslight.materialShininess);


        // this.setAmbient(this.adslight.lightAmbient , this.adslight.materialAmbient );
        // this.setDiffuse(this.adslight.lightDiffuse , this.adslight.materialDiffuse );
        // this.setSpecular(this.adslight.lightSpecular , this.adslight.materialSpecular );

        this.shape.drawLitCube();

        gl.useProgram(null);

    }

    // custom ambient light and material
    setAmbient(ambientLight, ambientMaterial)
    {
        gl.useProgram(this.adslight.getShaderProgramObject());
        if (ambientLight === undefined && ambientMaterial === undefined)
        {
            gl.uniform3fv(this.adslight.laUniform, this.adslight.lightAmbient);
            gl.uniform3fv(this.adslight.kaUniform, this.adslight.materialAmbient);
        }
        else
        {
            gl.uniform3fv(this.adslight.laUniform, ambientLight);
            gl.uniform3fv(this.adslight.kaUniform, ambientMaterial);
        }
        gl.useProgram(null);
    }

    // custom diffuse light and material
    setDiffuse(diffuseLight, diffuseMaterial)
    {
        gl.useProgram(this.adslight.getShaderProgramObject());
        if (diffuseLight === undefined && diffuseMaterial === undefined)
        {
            console.log(this.adslight.lightDiffuse);
            gl.uniform3fv(this.adslight.ldUniform, this.adslight.lightDiffuse);
            gl.uniform3fv(this.adslight.kdUniform, this.adslight.materialDiffuse);
        }
        else
        {
            gl.uniform3fv(this.adslight.ldUniform, diffuseLight);
            gl.uniform3fv(this.adslight.kdUniform, diffuseMaterial);
        }
        gl.useProgram(null);
    }

    // custom specular light and material
    setSpecular(specularLight, specularMaterial)
    {
        gl.useProgram(this.adslight.getShaderProgramObject());
        if (specularLight === undefined && specularMaterial === undefined)
        {
            gl.uniform3fv(this.adslight.lsUniform, this.adslight.lightSpecular);
            gl.uniform3fv(this.adslight.ksUniform, this.adslight.materialSpecular);
        }
        else
        {
            gl.uniform3fv(this.adslight.lsUniform, specularLight);
            gl.uniform3fv(this.adslight.ksUniform, specularMaterial);
        }
        gl.useProgram(null);
    }

    uninitialize()
    {
        this.adslight.uninitializeShader();
        if (this.adslight)
        {
            delete this.adslight;
            this.adslight = null;
        }
    }
};