class Triangle
{
    constructor()
    {
        this.shader = new TriangleShader();
        this.shaderProgramObject;
        this.vao;
        this.vbo;
    }

    initialize()
    {
        this.shaderProgramObject = this.shader.getShaderProgramObject();

        // geometry attribute declaration
        var trianglePosition = new Float32Array(
            [
                0.0, 1.0, 0.0,
                -1.0, -1.0, 0.0,
                1.0, -1.0, 0.0
            ]
        );

        // vao
        this.vao = gl.createVertexArray();
        gl.bindVertexArray(this.vao);

        // vbo pos
        this.vbo = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo);
        gl.bufferData(gl.ARRAY_BUFFER, trianglePosition, gl.STATIC_DRAW);
        gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        gl.bindVertexArray(null);

    }

    display(cam , [x, y , z])
    {
        gl.useProgram(this.shaderProgramObject);

        // transformation
        var modelMatrix = mat4.create();
        var viewMatrix = mat4.create();
        var modelViewMatrix = mat4.create();
        var modelViewProjection = mat4.create();

        mat4.identity(modelMatrix);
        mat4.identity(viewMatrix);
        mat4.identity(modelViewMatrix);
        mat4.identity(modelViewProjection);

        mat4.translate(modelMatrix , modelMatrix , [x , y , z] );



        viewMatrix = cam.getViewMatrix();
        
        mat4.multiply(modelViewMatrix, modelMatrix, viewMatrix);
        mat4.multiply(modelViewProjection, perspectiveProjectionMatrix, modelViewMatrix);
        gl.uniformMatrix4fv(this.shader.mvpMatrixUniform, false, modelViewProjection);

        gl.bindVertexArray(this.vao);
        gl.drawArrays(gl.TRIANGLES, 0, 3);
        gl.bindVertexArray(null);

        gl.useProgram(null);

    }

    update()
    {
        // code
    }

    uninitializeEffect()
    {
        if (this.shader)
        {
            this.shader.uninitializeShader();
            this.shader = null;
        }

        if (this.vbo)
        {
            gl.deleteBuffer(this.vbo);
            this.vbo = null;
        }
        
        if (this.vao)
        {
            gl.deleteVertexArray(this.vao);
            this.vao = null;
        }
    }
}