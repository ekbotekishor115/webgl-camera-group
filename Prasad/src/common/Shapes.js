class Shapes
{
    constructor()
    {

        this.vao_cube;
        this.vbo_cube_position;
        this.vbo_cube_texcoord;
        this.vbo_cube_normal;

        this.vao_quad;
        this.vbo_quad_position;
        this.vbo_quad_texcoord;



    }

    initialize()
    {

        var cubePosition = new Float32Array([
    
			// top
            1.0, 1.0, -1.0,
            -1.0, 1.0, -1.0,
            -1.0, 1.0, 1.0,
            1.0, 1.0, 1.0,
    
                // bottom
                1.0, -1.0, -1.0,
            -1.0, -1.0, -1.0,
            -1.0, -1.0,  1.0,
                1.0, -1.0,  1.0,
    
                // front
                1.0, 1.0, 1.0,
                -1.0, 1.0, 1.0,
                -1.0, -1.0, 1.0,
                1.0, -1.0, 1.0,
    
                // back
                1.0, 1.0, -1.0,
            -1.0, 1.0, -1.0,
            -1.0, -1.0, -1.0,
                1.0, -1.0, -1.0,
    
                // right
                1.0, 1.0, -1.0,
                1.0, 1.0, 1.0,
                1.0, -1.0, 1.0,
                1.0, -1.0, -1.0,
    
                // left
            -1.0, 1.0, 1.0,
            -1.0, 1.0, -1.0,
            -1.0, -1.0, -1.0,
            -1.0, -1.0, 1.0
    ]);
    
        var cubeTexcoord = new Float32Array([
            1.0, 1.0,
            0.0, 1.0,
            0.0, 0.0,
            1.0, 0.0,
    
            1.0, 1.0,
            0.0, 1.0,
            0.0, 0.0,
            1.0, 0.0,
    
            1.0, 1.0,
            0.0, 1.0,
            0.0, 0.0,
            1.0, 0.0,
    
            1.0, 1.0,
            0.0, 1.0,
            0.0, 0.0,
            1.0, 0.0,
    
            1.0, 1.0,
            0.0, 1.0,
            0.0, 0.0,
            1.0, 0.0,
    
            1.0, 1.0,
            0.0, 1.0,
            0.0, 0.0,
            1.0, 0.0
        ]);

        var cubeNormals = new Float32Array([
            // front surface
            0.0,  0.0,  1.0, // top-right of front
            0.0,  0.0,  1.0, // top-left of front
            0.0,  0.0,  1.0, // bottom-left of front
            0.0,  0.0,  1.0, // bottom-right of front

            // right surface
            1.0,  0.0,  0.0, // top-right of right
            1.0,  0.0,  0.0, // top-left of right
            1.0,  0.0,  0.0, // bottom-left of right
            1.0,  0.0,  0.0, // bottom-right of right

            // back surface
            0.0,  0.0, -1.0, // top-right of back
            0.0,  0.0, -1.0, // top-left of back
            0.0,  0.0, -1.0, // bottom-left of back
            0.0,  0.0, -1.0, // bottom-right of back

            // left surface
            -1.0,  0.0,  0.0, // top-right of left
            -1.0,  0.0,  0.0, // top-left of left
            -1.0,  0.0,  0.0, // bottom-left of left
            -1.0,  0.0,  0.0, // bottom-right of left

            // top surface
            0.0,  1.0,  0.0, // top-right of top
            0.0,  1.0,  0.0, // top-left of top
            0.0,  1.0,  0.0, // bottom-left of top
            0.0,  1.0,  0.0, // bottom-right of top

            // bottom surface
            0.0, -1.0,  0.0, // top-right of bottom
            0.0, -1.0,  0.0, // top-left of bottom
            0.0, -1.0,  0.0, // bottom-left of bottom
            0.0, -1.0,  0.0 // bottom-right of bottom
    ]);



        // vao Cube
        this.vao_cube = gl.createVertexArray();
        gl.bindVertexArray(this.vao_cube);
    
        // vbo position
        this.vbo_cube_position = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_cube_position);
        gl.bufferData(gl.ARRAY_BUFFER, cubePosition, gl.STATIC_DRAW);
        gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION, 
            3, 
            gl.FLOAT, 
            false, 
            0, 
            0);
        gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
    
        // vbo texcoord
        this.vbo_cube_texcoord = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_cube_texcoord);
        gl.bufferData(gl.ARRAY_BUFFER, cubeTexcoord, gl.STATIC_DRAW);
        gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS, 
            2, 
            gl.FLOAT, 
            false, 
            0, 
            0);
        gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        
        // vbo normal
        this.vbo_cube_normal = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_cube_normal);
        gl.bufferData(gl.ARRAY_BUFFER, cubeNormals, gl.STATIC_DRAW);
        gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_NORMAL,
            3, 
            gl.FLOAT, 
            false, 
            0, 
            0);
        gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_NORMAL);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);


    
        // unbind vao
        gl.bindVertexArray(null);


        //================= Quad ==================//
        var position = new Float32Array([
            1.0, 1.0, 0.0,
            -1.0, 1.0, 0.0,
            -1.0, -1.0, 0.0,
            1.0, -1.0, 0.0]);

        var texcoord = new Float32Array([
            1.0, 1.0,
            0.0, 1.0,
            0.0, 0.0,
            1.0, 0.0]);

        this.vao_quad = gl.createVertexArray();
        gl.bindVertexArray(this.vao_quad);
        {
            this.vbo_quad_position = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_quad_position);
            gl.bufferData(gl.ARRAY_BUFFER, position, gl.STATIC_DRAW);
            gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
            gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);

            this.vbo_quad_texcoord = gl.createBuffer();
            gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_quad_texcoord);
            gl.bufferData(gl.ARRAY_BUFFER, texcoord, gl.STATIC_DRAW);
            gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS, 2, gl.FLOAT, false, 0, 0);
            gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS);
            gl.bindBuffer(gl.ARRAY_BUFFER, null);
        }
        gl.bindVertexArray(null);


    }

    drawCube()
    {
        gl.bindVertexArray(this.vao_cube);
        
        gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
        gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
        gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
        gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
        gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
        gl.drawArrays(gl.TRIANGLE_FAN, 24, 4);

        gl.bindVertexArray(null);
    }

    drawLitCube()
    {
        gl.bindVertexArray(this.vao_cube);
        
        gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
        gl.drawArrays(gl.TRIANGLE_FAN, 4,4);
        gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
        gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
        gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
        gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);

        gl.bindVertexArray(null);
    }

    drawTextureCube()
    {
        // gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);


        gl.bindVertexArray(this.vao_cube);

        gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
        gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
        gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
        gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
        gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
        gl.drawArrays(gl.TRIANGLE_FAN, 24, 4);

        gl.bindVertexArray(null);
        

    } 

    drawQuad()
    {

        gl.bindVertexArray(this.vao_quad);
        gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
        gl.bindVertexArray(null);

    }

    uninitializeCube()
    {
        if (this.vbo_cube_position)
        {
            gl.deleteBuffer(this.vbo_cube_position);
            this.vbo_cube_position = null;
        }
    
        if (this.vbo_cube_texcoord)
        {
            gl.deleteBuffer(this.vbo_cube_texcoord);
            this.vbo_cube_texcoord = null;
        }
    
        if (this.vao_cube)
        {
            gl.deleteVertexArray(this.vao_cube);
            this.vao_cube = null;
        }

        if (this.shader)
        {
            this.shader.uninitializeShader();
            this.shader = null;
        }

    }
};