class PointLightShader
{
    constructor()
    {
        this.shaderProgramObject;
        this.vao;
        this.vbo_position;
        this.vbo_texcoord;
        this.perspectiveProjectionMatrix;



        this.modelMatrixUniform;
        this.viewMatrixUniform;
        this.projectionMatrixUniform;
        this.textureSamplerUniform;

        this.laUniform;
        this.ldUniform;
        this.lsUniform;

        this.kaUniform;
        this.kdUniform;
        this.ksUniform;


        this.constant;
        this.linear;
        this.quadric;

        this.materialShinessUniform;
        this.lightPositionUniform;

        this.lightAmbient = new Float32Array([0.1, 0.1, 0.1]);
        this.lightDiffuse = new Float32Array([1.0, 1.0, 1.0]);
        this.lightSpecular = new Float32Array([1.0, 1.0, 1.0]);
        this.lightPosition = new Float32Array([100.0, 100.0, 100.0]);

        this.materialAmbient = new Float32Array([0.0, 0.0, 0.0]);
        this.materialDiffuse = new Float32Array([0.5, 0.2, 0.7]);
        this.materialSpecular = new Float32Array([0.7, 0.7, 0.7]);
        this.materialShininess = 128.0;
        
        // vertex shader
        var vertexShaderSourceCode = 
        `#version 300 es
        \n
        in vec2 aTexCoord;
		out vec2 oTexCoord;
        in vec4 aPosition;
        in vec3 aNormal;
        uniform mat4 uModelMatrix;
        uniform mat4 uViewMatrix;
        uniform mat4 uProjectionMatrix;
        uniform vec4 uLightposition;
        out vec3 otransformedNormals;
        out vec3 olightDirection;
        out vec3 oviewerVector;
        out vec4 eyeCoordinates;
        in vec4 aColor;
        out vec4 oColor;
        void main(void)
        {
        eyeCoordinates =  uViewMatrix * uModelMatrix * aPosition;
        otransformedNormals = mat3(uViewMatrix * uModelMatrix) * aNormal;
        olightDirection = vec3(uLightposition - eyeCoordinates);
        oviewerVector = -eyeCoordinates.xyz;
        gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * aPosition;
        oTexCoord = aTexCoord;
        }`;

        // vertex shader object
        this.vertexShaderObject = this.createShaderObjects(vertexShaderSourceCode, gl.VERTEX_SHADER);

        // fragment shader
        var fragmentShaderSourceCode = 
        `#version 300 es
        \n
        precision highp float;
        uniform highp sampler2D uTextureSampler;
        in vec2 oTexCoord;
        in vec4 oColor;
        in vec3 otransformedNormals;
        in vec3 olightDirection;
        in vec3 oviewerVector;
        in vec4 eyeCoordinates;
        out vec4 FragColor;
        uniform vec3 uLightAmbient;
        uniform vec3 uLightDiffuse;
        uniform vec3 uLightSpecular;
        uniform vec3 uMaterialAmbient;
        uniform vec3 uMaterialDiffuse;
        uniform vec3 uMaterialSpecular;
        uniform float uMaterialShineness;
        uniform vec4 uLightposition;
        uniform float constant;
        uniform float linear;
        uniform float quadratic;
        void main(void)
        {
        vec3 Phong_ADS_Light;
        vec3 normalizedTranformedNormals = normalize(otransformedNormals);
        vec3 normalizedLightDirection = normalize(olightDirection);
        vec3 normalizedViewerVector= normalize(oviewerVector);
        vec3 ambientLight = uLightAmbient * uMaterialAmbient;
        vec3 diffuseLight = uLightDiffuse * uMaterialDiffuse * max(dot(normalizedLightDirection , normalizedTranformedNormals) , 0.0f); 
        vec3 reflectionVector = reflect(-normalizedLightDirection , normalizedTranformedNormals);
        vec3 specularLight = uLightSpecular * uMaterialSpecular * pow(max(dot(reflectionVector , normalizedViewerVector) , 0.0f) , uMaterialShineness); 
        float distance    = length(uLightposition - eyeCoordinates);
        float attenuation = 1.0 / (constant + linear * distance + quadratic * (distance * distance));
        ambientLight  *= attenuation;
        diffuseLight   *= attenuation;
        specularLight *= attenuation;
        Phong_ADS_Light = ambientLight + diffuseLight + specularLight;
        FragColor = texture(uTextureSampler , oTexCoord) + vec4(Phong_ADS_Light, 1.0);
        }`;

        // fragment shader object
        this.fragmentShaderObject = this.createShaderObjects(fragmentShaderSourceCode, gl.FRAGMENT_SHADER);

        // Attach and Link shaderObjects
        this.shaderProgramObject = attachAndLinkShaderObjects(
            [this.vertexShaderObject, this.fragmentShaderObject],
            [VertexAttributeEnum.CAM_ATTRIBUTE_POSITION, VertexAttributeEnum.CAM_ATTRIBUTE_NORMAL , VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS ],
            ["aPosition", "aNormal" , "aTexCoord"]
        );
        if (this.shaderProgramObject == null) {
            alert("ShaderProgramObject failed");
        }

        // uniforms
        this.modelMatrixUniform = gl.getUniformLocation(this.shaderProgramObject, "uModelMatrix");
        this.viewMatrixUniform = gl.getUniformLocation(this.shaderProgramObject, "uViewMatrix");
        this.projectionMatrixUniform = gl.getUniformLocation(this.shaderProgramObject, "uProjectionMatrix");
        this.textureSamplerUniform = gl.getUniformLocation(this.shaderProgramObject, "uTextureSampler");

        this.laUniform = gl.getUniformLocation(this.shaderProgramObject, "uLightAmbient");
        this.ldUniform = gl.getUniformLocation(this.shaderProgramObject, "uLightDiffuse");
        this.lsUniform = gl.getUniformLocation(this.shaderProgramObject, "uLightSpecular");

        this.kaUniform = gl.getUniformLocation(this.shaderProgramObject, "uMaterialAmbient");
        this.kdUniform = gl.getUniformLocation(this.shaderProgramObject, "uMaterialDiffuse");
        this.ksUniform = gl.getUniformLocation(this.shaderProgramObject, "uMaterialSpecular");

        this.constant = gl.getUniformLocation(this.shaderProgramObject, "constant");
        this.linear = gl.getUniformLocation(this.shaderProgramObject, "linear");
        this.quadric = gl.getUniformLocation(this.shaderProgramObject, "quadratic");

        this.materialShinessUniform = gl.getUniformLocation(this.shaderProgramObject, "uMaterialShineness");
        this.lightPositionUniform = gl.getUniformLocation(this.shaderProgramObject, "uLightposition");  

    }

    initialize()
    {
        
    }

    createShaderObjects(shaderStr, shaderType)
    {
        let shaderObject = createAndCompileShaders(shaderStr, shaderType);
        if (shaderObject == null) 
        {
            alert(shaderType + " Shader Compilation failed !!!");
            return null;
        }

        return shaderObject;
    }

    getUniformLocation(uniform)
    {
        return gl.getUniformLocation(this.shaderProgramObject, uniform);
    }

    getShaderProgramObject()
    {
        return this.shaderProgramObject;
    }

    uninitializeShader()
    {
        uninitializeShaders(this.shaderProgramObject);
    }

};