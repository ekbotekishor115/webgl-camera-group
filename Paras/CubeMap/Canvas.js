
var canvas = null;
var gl = null;
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

// WebGL related variables
const vertexAttributeEnum =
{
	AMC_ATTRIBUTE_POSITION: 0,// kvc (key value coding)
	AMC_ATTRIBUTE_COLOR: 1,
	AMC_ATTRIBUTE_TEXTURE: 2
};

var shaderProgramObject = null;


var vao_cube = null;
var vbo_cube_position = null;
var vbo_cube_texcoord = null;

var mvpMatrixUniform;
var textureSamplerUniform;

var perspectiveProjectionMatrix;

var modelViewMatrix;

var requestAnimationFrame =
	window.requestAnimationFrame ||
	window.webkitRequestAnimationFrame ||
	window.mozRequestAnimationFrame ||
	window.oRequestAnimationFrame ||
	window.msRequestAnimationFrame;

var angle = 0.0;

var skyBox;
var texture;

// Our main function
function main() {
	// Get Canvas
	canvas = document.getElementById("PVS");
	if (canvas == null) {
		console.log("Getting canvas failed\n");
	}
	else {
		console.log("Getting canvas succeded\n");
	}

	// Set canvas width and height for future use
	canvas_original_width = canvas.width;
	canvas_original_height = canvas.height;



	// Register for keyboard events
	window.addEventListener("keydown", keyDown, false);

	// Register for mouse
	window.addEventListener("click", mouseDown, false);

	window.addEventListener("resize", resize, false);

	initialize();

	resize();

	display();
}

function toggleFullscreen() {
	var fullscreen_element =
		document.fullscreenElement ||
		document.webkitFullscreenElement ||
		document.mozFullScreenElement ||
		document.msFullscreenElement ||
		null;

	if (fullscreen_element == null) {
		if (canvas.requestFullscreen) {
			canvas.requestFullscreen();
		}
		else if (canvas.webkitRequestFullscreen) {
			canvas.webkitRequestFullscreen();
		}
		else if (canvas.mozRequestFullScreen) {
			canvas.mozRequestFullScreen();
		}
		else if (canvas.msRequestFullscreen) {
			canvas.msRequestFullscreen();
		}
		bFullscreen = true;
	}
	else {
		if (document.exitFullscreen) {
			document.exitFullscreen();
		}
		else if (document.webkitExitFullscreen) {
			document.webkitExitFullscreen();
		}
		else if (document.mozCancelFullScreen) {
			document.mozCancelFullScreen();
		}
		else if (document.msExitFullScreen) {
			document.msExitFullScreen();
		}
		bFullscreen = false;

	}

}

function keyDown(event) {
	// Code
	switch (event.keyCode) {
		case 81:
		case 113:
			uninitialize();
			window.close();
			break;

		case 70:
		case 102:
			toggleFullscreen();
			break;
	}
}

function mouseDown() {
}

function initialize() {
	// Code
	// Get Context from above canvas
	gl = canvas.getContext("webgl2");
	if (gl == null) {
		console.log("Getting webGL 2  failed\n");
	}
	else {
		console.log("Getting webGL 2 succeded\n");

	}

	// Sent webGL2 context's view width and vie
	gl.viewportWidth = canvas.width;
	gl.viewportHeight = canvas.height;

	// Vertex shader
	var vertexShaderSourceCode =
		"#version 300 es" +
		"\n" +
		"in vec4 aPosition;" +
		"in vec2 aTexture;" +
		"out vec2 oTexture;" +
		"uniform mat4 uMVPMatrix;" +
		"void main(void)" +
		"{" +
		"gl_Position = uMVPMatrix * aPosition;" +
		"oTexture = aTexture;" +
		"}";

	var vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShaderObject, vertexShaderSourceCode);
	gl.compileShader(vertexShaderObject);

	if (gl.getShaderParameter(vertexShaderObject, gl.COMPILE_STATUS) == false) {
		var error = gl.getShaderInfoLog(vertexShaderObject);
		if (error.length > 0) {
			var log = "Vertex Shader Comiplation error : " + error;
			alert(log);
			uninitialize();
		}
	}
	else {
		console.log("Vertex Shader Success\n");
	}

	// Fragment shader
	var fragmentShaderSourceCode =
		"#version 300 es" +
		"\n" +
		"precision highp float;" +
		"in vec2 oTexture;" +
		"uniform sampler2D uTextureSampler;" +
		"out vec4 fragColor;" +
		"void main(void)" +
		"{" +
		"fragColor = texture(uTextureSampler,oTexture);" +
		"}";

	var fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShaderObject, fragmentShaderSourceCode);
	gl.compileShader(fragmentShaderObject);

	if (gl.getShaderParameter(fragmentShaderObject, gl.COMPILE_STATUS) == false) {
		var error = gl.getShaderInfoLog(fragmentShaderObject);
		if (error.length > 0) {
			var log = "fragment Shader Comiplation error : " + error;
			alert(log);
			uninitialize();
		}
	}
	else {
		console.log("Fragment Shader Success\n");
	}

	// Shader program
	shaderProgramObject = gl.createProgram();

	gl.attachShader(shaderProgramObject, vertexShaderObject);
	gl.attachShader(shaderProgramObject, fragmentShaderObject);

	gl.bindAttribLocation(shaderProgramObject, vertexAttributeEnum.AMC_ATTRIBUTE_POSITION, "aPosition");
	gl.bindAttribLocation(shaderProgramObject, vertexAttributeEnum.AMC_ATTRIBUTE_TEXTURE, "aTexture");

	gl.linkProgram(shaderProgramObject);

	if (gl.getProgramParameter(shaderProgramObject, gl.LINK_STATUS) == false) {
		var error = gl.getProgramInfoLog(shaderProgramObject);
		if (error.length > 0) {
			var log = "Shader Linking error : " + error;
			alert(log);
			uninitialize();
		}
	}
	else {
		console.log("Shader Linking Success\n");
	}

	mvpMatrixUniform = gl.getUniformLocation(shaderProgramObject, "uMVPMatrix");
	textureSamplerUniform = gl.getUniformLocation(shaderProgramObject, "uTextureSampler");

	// geometry attribute declaration
	
	var cube_position = new Float32Array([
		1.0, 1.0, 1.0,
		-1.0, 1.0, 1.0,
		-1.0, -1.0, 1.0,
		1.0, -1.0, 1.0,

		1.0, 1.0, 1.0,
		1.0, -1.0, 1.0,
		1.0, -1.0, -1.0,
		1.0, 1.0, -1.0,

		1.0, 1.0, -1.0,
		-1.0, 1.0, -1.0,
		-1.0, -1.0, -1.0,
		1.0, -1.0, -1.0,

		-1.0, 1.0, 1.0,
		-1.0, -1.0, 1.0,
		-1.0, -1.0, -1.0,
		-1.0, 1.0, -1.0,

		1.0, 1.0, 1.0,
		-1.0, 1.0, 1.0,
		-1.0, 1.0, -1.0,
		1.0, 1.0, -1.0,

		1.0, -1.0, 1.0,
		-1.0, -1.0, 1.0,
		-1.0, -1.0, -1.0,
		1.0, -1.0, -1.0,

	]);
	var cube_texcoord = new Float32Array([
		// front
		1.0, 1.0, // top-right of front
		0.0, 1.0, // top-left of front
		0.0, 0.0, // bottom-left of front
		1.0, 0.0, // bottom-right of front

		// right
		1.0, 1.0, // top-right of right
		0.0, 1.0, // top-left of right
		0.0, 0.0, // bottom-left of right
		1.0, 0.0, // bottom-right of right

		// back
		1.0, 1.0, // top-right of back
		0.0, 1.0, // top-left of back
		0.0, 0.0, // bottom-left of back
		1.0, 0.0, // bottom-right of back

		// left
		1.0, 1.0, // top-right of left
		0.0, 1.0, // top-left of left
		0.0, 0.0, // bottom-left of left
		1.0, 0.0, // bottom-right of left

		// top
		1.0, 1.0, // top-right of top
		0.0, 1.0, // top-left of top
		0.0, 0.0, // bottom-left of top
		1.0, 0.0, // bottom-right of top

		// bottom
		1.0, 1.0, // top-right of bottom
		0.0, 1.0, // top-left of bottom
		0.0, 0.0, // bottom-left of bottom
		1.0, 0.0, // bottom-right of bottom
	]);

	// vao_cube
	vao_cube = gl.createVertexArray();
	gl.bindVertexArray(vao_cube);

	// vbo_cube_position
	vbo_cube_position = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_cube_position);

	gl.bufferData(gl.ARRAY_BUFFER, cube_position, gl.STATIC_DRAW);
	gl.vertexAttribPointer(vertexAttributeEnum.AMC_ATTRIBUTE_POSITION,
		3,
		gl.FLOAT,
		false,
		0,
		0);
	gl.enableVertexAttribArray(vertexAttributeEnum.AMC_ATTRIBUTE_POSITION);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	// vbo cube texcoord
	vbo_cube_texcoord = gl.createBuffer();
	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_cube_texcoord);

	gl.bufferData(gl.ARRAY_BUFFER, cube_texcoord, gl.STATIC_DRAW);
	gl.vertexAttribPointer(vertexAttributeEnum.AMC_ATTRIBUTE_TEXTURE,
		2,
		gl.FLOAT,
		false,
		0,
		0);
	gl.enableVertexAttribArray(vertexAttributeEnum.AMC_ATTRIBUTE_TEXTURE);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	// Unbind vertex array
	gl.bindVertexArray(null);

	skyBox = loadGLTexture();

	texture = gl.createTexture();
	gl.bindTexture(gl.TEXTURE_CUBE_MAP, texture);

	const faceInfos = [
		{
			target: gl.TEXTURE_CUBE_MAP_POSITIVE_X,
			src: 'right.png',
		},
		{
			target: gl.TEXTURE_CUBE_MAP_NEGATIVE_X,
			src: 'left.png',
		},
		{
			target: gl.TEXTURE_CUBE_MAP_POSITIVE_Y,
			src: 'top.png',
		},
		{
			target: gl.TEXTURE_CUBE_MAP_NEGATIVE_Y,
			src: 'bottom.png',
		},
		{
			target: gl.TEXTURE_CUBE_MAP_POSITIVE_Z,
			src: 'front.png',
		},
		{
			target: gl.TEXTURE_CUBE_MAP_NEGATIVE_Z,
			src: 'back.png',
		},
	];

	faceInfos.forEach((faceInfo) => {
		const { target, src } = faceInfo;

		// Upload the canvas to the cubemap face.
		const width = 512;
		const height = 512;

		// setup each face so it's immediately renderable
		gl.texImage2D(target, 0, gl.RGBA, width, height, 0, gl.RGBA, gl.UNSIGNED_BYTE, null);

		// Asynchronously load an image
		const image = new Image();
		image.src = src;
		image.addEventListener('load', function () {
			// Now that the image has loaded make copy it to the texture.
			gl.bindTexture(gl.TEXTURE_CUBE_MAP, texture);
			gl.texImage2D(target, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
			gl.generateMipmap(gl.TEXTURE_CUBE_MAP);
		});
	});

	gl.generateMipmap(gl.TEXTURE_CUBE_MAP);
	gl.texParameteri(gl.TEXTURE_CUBE_MAP, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);


	gl.enable(gl.CULL_FACE);
	// Depth related line
	gl.clearDepth(1.0);
	gl.enable(gl.DEPTH_TEST);
	gl.depthFunc(gl.LEQUAL);

	// Set clear color
	gl.clearColor(0.0, 0.0, 1.0, 1.0);

	// initialize matrix
	perspectiveProjectionMatrix = mat4.create();

}
function loadGLTexture() {
	
}

function resize() {
	// Code
	if (bFullscreen == true) {
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;
	}
	else {
		canvas.width = canvas_original_width;
		canvas.height = canvas_original_height;
	}

	// Set viewport
	gl.viewport(0, 0, canvas.width, canvas.height);

	mat4.perspective(perspectiveProjectionMatrix, 45.0, parseFloat(canvas.width) / parseFloat(canvas.height), 0.1, 100.0);

}

function display() {
	// Code
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	gl.useProgram(shaderProgramObject);

	// transformation
	modelViewMatrix = mat4.create();
	var modelViewProjectionMatrix = mat4.create();

	// Cube
	mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -5.0]);
	mat4.scale(modelViewMatrix, modelViewMatrix, [5.0, 5.0, 5.0]);
	mat4.rotateY(modelViewMatrix, modelViewMatrix, regToRad(angle));
	mat4.multiply(modelViewProjectionMatrix, perspectiveProjectionMatrix, modelViewMatrix);

	gl.uniformMatrix4fv(mvpMatrixUniform, false, modelViewProjectionMatrix);

	gl.activeTexture(gl.TEXTURE0);
	gl.bindTexture(gl.TEXTURE_CUBE_MAP, texture);
	gl.uniform1i(textureSamplerUniform, 0);

	gl.bindVertexArray(vao_cube);
	gl.drawArrays(gl.TRIANGLE_FAN, 0, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 4, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 8, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 12, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 16, 4);
	gl.drawArrays(gl.TRIANGLE_FAN, 20, 4);
	gl.bindVertexArray(null);

	gl.useProgram(null);

	// Double buffering
	requestAnimationFrame(display, canvas);
	update();
}

function regToRad(degree) {
	return (degree / 3.14 * 180.0);
}
function update() {
	// Code
	angle = angle + 0.0005;
}
function uninitialize() {
	// Code
	if (shaderProgramObject) {
		gl.useProgram(shaderProgramObject);
		var shaderObjects = gl.getAttachedShaders(shaderProgramObject);

		if (shaderObjects && shaderObjects.length > 0) {

			for (let i = 0; i < shaderObjects.length;) {
				gl.detachShader(shaderProgramObject, shaderObjects[i]);
				gl.deleteShader(shaderObjects[i]);
				shaderObjects[i];
			}

		}
		gl.useProgram(null);
		gl.deleteProgram(shaderProgramObject);
		shaderProgramObject = null;
	}
	if (vbo_cube_texcoord) {
		gl.deleteBuffers(1, vbo_cube_texcoord);
		vbo_cube_texcoord = 0;

	}
	if (vbo_cube_position) {
		gl.deleteBuffers(1, vbo_cube_position);
		vbo_cube_position = 0;

	}
	if (vao_cube) {
		gl.deleteVertexArrays(1, vao_cube);
		vao_cube = 0;
	}
}
