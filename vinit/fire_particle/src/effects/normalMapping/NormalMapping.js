class NormalMapping
{
    constructor()
    {
        this.wall;

        this.normalMapShader = null;
    }

    async initialize()
    {
        this.wall = new Shapes();
        // this.wall.initQuadForNormalMapping();

        this.normalMapShader = new NormalMappingShader();

        // texture
        this.texture = loadTextureFromFile('./assets/textures/house/wall.jpg');
        this.texture_nmap = loadTextureFromFile('./assets/textures/house/wall_normal.jpg');
        // this.texture = createTexture('./assets/textures/brickwall.jpg');
        // this.texture_nmap = createTexture('./assets/textures/brickwall_normal.jpg');
        // this.texture = loadTextureFromFile('./assets/models/stone/textures/boulder_01_diff_2k.jpg');
        // this.texture_nmap = loadTextureFromFile('./assets/models/stone/textures/boulder_01_nor_gl_2k.png');

        
    }

    displayNMap(cam)
    {
        var modelMatrix = mat4.create();
        var viewMatrix = mat4.create();
        var translationMatrix = mat4.create();
        var rotationMatrixY = mat4.create();
        var scaleMatrix = mat4.create();

        // need to pass this tiling array for 4 faces of cube
        let tileingArray = [2.5, 2.5, 2.5, 2.5, 0.4, 2.0, 0.4, 2.0];
        
        // back wall ===========================================================================
        mat4.translate(translationMatrix, translationMatrix , [0.0 ,0.0 ,-1.0]);
        mat4.rotate(rotationMatrixY, rotationMatrixY,(Math.PI * -90.0) / 180.0, [0.0, 1.0, 0.0]);
        mat4.scale(scaleMatrix, scaleMatrix, [1.2 ,1 ,1]);

        mat4.multiply(modelMatrix, modelMatrix, translationMatrix);
        // mat4.multiply(modelMatrix, modelMatrix, rotationMatrixY);
        mat4.multiply(modelMatrix, modelMatrix, scaleMatrix);
        // modelMatrix = this.modelPlacer.getModelMatrix();
        // this.display(modelMatrix, cam, this.texture, this.texture_nmap, 1.2, 0.8);
        this.displayCube(modelMatrix, cam, this.texture, this.texture_nmap, tileingArray);

    }

    /*
        This function will give a NormalMapped quad.
        parameters :
        1. Model Matrix
        2. Object of Camera Class, which gives View Matrix
        3. texture 
        4. Normal Map of 3rd parameter (above texture)
        5. texture tiling factor for Y axis 
        6. texture tiling scale which will be multiplied with tile_factor (Y), and will be passed as tile factor for X axis
    */
    display(modelMat, cam, tex, tex_nor, tile_factor, tile_scale)
    {
        var lightPos = [0.5, 1.0, 0.3];
        // var lightPos = [0.0, 0.0, 2.0];
        var camPos = [0.0, 0.0, 2.0];
        var angle = 0.0;

        gl.useProgram(this.normalMapShader.getShaderProgramObject());

        // var translationMatrix = mat4.create();
        // var modelMatrix = mat4.create();
        // var viewMatrix = mat4.create();
        // var modelViewProjectionMatrix = mat4.create();
        
        // mat4.lookAt(viewMatrix, camPos, [0.0 ,0.0, 0.0], [0.0, 1.0, 0.0]);
        // mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -1.0]);

        gl.uniformMatrix4fv(this.normalMapShader.modelMatrixUniform, false, modelMat);
        // gl.uniformMatrix4fv(this.normalMapShader.viewMatrixUniform, false, viewMatrix);
        gl.uniformMatrix4fv(this.normalMapShader.viewMatrixUniform, false, cam.getViewMatrix());
        gl.uniformMatrix4fv(this.normalMapShader.projectionMatrixUniform, false, perspectiveProjectionMatrix);

        gl.uniform3fv(this.normalMapShader.lightPosUniform, lightPos);
	    // gl.uniform3fv(this.normalMapShader.viewPosUniform, camPos);
	    gl.uniform3fv(this.normalMapShader.viewPosUniform, cam.getEye());

        // Tiling
        this.normalMapShader.setTilingUniform(tile_factor * tile_scale, tile_factor);

        gl.activeTexture(gl.TEXTURE1);
        gl.bindTexture(gl.TEXTURE_2D, tex_nor);
        gl.uniform1i(this.normalMapShader.textureNormalUniform, 1);
        gl.uniform1i(this.normalMapShader.KeyPressedUniform, 1);

        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, tex);
        gl.uniform1i(this.normalMapShader.textureDiffuseUniform, 0);

        this.wall.drawQuadNormalMapped();

        gl.useProgram(null);
    }


    /*
        This function will give a NormalMapped quad.
        parameters :
        1. Model Matrix
        2. Object of Camera Class, which gives View Matrix
        3. texture 
        4. Normal Map of 3rd parameter (above texture)
        5. texture tiling factor array for X and Y axis for 4 faces of Cube
    */
    displayCube(modelMat, cam, tex, tex_nor, tile_factor)
    {
        // var lightPos = [0.5, 1.0, 0.3];
        var lightPos = [0.0, 0.0, -2.0];
        var camPos = [0.0, 0.0, 2.0];
        var angle = 0.0;

        
        // var translationMatrix = mat4.create();
        // var modelMatrix = mat4.create();
        // var viewMatrix = mat4.create();
        // var modelViewProjectionMatrix = mat4.create();
        
        // mat4.lookAt(viewMatrix, camPos, [0.0 ,0.0, 0.0], [0.0, 1.0, 0.0]);
        // mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -1.0]);
        
        gl.useProgram(this.normalMapShader.getShaderProgramObject());
        gl.uniformMatrix4fv(this.normalMapShader.modelMatrixUniform, false, modelMat);
        // gl.uniformMatrix4fv(this.normalMapShader.viewMatrixUniform, false, viewMatrix);
        gl.uniformMatrix4fv(this.normalMapShader.viewMatrixUniform, false, cam.getViewMatrix());
        gl.uniformMatrix4fv(this.normalMapShader.projectionMatrixUniform, false, perspectiveProjectionMatrix);

        gl.uniform3fv(this.normalMapShader.lightPosUniform, lightPos);
	    // gl.uniform3fv(this.normalMapShader.viewPosUniform, camPos);
	    gl.uniform3fv(this.normalMapShader.viewPosUniform, cam.getEye());

        // Tiling
        this.normalMapShader.setTilingUniform(tile_factor[0], tile_factor[1]);
        // this.normalMapShader.setTilingUniform(tile_factor);

        gl.activeTexture(gl.TEXTURE1);
        gl.bindTexture(gl.TEXTURE_2D, tex_nor);
        gl.uniform1i(this.normalMapShader.textureNormalUniform, 1);
        gl.uniform1i(this.normalMapShader.KeyPressedUniform, 1);

        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, tex);
        gl.uniform1i(this.normalMapShader.textureDiffuseUniform, 0);

        this.wall.drawQuadNormalMapped();

        // 
        mat4.translate(modelMat, modelMat, [0.0, 0.0, 0.2]);
        gl.uniformMatrix4fv(this.normalMapShader.modelMatrixUniform, false, modelMat);
        this.normalMapShader.setTilingUniform(tile_factor[2], tile_factor[3]);
        this.wall.drawQuadNormalMapped();

        // 
        mat4.translate(modelMat, modelMat, [-1.0, 0.0, -0.1]);
        mat4.rotate(modelMat, modelMat,(Math.PI * -90.0) / 180.0, [0.0, 1.0, 0.0]);
        mat4.scale(modelMat, modelMat, [0.1, 1.0, 1.0]);
        gl.uniformMatrix4fv(this.normalMapShader.modelMatrixUniform, false, modelMat);
        this.normalMapShader.setTilingUniform(tile_factor[4], tile_factor[5]);
        this.wall.drawQuadNormalMapped();

        //
        mat4.translate(modelMat, modelMat, [0.0, 0.0, -2.0]);
        // mat4.rotate(modelMat, modelMat,(Math.PI * -90.0) / 180.0, [0.0, 1.0, 0.0]);
        // mat4.scale(modelMat, modelMat, [0.1, 1.0, 1.0]);
        gl.uniformMatrix4fv(this.normalMapShader.modelMatrixUniform, false, modelMat);
        this.normalMapShader.setTilingUniform(tile_factor[6], tile_factor[7]);
        this.wall.drawQuadNormalMapped();

        gl.useProgram(null);
    }

    uninitialize()
    {
        if (this.normalMapShader)
        {
            delete this.normalMapShader;
            this.normalMapShader = null;
        }

        if (this.wall)
        {
            delete this.wall;
            this.wall = null;
        }
    }
};