class FireParticle
{
    constructor()
    {
        this.particleVAO = null;
        this.particlePositionVBO = null;
        this.particleYSpeedVBO = null;
        this.particleLifetimeVBO = null;
        this.particleColorVBO = null;


        this.fireTexture;

        this.numberOfParticles = 150;
        this.fadeInFactor = 1.0;
        this.fadeOutFactor = 1.0;
        this.time = 0.0;

        this.shader = new FireParticleShader();
        this.shaderProgramObject = this.shader.getShaderProgramObject();

        this.lifetimeAttribute = 0;
        this.xPosAttribute = 1;
        this.ySpeedAttribute = 2;
        this.colorAttribute = 3;
    }   

    init()
    {
        var lifeTimes = [];
        var xPos = [];
        var ySpeed = [];
        var colors = [];

        for(var i=0; i < this.numberOfParticles ; ++i)
        {
            lifeTimes.push(2.0 * Math.random() + 1.0);
            xPos.push(1.5 * (Math.random() - 1.5));
            ySpeed.push(0.7 * Math.random());
            colors.push(Math.random());  
            colors.push(0.2 * Math.random());
        }


        this.particleVAO = gl.createVertexArray();
        gl.bindVertexArray(this.particleVAO);

        //lifetimes
        this.particleLifetimeVBO = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.particleLifetimeVBO);
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(lifeTimes), gl.STATIC_DRAW);
            gl.vertexAttribPointer(this.lifetimeAttribute, 1, gl.FLOAT, false, 0, 0);
            gl.enableVertexAttribArray(this.lifetimeAttribute);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        //xPos
        this.particlePositionVBO = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.particlePositionVBO);
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(xPos), gl.STATIC_DRAW);
            gl.vertexAttribPointer(this.xPosAttribute, 1, gl.FLOAT, false, 0, 0);
            gl.enableVertexAttribArray(this.xPosAttribute);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        //YSpeed
        this.particleYSpeedVBO = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.particleYSpeedVBO);
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(ySpeed), gl.STATIC_DRAW);
            gl.vertexAttribPointer(this.ySpeedAttribute, 1, gl.FLOAT, false, 0, 0);
            gl.enableVertexAttribArray(this.ySpeedAttribute);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        //colors
        this.particleColorVBO = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.particleColorVBO);
            gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);
            gl.vertexAttribPointer(this.colorAttribute, 2, gl.FLOAT, false, 0, 0);
            gl.enableVertexAttribArray(this.colorAttribute);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        gl.bindVertexArray(null);

        this.loadTexture("flame.png");
        
    }
    loadTexture(textureName)
    {
        this.fireTexture = gl.createTexture();
        this.fireTexture.image = new Image();
        this.fireTexture.image.src = textureName;
        this.fireTexture.image.onload = function()
        {
            gl.bindTexture(gl.TEXTURE_2D, this.fireTexture);
            gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, 1);

            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
            gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_LINEAR);

            gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, this.fireTexture.image);
            gl.generateMipmap(gl.TEXTURE_2D);

            gl.bindTexture(gl.TEXTURE_2D, null);
        }

    }

    drawFireParticles(cam)
    {
        var modelMatrix = mat4.create();
        var viewMatrix = mat4.create();


        gl.useProgram(this.shaderProgramObject);

        gl.enable(gl.BLEND);
        gl.blendFunc(gl.SRC_ALPHA,gl.ONE);
        
        viewMatrix = cam.getViewMatrix();
        
        mat4.translate(modelMatrix, modelMatrix, [0.0, 0.0, -5.0]);
        mat4.scale(modelMatrix, modelMatrix, [0.1, 0.2, 1.0]);
        
        gl.uniformMatrix4fv(this.shader.modelMatrixUniform, false, modelMatrix);
        gl.uniformMatrix4fv(this.shader.viewMatrixUniform, false, viewMatrix);
        gl.uniformMatrix4fv(this.shader.projectionMatrixUniform, false, perspectiveProjectionMatrix);

        gl.uniform1f(this.shader.fadeOutFactorUniform, this.fadeOutFactor);
        gl.uniform1f(this.shader.fadeInFactorUniform, this.fadeInFactor);        
        gl.uniform1f(this.shader.timeUniform, this.time);

        //gl.pointSize(35);
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, this.fireTexture);
        gl.uniform1i(this.shader.textureSamplerUniform, 0);

        gl.bindVertexArray(this.particleVAO);
        gl.drawArrays(gl.POINTS, 0, this.numberOfParticles);
        
        gl.bindTexture(gl.TEXTURE_2D, null);
        
        gl.bindVertexArray(null);

        gl.disable(gl.BLEND);
        gl.useProgram(null);

    }

    update()
    {
        this.time += 0.1;
        if(this.time > 360.0)
        {
            this.time = 0.0;
        }
    }

    uninitialize()
    {
        if(this.fireTexture)
        {
            gl.deleteTexture(this.fireTexture);
            this.fireTexture = null;
        }

        //release vao and vbo for particle
        if(this.particleVAO)
        {
            gl.deleteVertexArray(this.particleVAO);
            this.particleVAO = null;
        }

        if(this.particleLifetimeVBO)
        {
            gl.deleteBuffer(this.particleLifetimeVBO);
            this.particleLifetimeVBO = null;
        }

        if(this.particlePositionVBO)
        {
            gl.deleteBuffer(this.particlePositionVBO);
            this.particlePositionVBO = null;
        }

        if(this.particleYSpeedVBO)
        {
            gl.deleteBuffer(this.particleYSpeedVBO);
            this.particleYSpeedVBO = null;
        }

        if(this.particleColorVBO)
        {
            gl.deleteBuffer(this.particleColorVBO);
            this.particleColorVBO = null;
        }

        this.shader.uninitializeShader();
    }
};