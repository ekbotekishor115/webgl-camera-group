class Cube
{
    constructor()
    {
        var cubePosition = new Float32Array([
            // top
            1.0, 1.0, -1.0,
            -1.0, 1.0, -1.0,
            -1.0, 1.0, 1.0,
            1.0, 1.0, 1.0,
    
            // bottom
            1.0, -1.0, -1.0,
            -1.0, -1.0, -1.0,
            -1.0, -1.0, 1.0,
            1.0, -1.0, 1.0,
    
            // front
            1.0, 1.0, 1.0,
            -1.0, 1.0, 1.0,
            -1.0, -1.0, 1.0,
            1.0, -1.0, 1.0,
    
            // back
            1.0, 1.0, -1.0,
            -1.0, 1.0, -1.0,
            -1.0, -1.0, -1.0,
            1.0, -1.0, -1.0,
    
            // right
            1.0, 1.0, -1.0,
            1.0, 1.0, 1.0,
            1.0, -1.0, 1.0,
            1.0, -1.0, -1.0,
    
            // left
            -1.0, 1.0, 1.0,
            -1.0, 1.0, -1.0,
            -1.0, -1.0, -1.0,
            -1.0, -1.0, 1.0, 
        ]);
    
        var cubeTexcoord = new Float32Array([
            1.0, 1.0,
            0.0, 1.0,
            0.0, 0.0,
            1.0, 0.0,
    
            1.0, 1.0,
            0.0, 1.0,
            0.0, 0.0,
            1.0, 0.0,
    
            1.0, 1.0,
            0.0, 1.0,
            0.0, 0.0,
            1.0, 0.0,
    
            1.0, 1.0,
            0.0, 1.0,
            0.0, 0.0,
            1.0, 0.0,
    
            1.0, 1.0,
            0.0, 1.0,
            0.0, 0.0,
            1.0, 0.0,
    
            1.0, 1.0,
            0.0, 1.0,
            0.0, 0.0,
            1.0, 0.0
        ]);
    
    
        // vao Cube
        this.vao_cube = gl.createVertexArray();
        gl.bindVertexArray(this.vao_cube);
    
        // vbo position
        this.vbo_cube_position = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_cube_position);
        gl.bufferData(gl.ARRAY_BUFFER, cubePosition, gl.STATIC_DRAW);
        gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION, 
            3, 
            gl.FLOAT, 
            false, 
            0, 
            0);
        gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
    
        // vbo color
        this.vbo_cube_texcoord = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, this.vbo_cube_texcoord);
        gl.bufferData(gl.ARRAY_BUFFER, cubeTexcoord, gl.STATIC_DRAW);
        gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS, 
            2, 
            gl.FLOAT, 
            false, 
            0, 
            0);
        gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
    
        // unbind vao
        gl.bindVertexArray(null);
    }

    drawCube()
    {
        gl.bindVertexArray(this.vao_cube);
        gl.drawArrays(gl.TRIANGLE_FAN, 0, 24);
        gl.bindVertexArray(null);
    }

    uninitializeCube()
    {
        if (this.vbo_cube_position)
        {
            gl.deleteBuffer(this.vbo_cube_position);
            this.vbo_cube_position = null;
        }
    
        if (this.vbo_cube_texcoord)
        {
            gl.deleteBuffer(this.vbo_cube_texcoord);
            this.vbo_cube_texcoord = null;
        }
    
        if (this.vao_cube)
        {
            gl.deleteVertexArray(this.vao_cube);
            this.vao_cube = null;
        }
    }
};