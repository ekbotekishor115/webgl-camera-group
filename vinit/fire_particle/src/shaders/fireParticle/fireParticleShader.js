class FireParticleShader{

    constructor() {
        // Shaders
        this.shaderProgramObject = null;

        // Uniforms
        //this.mvpMatrixUniform;
        this.modelMatrixUniform;
        this.viewMatrixUniform;
        this.projectionMatrixUniform;


        this.vertexShaderObject;
        this.fragmentShaderObject;

        this.lifetimeAttribute = 0;
        this.xPosAttribute = 1;
        this.ySpeedAttribute = 2;
        this.colorAttribute = 3;


        this.timeUniform;
        this.pointSizeUniform;
        this.textureSamplerUniform;
        this.fadeInFactorUniform;
        this.fadeOutFactorUniform;

        // vertex shader
        var vertexShaderSourceCode = 
        "#version 300 es"                           +
		"\n"                                        +
        "precision highp float;"                    +

		"in float aLifetime;"                       +
		"in float aXPos;"                           +
		"in float aYSpeed;"                         +
		"in vec2 aColor;"                           +

		"uniform float uTime;"                      +
		"uniform float uPointSize;"                 +

		"uniform mat4 uModelMatrix;"              +
		"uniform mat4 uViewMatrix;"               +
		"uniform mat4 uProjectionMatrix;"         +

		"out float vLifetime;"                      +
		"out vec2 color;"                           +

		"void main(void)"                                                                                                       +
		"{"                                                                                                                     +
            "gl_PointSize = 30.0;"                                                                                              +
            "vLifetime		= mod(uTime, aLifetime);"                                                                           +
			"float ti		= 1.0 - vLifetime/aLifetime;"                                                                       +

			"mat4 mv_matrix = uViewMatrix * uModelMatrix;"                                                                  +

			"gl_Position	= uProjectionMatrix * mv_matrix * vec4(aXPos * ti, aYSpeed * vLifetime - 1.0, 1.0, 1.0);"                                           +
			"vLifetime		= 4.0 * ti * (1.0 - ti);"                                                                           +
			"color			= aColor;"                                                                                          +
        "}";

        // vertex shader object
        this.vertexShaderObject = this.createShaderObjects(vertexShaderSourceCode, gl.VERTEX_SHADER);

        // fragment shader
        var fragmentShaderSourceCode =
        "#version 300 es"                       +
		"\n"                                    +
        "precision highp float;"              +

		"uniform sampler2D sTexture;"           +

		"in float vLifetime;"                   +
		"in vec2 color;"                        +

		"out vec4 FragColor;"                   +

		"uniform float fadeinFactor;"           +
		"uniform float fadeoutFactor;"          +

		"void main(void)"                                                           +
		"{"                                                                         +
			"vec4 ColorTemp;"                                                       +
			"vec4 texColor	= texture(sTexture, gl_PointCoord);"                    +
			"ColorTemp		= vec4(color, 0.0, 1.0) * texColor ;"                   +
			"ColorTemp.a	= vLifetime + 0.75;"                                    +
            "if(ColorTemp.r < 0.1 && ColorTemp.g < 0.1 && ColorTemp.b < 0.1)"       +
			"discard;"                                                              +
			"FragColor		= ColorTemp * fadeinFactor * fadeoutFactor;"            +
		"}";

        // fragment shader object
        this.fragmentShaderObject = this.createShaderObjects(fragmentShaderSourceCode, gl.FRAGMENT_SHADER);
        
        // Attach and Link shaderObjects
        this.shaderProgramObject = attachAndLinkShaderObjects(
            [this.vertexShaderObject, this.fragmentShaderObject],
            [this.lifetimeAttribute,this.xPosAttribute,this.ySpeedAttribute,this.colorAttribute],
            ["aLifetime", "aXPos","aYSpeed","aColor"]
        );
        if (this.shaderProgramObject == null) {
            alert("ShaderProgramObject failed");
        }

        // uniforms
        //this.mvpMatrixUniform = this.getUniformLocation("uMVPMatrix");


        this.viewMatrixUniform = this.getUniformLocation(shaderProgramObject,"uViewMatrix");
        this.modelMatrixUniform = this.getUniformLocation(shaderProgramObject,"uModelMatrix");
        this.projectionMatrixUniform = this.getUniformLocation(shaderProgramObject,"uProjectionMatrix");


        this.timeUniform = this.getUniformLocation(this.shaderProgramObject,"uTime");
        this.pointSizeUniform = this.getUniformLocation(this.shaderProgramObject,"uPointSize");
        this.textureSamplerUniform = this.getUniformLocation(this.shaderProgramObject,"sTexture");
        this.fadeInFactorUniform = this.getUniformLocation(this.shaderProgramObject,"fadeinFactor");
        this.fadeOutFactorUniform = this.getUniformLocation(this.shaderProgramObject,"fadeoutFactor");
        
    }

    createShaderObjects(shaderStr, shaderType)
    {
        let shaderObject = createAndCompileShaders(shaderStr, shaderType);
        if (shaderObject == null) 
        {
            alert(shaderType + " Shader Compilation failed !!!");
            return null;
        }

        return shaderObject;
    }

    getUniformLocation(uniform)
    {
        return gl.getUniformLocation(this.shaderProgramObject, uniform);
    }

    getShaderProgramObject()
    {
        return this.shaderProgramObject;
    }

    uninitializeShader()
    {
        uninitializeShaders();
    }
};