class NormalMappingShader
{
    constructor() {
        // Shaders
        this.shaderProgramObject = null;

        // Uniforms
        this.mvpMatrixUniform;
        this.modelMatrixUniform;
        this.viewMatrixUniform;
        this.projectionMatrixUniform;

        this.vertexShaderObject;
        this.fragmentShaderObject;

        // vertex shader
        // vertex shader
        var vertexShaderSourceCode = 
        `#version 300 es
        \n
        precision highp float;
        in vec3 aPos;
        in vec3 aNormal;
        in vec2 aTexCoords;
        in vec3 aTangent;
        in vec3 aBitangent;

		out vec3 FragPos;
		out vec2 TexCoords;
		out vec3 TangentLightPos;
		out vec3 TangentViewPos;
		out vec3 TangentFragPos;

		uniform mat4 projection;
		uniform mat4 view;
		uniform mat4 model;

		uniform vec3 lightPos;
		uniform vec3 viewPos;

        uniform float tilingX;
        uniform float tilingY;

		void main()
		{
		FragPos = vec3(model * vec4(aPos, 1.0));
		TexCoords.s = aTexCoords.s * tilingX;
		TexCoords.t = aTexCoords.t * tilingY;

		mat3 normalMatrix = transpose(inverse(mat3(model)));
		vec3 T = normalize(normalMatrix * aTangent);
		vec3 N = normalize(normalMatrix * aNormal);
		T = normalize(T - dot(T, N) * N);
		vec3 B = cross(N, T);

		mat3 TBN = transpose(mat3(T, B, N));
		TangentLightPos = TBN * lightPos;
		TangentViewPos = TBN * viewPos;
		TangentFragPos = TBN * FragPos;

		gl_Position = projection * view * model * vec4(aPos, 1.0);
        }`;

        // vertex shader object
        this.vertexShaderObject = this.createShaderObjects(vertexShaderSourceCode, gl.VERTEX_SHADER);

        // fragment shader
        var fragmentShaderSourceCode =
        `#version 300 es
        \n
        precision highp float;
        out vec4 FragColor;

        in vec3 FragPos;
        in vec2 TexCoords;
        in vec3 TangentLightPos;
        in vec3 TangentViewPos;
        in vec3 TangentFragPos;

        uniform sampler2D diffuseMap;
        uniform sampler2D normalMap;
        uniform vec3 lightPos;
        uniform vec3 viewPos;
        uniform int uKeyPressed;
        void main()
        {
            if (uKeyPressed == 1)
            {
            vec3 normal = texture(normalMap, TexCoords).rgb;
            normal = normalize(normal * 2.0 - 1.0);
            vec3 color = texture(diffuseMap, TexCoords).rgb;
            vec3 ambient = 1.0 * color;
            vec3 lightDir = normalize(TangentLightPos - TangentFragPos);
            float diff = max(dot(lightDir, normal), 0.0);
            vec3 diffuse = diff * color;
            vec3 viewDir = normalize(TangentViewPos - TangentFragPos);
            vec3 reflectDir = reflect(-lightDir, normal);
            vec3 halfwayDir = normalize(lightDir + viewDir);  
            float spec = pow(max(dot(normal, halfwayDir), 0.0), 32.0);
            vec3 specular = vec3(0.3) * spec;
            FragColor = vec4(ambient + diffuse + specular, 1.0);
            }
            else
            {
            vec3 normal = texture(normalMap, TexCoords).rgb;
            normal = normalize(normal * 2.0 - 1.0);
            vec3 color = texture(diffuseMap, TexCoords).rgb;
            vec3 ambient = 1.0 * color;
            vec3 lightDir = normalize(TangentLightPos - TangentFragPos);
            float diff = max(dot(lightDir, normal), 0.0);
            vec3 diffuse = diff * color;
            vec3 viewDir = normalize(TangentViewPos - TangentFragPos);
            vec3 reflectDir = reflect(-lightDir, normal);
            vec3 halfwayDir = normalize(lightDir + viewDir);  
            float spec = pow(max(dot(normal, halfwayDir), 0.0), 32.0);
            vec3 specular = vec3(0.3) * spec;
            FragColor = vec4(ambient + diffuse + specular, 1.0);
            }
        }`;

        // fragment shader object
        this.fragmentShaderObject = this.createShaderObjects(fragmentShaderSourceCode, gl.FRAGMENT_SHADER);
        
        // Attach and Link shaderObjects
        this.shaderProgramObject = attachAndLinkShaderObjects(
            [this.vertexShaderObject, this.fragmentShaderObject],
            [VertexAttributeEnum.CAM_ATTRIBUTE_POSITION, VertexAttributeEnum.CAM_ATTRIBUTE_NORMAL, VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS, VertexAttributeEnum.CAM_ATTRIBUTE_TANGENTS, VertexAttributeEnum.CAM_ATTRIBUTE_BITANGENTS],
            ["aPos", "aNormal", "aTexCoords", "aTangent", "aBitangent"]
        );
        if (this.shaderProgramObject == null) {
            alert("ShaderProgramObject failed");
        }

        // uniforms
        // this.mvpMatrixUniform = this.getUniformLocation("uMVPMatrix");
        this.modelMatrixUniform = this.getUniformLocation( "model");
        this.viewMatrixUniform = this.getUniformLocation( "view");
        this.projectionMatrixUniform = this.getUniformLocation( "projection");
        this.textureNormalUniform = this.getUniformLocation( "normalMap");
        this.textureDiffuseUniform = this.getUniformLocation( "diffuseMap");
        this.lightPosUniform = this.getUniformLocation( "lightPos");
        this.viewPosUniform = this.getUniformLocation( "viewPos");
        this.KeyPressedUniform = this.getUniformLocation( "uKeyPressed");

        // use this carefully
        this.tilingUniformX = this.getUniformLocation("tilingX");
        this.tilingUniformY = this.getUniformLocation("tilingY");
    }

    createShaderObjects(shaderStr, shaderType)
    {
        let shaderObject = createAndCompileShaders(shaderStr, shaderType);
        if (shaderObject == null) 
        {
            alert(shaderType + " Shader Compilation failed !!!");
            return null;
        }

        return shaderObject;
    }

    getUniformLocation(uniform)
    {
        return gl.getUniformLocation(this.shaderProgramObject, uniform);
    }

    getShaderProgramObject()
    {
        return this.shaderProgramObject;
    }

    setTilingUniform(tileFactorX, tileFactorY)
    {
        gl.uniform1f(this.tilingUniformX, tileFactorX);
        gl.uniform1f(this.tilingUniformY, tileFactorY);
    }

    uninitializeShader()
    {
        uninitializeShaders();
    }
};