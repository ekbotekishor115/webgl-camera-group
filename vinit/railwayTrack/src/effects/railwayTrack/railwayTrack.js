class RailwayTrack
{
    constructor()
    {
        this.leftTrack = new Shapes();
        this.rightTrack = new Shapes();
        this.tie = new Shapes();
        this.ground = new Shapes();
        this.shader = new RailwayTrackShader();
        this.shaderProgramObject = this.shader.getShaderProgramObject();
    }

    drawTrack(cam,totalTiles)
    {
        // var lightAmbient = [0.1,0.1,0.1];
        // var lightDiffuse = [1.0,1.0,1.0];
        // var lightSpecular = [1.0,1.0,1.0];
        // var lightPosition = [100.0,100.0,100.0,1.0];

        // var trackMaterialDiffuse = [0.560,0.570,0.580];
        // var trackMaterialAmbient = [0.2,0.2,0.2];
        // var trackMaterialSpecular = [1.0,1.0,1.0];
        // var trackMaterialShineness = 255.0;


        gl.useProgram(this.shaderProgramObject);
        


        var modelMatrix = mat4.create();
        var viewMatrix = mat4.create();
        var projectionMatrix = mat4.create();
        mat4.identity(modelMatrix);
        mat4.identity(viewMatrix);
        mat4.identity(projectionMatrix);


        //var trackLength = (totalTiles/10.0);

        
        //matrix transformations
        viewMatrix = cam.getViewMatrix();

        //mat4.multiply(modelViewMatrix, modelMatrix, viewMatrix);

        //mat4.translate(modelViewMatrix,modelViewMatrix,[11.5,-3.35,0.2]);
        //mat4.translate(modelMatrix,modelMatrix,[4.0,0.65,-13.0 * trackLength]);
        //mat4.translate(modelMatrix,modelMatrix,[4.0,0.65,0.0]);

        //mat4.rotateZ(modelViewMatrix,modelViewMatrix,(Math.PI *90.0/180.0));

        //mat4.scale(modelMatrix,modelMatrix,[0.2,0.2,15.0 * trackLength]);
        //mat4.scale(modelMatrix,modelMatrix,[0.2,0.2,15.0]);

        

        //mat4.multiply(projectionMatrix, projectionMatrix, perspectiveProjectionMatrix);

        //set uniforms
        //gl.uniformMatrix4fv(this.shader.mvpMatrixUniform, false, modelViewProjection);
        gl.uniformMatrix4fv(this.shader.viewMatrixUniform,false,viewMatrix);
        gl.uniformMatrix4fv(this.shader.modelMatrixUniform,false,modelMatrix);
        gl.uniformMatrix4fv(this.shader.projectionMatrixUniform,false,perspectiveProjectionMatrix);

        // gl.uniform3fv(this.shader.lightDiffuseUniform,lightDiffuse);
        // gl.uniform3fv(this.shader.materialDiffuseUniform,trackMaterialDiffuse);
        // gl.uniform3fv(this.shader.lightAmbientUniform,lightAmbient);
        // gl.uniform3fv(this.shader.materialAmbientUniform,trackMaterialAmbient);
        // gl.uniform3fv(this.shader.lightSpecularUniform,lightSpecular);
        // gl.uniform3fv(this.shader.materialSpecularUniform,trackMaterialSpecular);
        // gl.uniform4fv(this.shader.lightPositionUniform,lightPosition);
        // gl.uniform1f(this.shader.materialShinenessUnifrom,trackMaterialShineness);

        this.ground.drawQuad();

        /*
        this.leftTrack.drawCube();

        
        
        mat4.identity(modelMatrix);
        mat4.identity(viewMatrix);
       


        //matrix transformations
        viewMatrix = cam.getViewMatrix();

        //mat4.multiply(modelViewMatrix, modelMatrix, viewMatrix);

        //mat4.translate(modelViewMatrix,modelViewMatrix,[11.5,3.35,0.2]);
        //mat4.translate(modelMatrix,modelMatrix,[-4.0,0.65,-13.0 * trackLength]);
        mat4.translate(modelMatrix,modelMatrix,[-4.0,0.65,0.0]);

        //mat4.rotateZ(modelViewMatrix,modelViewMatrix,(Math.PI *90.0/180.0));

        //mat4.scale(modelMatrix,modelMatrix,[0.2,0.2,15.0 * trackLength]);
        mat4.scale(modelMatrix,modelMatrix,[0.2,0.2,15.0]);

        

        //mat4.multiply(modelViewProjection, perspectiveProjectionMatrix, modelViewMatrix);

        //set uniforms
        //gl.uniformMatrix4fv(this.shader.mvpMatrixUniform, false, modelViewProjection);
        gl.uniformMatrix4fv(this.shader.viewMatrixUniform,false,viewMatrix);
        gl.uniformMatrix4fv(this.shader.modelMatrixUniform,false,modelMatrix);
        gl.uniformMatrix4fv(this.shader.projectionMatrixUniform,false,perspectiveProjectionMatrix);


        this.rightTrack.drawCube();
        

        //tile

        var noOfTiles = totalTiles;

        for(let i= 0 ; i < noOfTiles; ++i)
        {

            mat4.identity(modelMatrix);
            mat4.identity(viewMatrix);


            //matrix transformations
            viewMatrix = cam.getViewMatrix();

            //mat4.multiply(modelViewMatrix, modelMatrix, viewMatrix);

            mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,(i*(-2.62))]);

            //mat4.rotateZ(modelViewMatrix,modelViewMatrix,(Math.PI *90.0/180.0));

            //mat4.scale(modelViewMatrix,modelViewMatrix,[0.5,5.0,0.2]);
            mat4.scale(modelMatrix,modelMatrix,[5.0,0.5,0.5]);

            

            //mat4.multiply(modelViewProjection, perspectiveProjectionMatrix, modelViewMatrix);

            //set uniforms
            //gl.uniformMatrix4fv(this.shader.mvpMatrixUniform, false, modelViewProjection);
            gl.uniformMatrix4fv(this.shader.viewMatrixUniform,false,viewMatrix);
            gl.uniformMatrix4fv(this.shader.modelMatrixUniform,false,modelMatrix);
            gl.uniformMatrix4fv(this.shader.projectionMatrixUniform,false,perspectiveProjectionMatrix);


            this.tie.drawCube();

        }
        */
        
        gl.useProgram(null);

    }

    destroy()
    {
        this.leftTrack.uninitializeCube();
        this.rightTrack.uninitializeCube();
        this.tie.uninitializeCube();
    }

};