class RailwayTrackShader{

    constructor() {
        // Shaders
        this.shaderProgramObject = null;

        // Uniforms
        //this.mvpMatrixUniform;
        this.modelMatrixUniform;
        this.viewMatrixUniform;
        this.projectionMatrixUniform;
        this.lightDiffuseUniform;
        this.materialDiffuseUniform;
        this.lightAmbientUnifom;
        this.materialAmbientUniform;
        this.lightSpecularUniform;
        this.materialSpecularUniform;
        this.lightPositionUniform;
        this.materialShinenessUniform;


        this.vertexShaderObject;
        this.fragmentShaderObject;

        // vertex shader
        var vertexShaderSourceCode = 
        `#version 300 es
        \n
        precision highp float;
        in vec3 aPosition;
        in vec3 aNormal;
        uniform mat4 uModelMatrix;
        uniform mat4 uViewMatrix;
        uniform mat4 uProjectionMatrix;
        // uniform vec3 uLightDiffuse;
        // uniform vec3 uMaterialDiffuse;
        // uniform vec3 uLightAmbient;
        // uniform vec3 uMaterialAmbient;
        // uniform vec3 uLightSpecular;
        // uniform vec3 uMaterialSpecular;
        // uniform float uMaterialShineness;
        // uniform vec4 uLightPosition;
        // out vec3 oTransformedNormals;
        // out vec3 oLightDirection;
        // out vec3 oViewerVector;

        void main(void)
        {
            // vec4 eyeCoordinates =  uViewMatrix * uModelMatrix * aPosition;
            // oTransformedNormals = mat3(uViewMatrix * uModelMatrix) * aNormal;
            // oLightDirection = vec3(uLightPosition - eyeCoordinates);
            // oViewerVector = (-eyeCoordinates . xyz);
            
            gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * vec4(aPosition,1.0);
            //gl_Position = uProjectionMatrix * aPosition;
        }`;

        // vertex shader object
        this.vertexShaderObject = this.createShaderObjects(vertexShaderSourceCode, gl.VERTEX_SHADER);

        // fragment shader
        var fragmentShaderSourceCode =
        `#version 300 es
        \n
        precision highp float;
        // in vec3 oTransformedNormals;
        // in vec3 oLightDirection;
        // in vec3 oViewerVector;
        // uniform vec3 uLightDiffuse;
        // uniform vec3 uMaterialDiffuse;
        // uniform vec3 uLightAmbient;
        // uniform vec3 uMaterialAmbient;
        // uniform vec3 uLightSpecular;
        // uniform vec3 uMaterialSpecular;
        // uniform float uMaterialShineness;
        out vec4 FragColor;
        void main(void)
        {
            // vec3 phong_ADS_Light;
            // vec3 normalisedTransformedNormals = normalize(oTransformedNormals);
            // vec3 normalisedLightDirection = normalize(oLightDirection);
            // vec3 normalisedViewerVector = normalize(oViewerVector);
            // vec3 ambientLight = uLightAmbient *  uMaterialAmbient;
            // vec3 diffuseLight = uLightDiffuse * uMaterialDiffuse * max(dot(normalisedLightDirection,normalisedTransformedNormals),0.0);
            // vec3 reflectionVector = reflect(-normalisedLightDirection,normalisedTransformedNormals);
            // vec3 specularLight =uLightSpecular * uMaterialSpecular * pow(max(dot(reflectionVector,normalisedViewerVector),0.0),uMaterialShineness);
            // phong_ADS_Light = ambientLight + diffuseLight + specularLight;
            // FragColor = vec4(phong_ADS_Light, 1.0);
            FragColor = vec4(1.0,1.0,1.0,1.0);
        }`;

        // fragment shader object
        this.fragmentShaderObject = this.createShaderObjects(fragmentShaderSourceCode, gl.FRAGMENT_SHADER);
        
        // Attach and Link shaderObjects
        this.shaderProgramObject = attachAndLinkShaderObjects(
            [this.vertexShaderObject, this.fragmentShaderObject],
            [VertexAttributeEnum.CAM_ATTRIBUTE_POSITION, VertexAttributeEnum.CAM_ATTRIBUTE_NORMAL],
            ["aPosition", "aNormal"]
        );
        if (this.shaderProgramObject == null) {
            alert("ShaderProgramObject failed");
        }

        // uniforms
        //this.mvpMatrixUniform = this.getUniformLocation("uMVPMatrix");

        this.viewMatrixUniform = this.getUniformLocation(shaderProgramObject,"uViewMatrix");
        this.modelMatrixUniform = this.getUniformLocation(shaderProgramObject,"uModelMatrix");
        this.projectionMatrixUniform = this.getUniformLocation(shaderProgramObject,"uProjectionMatrix");
        // this.lightDiffuseUniform = this.getUniformLocation(shaderProgramObject,"uLightDiffuse");
        // this.materialDiffuseUniform = this.getUniformLocation(shaderProgramObject,"uMaterialDiffuse");
        
        // this.lightAmbientUniform = this.getUniformLocation(shaderProgramObject,"uLightAmbient");
        // this.materialAmbientUniform = this.getUniformLocation(shaderProgramObject,"uMaterialAmbient");

        // this.lightSpecularUniform = this.getUniformLocation(shaderProgramObject,"uLightSpecular");
        // this.materialSpecularUniform = this.getUniformLocation(shaderProgramObject,"uMaterialSpecular");

        // this.lightPositionUniform = this.getUniformLocation(shaderProgramObject,"uLightPosition");
        // this.materialShinenessUnifrom = this.getUniformLocation(shaderProgramObject,"uMaterialShineness");
    }

    createShaderObjects(shaderStr, shaderType)
    {
        let shaderObject = createAndCompileShaders(shaderStr, shaderType);
        if (shaderObject == null) 
        {
            alert(shaderType + " Shader Compilation failed !!!");
            return null;
        }

        return shaderObject;
    }

    getUniformLocation(uniform)
    {
        return gl.getUniformLocation(this.shaderProgramObject, uniform);
    }

    getShaderProgramObject()
    {
        return this.shaderProgramObject;
    }

    uninitializeShader()
    {
        uninitializeShaders();
    }
};