class ModelPlacer
{
    constructor()
    {
        this.SENSITIVITY_TRANSLATE = 1.0;
        this.SENSITIVITY_ROTATE = 1.0;
        this.SENSITIVITY_SCALE = 1.0;

        this.modelMatrix;
        this.translationMatrix;
        this.rotationMatrix;
        this.scaleMatrix;

        this.Xt = 0.0;
        this.Yt = 0.0;
        this.Zt = 0.0;

        this.Xr = 10.0;
        this.Yr = 10.0;
        this.Zr = 10.0;

        this.Xs = 1.0;
        this.Ys = 1.0;
        this.Zs = 1.0;
    }

    getModelMatrix()
    {
        this.modelMatrix = mat4.create();
        mat4.identity(this.modelMatrix);

        this.translationMatrix = mat4.create();
        this.rotationMatrixX = mat4.create();
        this.rotationMatrixY = mat4.create();
        this.rotationMatrixZ = mat4.create();
        this.rotationMatrix = mat4.create();
        this.scaleMatrix = mat4.create();

        mat4.translate(this.translationMatrix, this.translationMatrix , [this.Xt, this.Yt, this.Zt]);

        mat4.rotate(this.rotationMatrixX, this.rotationMatrixX, this.Xr, [1.0, 0.0, 0.0]);
        mat4.rotate(this.rotationMatrixY, this.rotationMatrixY, this.Yr, [0.0, 1.0, 0.0]);
        mat4.rotate(this.rotationMatrixZ, this.rotationMatrixZ, this.Zr, [0.0, 0.0, 1.0]);

        mat4.multiply(this.rotationMatrix, this.rotationMatrix, this.rotationMatrixX);
        mat4.multiply(this.rotationMatrix, this.rotationMatrix, this.rotationMatrixY);
        mat4.multiply(this.rotationMatrix, this.rotationMatrix, this.rotationMatrixZ);

        mat4.scale(this.scaleMatrix, this.scaleMatrix, [this.Xs, this.Ys, this.Zs]);

        mat4.multiply(this.modelMatrix, this.modelMatrix, this.translationMatrix);
        mat4.multiply(this.modelMatrix, this.modelMatrix, this.rotationMatrix);
        mat4.multiply(this.modelMatrix, this.modelMatrix, this.scaleMatrix);

        return this.modelMatrix;
    }

    getKeyForTransformation(key)
    {
        switch(key)
        {
            case 'm':
            case 'M':
                this.isTranslate = true;
                this.isRotate = false;
                this.isScale = false;
                break;
            case 'n':
            case 'N':
                this.isRotate = true;
                this.isTranslate = false;
                this.isScale = false;
                break;
            case 'b':
            case 'B':
                this.isScale = true;
                this.isTranslate = false;
                this.isRotate = false;
                break;
            case ';':
                this.printForPlacement();
                break;
        }

        // for translation
        if (this.isTranslate === true)
        {
            this.doTranslate(key);
        }

        // for scale
        if (this.isScale === true)
        {
            this.doScale(key);
        }

        // for rotate
        if (this.isRotate === true)
        {
            this.doRotate(key);
        }
        
    }

    doTranslate(key)
    {
        switch(key)
        {
            case 'u':
            case 'U':
                this.Xt += this.SENSITIVITY_TRANSLATE;
                break;
            case 'j':
            case 'J':
                this.Xt -= this.SENSITIVITY_TRANSLATE;
                break;
            case 'i':
            case 'I':
                this.Zt += this.SENSITIVITY_TRANSLATE;
                break;
            case 'k':
            case 'K':
                this.Zt -= this.SENSITIVITY_TRANSLATE;
                break;
            case 'o':
            case 'O':
                this.Yt += this.SENSITIVITY_TRANSLATE;
                break;
            case 'l':
            case 'L':
                this.Yt -= this.SENSITIVITY_TRANSLATE;
                break;
        }
    }

    doRotate(key)
    {
        switch(key)
        {
            case 'u':
            case 'U':
                this.Xr += this.SENSITIVITY_ROTATE;
                break;
            case 'j':
            case 'J':
                this.Xr -= this.SENSITIVITY_ROTATE;
                break;
            case 'i':
            case 'I':
                this.Zr += this.SENSITIVITY_ROTATE;
                break;
            case 'k':
            case 'K':
                this.Zr -= this.SENSITIVITY_ROTATE;
                break;
            case 'o':
            case 'O':
                this.Yr += this.SENSITIVITY_ROTATE;
                break;
            case 'l':
            case 'L':
                this.Yr -= this.SENSITIVITY_ROTATE;
                break;
        }
    }

    doScale(key)
    {
        switch(key)
        {
            case 'u':
            case 'U':
                break;
            case 'j':
            case 'J':
                break;
            case 'i':
            case 'I':
                break;
            case 'k':
            case 'K':
                break;
            case 'o':
            case 'O':
                break;
            case 'l':
            case 'L':
                break;
        }
    }

    printForPlacement()
    {
        // translate
        console.log("mat4.translate(this.translationMatrix, this.translationMatrix , " + "[" + this.Xt +" ," + this.Yt + " ," + this.Zt +"]);");

        // rotate
        console.log("mat4.rotate(this.rotationMatrixX, this.rotationMatrixX,"+ this.Xr + ", [1.0, 0.0, 0.0]);");
        console.log("mat4.rotate(this.rotationMatrixY, this.rotationMatrixY,"+ this.Yr + ", [0.0, 1.0, 0.0]);");
        console.log("mat4.rotate(this.rotationMatrixZ, this.rotationMatrixZ,"+ this.Zr + ", [0.0, 0.0, 1.0]);");
        
        // scale
        console.log("mat4.scale(this.scaleMatrix, this.scaleMatrix, [" +this.Xs + " ," + this.Ys + " ,"+ this.Zs+ "]);")
    }
};