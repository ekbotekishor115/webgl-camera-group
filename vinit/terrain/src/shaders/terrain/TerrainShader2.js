class Terrain2Shader{

    constructor() {
        // Shaders
        this.shaderProgramObject = null;

        // Uniforms
        this.modelMatrixUniform;
        this.viewMatrixUniform;
        this.projectionMatrixUniform;

        this.vertexShaderObject;
        this.fragmentShaderObject;

        // vertex shader
        var vertexShaderSourceCode = 
        `#version 300 es
        \n
        in vec4 aPosition;
        in vec3 aNormal;
        in vec2 aTexcoord;
        uniform mat4 uModelMatrix;
        uniform mat4 uViewMatrix;
        uniform mat4 uProjectionMatrix;
        uniform vec4 uLightPosition;
        uniform vec3 uLightDiffuse;
        uniform lowp int uLightEnabled;
        out vec3 oDiffuseLight;
        out vec3 oNormal;
        out vec2 oTexCoord;
        out vec3 oPosition;
        void main(void)
        {
            mat4 modelViewMatrix = uViewMatrix * uModelMatrix ;
            gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * aPosition;
            oNormal = mat3(modelViewMatrix) * aNormal;
            oTexCoord = aTexcoord * 10.0;
            // oTexcoord = aPosition.xy / 2.0 - 0.5;
            oPosition = vec3(aPosition.x, aPosition.y, aPosition.z);
            if(uLightEnabled == 1)
            {
                vec4 eyePosition = modelViewMatrix * aPosition;
                mat3 normalMatrix = mat3(transpose(inverse(modelViewMatrix)));
                vec3 n = normalize(normalMatrix * aNormal);
                vec3 s = normalize(vec3(uLightPosition - eyePosition));
                oDiffuseLight = uLightDiffuse * max(dot(s,n),0.0);
            }
            else
            {
                oDiffuseLight = vec3(0.0,0.0,0.0);
            }
        }`;

        // vertex shader object
        this.vertexShaderObject = this.createShaderObjects(vertexShaderSourceCode, gl.VERTEX_SHADER);

        // fragment shader
        var fragmentShaderSourceCode =
        `#version 300 es
        \n
        precision highp float;
        in vec3 oNormal;
        in vec2 oTexCoord;
        in vec3 oPosition;
        in vec3 oDiffuseLight;
        
        uniform lowp int uLightEnabled;
        uniform sampler2D uTextureRockSampler;
        uniform sampler2D uTextureMuddSampler;
        uniform sampler2D uTextureGroundSampler;
        uniform float uHeightOne;
        uniform float uHeightTwo;
        uniform float uHeightThree;
        out vec4 FragColor;

        vec4 calculatedTexcolor()
        {
            vec4 color1;
            vec4 color2;
            float delta;
            float factor;
            vec4 calTexColor;

            float height = oPosition.y;


            if(height < uHeightOne)
            //if(keyInput == 1)
            {
                calTexColor = texture(uTextureMuddSampler,oTexCoord);
            }
            else if(height < uHeightTwo)
            //else if(keyInput  == 2)
            {
                color1 = texture(uTextureMuddSampler,oTexCoord);
                color2 = texture(uTextureGroundSampler,oTexCoord);
                delta = uHeightTwo - uHeightOne;
                factor = (height - uHeightOne)/delta;

                calTexColor =mix(color1,color2,factor);
            }
            else if(height < uHeightThree)
            // else if(keyInput  == 3)
            {
                color1 = texture(uTextureGroundSampler,oTexCoord);
                color2 = texture(uTextureRockSampler,oTexCoord);
                delta = uHeightThree - uHeightTwo;
                factor = (height - uHeightTwo)/delta;

                calTexColor = mix(color1,color2,factor);
            }
            else
            {
                calTexColor = texture(uTextureRockSampler,oTexCoord);
                //calTexColor = vec4(1.0,1.0,1.0,1.0);
            }

            return calTexColor;
        }


        void main(void)
        {
            /*
            vec3 lightDirection = normalize(vec3(1,2,-3));

            float light = dot(lightDirection,normalize(oNormal)) * 0.5 +0.5;
            vec4 color1 = texture(uTextureSampler, oTex coord);
            vec4 color2 = texture(uTextureMudd, oTexc   oord);
            // FragColor = vec4(vec3(0,1,0) * light, 1) + mix(color1, color2, 0.6);
            FragColor = mix(color1, color2, 0.7);
            */

            vec4 texColor = calculatedTexcolor();

            if(uLightEnabled == 1)
            {
                FragColor = vec4(oDiffuseLight,1.0) * texColor;
            }
            else
            {
                FragColor = texColor;
            }
        
        }`;

        // fragment shader object
        this.fragmentShaderObject = this.createShaderObjects(fragmentShaderSourceCode, gl.FRAGMENT_SHADER);
        
        // Attach and Link shaderObjects
        this.shaderProgramObject = attachAndLinkShaderObjects(
            [this.vertexShaderObject, this.fragmentShaderObject],
            [VertexAttributeEnum.CAM_ATTRIBUTE_POSITION, VertexAttributeEnum.CAM_ATTRIBUTE_NORMAL,VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS],
            ["aPosition", "aNormal","aTexcoord"]
        );
        if (this.shaderProgramObject == null) {
            alert("ShaderProgramObject failed");
        }

        // uniforms
        //this.mvpMatrixUniform = this.getUniformLocation("uMVPMatrix");
        this.modelMatrixUniform = this.getUniformLocation("uModelMatrix");
        this.viewMatrixUniform = this.getUniformLocation("uViewMatrix");
        this.projectionMatrixUniform = this.getUniformLocation("uProjectionMatrix");
        this.textureRockSamplerUniform = this.getUniformLocation("uTextureRockSampler");
        this.textureMuddSamplerUniform = this.getUniformLocation("uTextureMuddSampler");
        this.textureGroundSamplerUniform = this.getUniformLocation("uTextureGroundSampler");

        this.height1Uniform = this.getUniformLocation("uHeightOne");
        this.height2Uniform = this.getUniformLocation("uHeightTwo");
        this.height3Uniform = this.getUniformLocation("uHeightThree");

        this.lightEnabledUniform = this.getUniformLocation("ulightEnabled");
        this.lightPositionUniform = this.getUniformLocation("uLightPosition");
        this.lightDiffuseUniform = this.getUniformLocation("uLightDiffuse");
    }

    createShaderObjects(shaderStr, shaderType)
    {
        let shaderObject = createAndCompileShaders(shaderStr, shaderType);
        if (shaderObject == null) 
        {
            alert(shaderType + " Shader Compilation failed !!!");
            return null;
        }

        return shaderObject;
    }

    getUniformLocation(uniform)
    {
        return gl.getUniformLocation(this.shaderProgramObject, uniform);
    }

    getShaderProgramObject()
    {
        return this.shaderProgramObject;
    }

    uninitializeShader()
    {
        uninitializeShaders();
    }
};