class TerrainShader{

    constructor() {
        // Shaders
        this.shaderProgramObject = null;

        // Uniforms
        this.mvpMatrixUniform;
        this.modelMatrixUniform;
        this.viewMatrixUniform;
        this.modelViewMatrixUnifrom;
        this.projectionMatrixUniform;

        this.textureSampler1Uniform;
        this.textureSampler2Uniform;
        this.textureSampler3Uniform;
        this.textureSampler4Uniform;

        this.height1Uniform;
        this.height2Uniform;
        this.height3Uniform;
        this.height4Uniform;


        this.lightEnabledUniform;
        this.lightDiffuseUniform;
        this.lightPositionUniform;


        this.keyInputUniform;


        this.vertexShaderObject;
        this.fragmentShaderObject;

        // vertex shader
        // vertex shader
        var vertexShaderSourceCode = 
        `#version 300 es
        \n
        precision highp float;
        in vec4 aPosition;
        in vec4 aColor;
        in vec2 aTexCoord;
        in vec3 aNormal;
        uniform mat4 uModelViewMatrix;
        uniform mat4 uProjectionMatrix;
        uniform lowp int ulightEnabled;
        uniform vec4 uLightPosition;
        uniform vec3 uLightDiffuse;
        out vec3 oDiffuseLight;
        out vec4 oColor;
        out vec2 oTexCoord;
        out vec3 oPosition;

        void main(void)
        {
            //gl_Position = uMVPMatrix * vec4(aPosition,1.0);
            //oColor = aColor;
            oTexCoord = aTexCoord;
            oPosition = vec3(aPosition.x,aPosition.y,aPosition.z);

            if(ulightEnabled == 1)
            {
                vec4 eyePosition =  uModelViewMatrix * aPosition;
                mat3 normalMatrix = mat3(transpose(inverse(uModelViewMatrix)));
                vec3 n = normalize(normalMatrix * aNormal );
                vec3 s = normalize(vec3(uLightPosition - eyePosition));
                oDiffuseLight = uLightDiffuse * max(dot(s,n),0.0);
            }
            else
            {
                oDiffuseLight = vec3(0.0,0.0,0.0);
            }
            gl_Position = uProjectionMatrix * uModelViewMatrix * aPosition;
        }`;

        // vertex shader object
        this.vertexShaderObject = this.createShaderObjects(vertexShaderSourceCode, gl.VERTEX_SHADER);

        // fragment shader
        var fragmentShaderSourceCode =
        `#version 300 es
        \n
        precision highp float;
        in vec4 oColor;
        in vec2 oTexCoord;
        in vec3 oPosition;
        in vec3 oDiffuseLight;
        uniform lowp int ulightEnabled;
        uniform sampler2D uTexture1Sampler;
        uniform sampler2D uTexture2Sampler;
        uniform sampler2D uTexture3Sampler;
        uniform sampler2D uTexture4Sampler;
        uniform float uHeightOne;
        uniform float uHeightTwo;
        uniform float uHeightThree;
        uniform float uHeightFour;
        uniform lowp int keyInput;

        out vec4 FragColor;

        vec4 calculatedTexcolor()
        {
            vec4 color1;
            vec4 color2;
            float delta;
            float factor;
            vec4 calTexColor;

            float height = oPosition.y;


            if(height < 5.0)
            //if(keyInput == 1)
            {
                calTexColor = texture(uTexture1Sampler,oTexCoord);
            }
            else if(height < 12.0)
            //else if(keyInput  == 2)
            {
                color1 = texture(uTexture1Sampler,oTexCoord);
                color2 = texture(uTexture2Sampler,oTexCoord);
                delta = 12.0 - 5.0;
                factor = (height - 5.0)/delta;

                calTexColor =mix(color1,color2,factor);
            }
            else if(height < 18.0)
            // else if(keyInput  == 3)
            {
                color1 = texture(uTexture2Sampler,oTexCoord);
                color2 = texture(uTexture3Sampler,oTexCoord);
                delta = 18.0 - 12.0;
                factor = (height - 12.0)/delta;

                calTexColor = mix(color1,color2,factor);
            }
            else if(height < 22.0)
            //else if(keyInput  == 4)
            {
                color1 = texture(uTexture3Sampler,oTexCoord);
                color2 = texture(uTexture4Sampler,oTexCoord);
                delta = 22.0 - 18.0;
                factor = (height - 18.0)/delta;

                calTexColor = mix(color1,color2,factor);
            }
            else
            {
                calTexColor = texture(uTexture4Sampler,oTexCoord);
                //calTexColor = vec4(1.0,1.0,1.0,1.0);
            }

            return calTexColor;
        }

        void main(void)
        {
            
            //FragColor = texture(uTexture1Sampler,oTexCoord) + texture(uTexture2Sampler,oTexCoord) + texture(uTexture3Sampler,oTexCoord) + texture(uTexture4Sampler,oTexCoord);
            //FragColor = vec4(1.0f,1.0f,1.0f,1.0f);

            vec4 texColor = calculatedTexcolor();

            //FragColor = texColor;


            if(ulightEnabled == 1)
            {
                FragColor = vec4(oDiffuseLight,1.0) * texColor;
            }
            else
            {
                FragColor = texColor;
            }
            FragColor = vec4(1.0f,1.0f,1.0f,1.0f);
        }`;

        // fragment shader object
        this.fragmentShaderObject = this.createShaderObjects(fragmentShaderSourceCode, gl.FRAGMENT_SHADER);
        
        // Attach and Link shaderObjects
        this.shaderProgramObject = attachAndLinkShaderObjects(
            [this.vertexShaderObject, this.fragmentShaderObject],
            [VertexAttributeEnum.CAM_ATTRIBUTE_POSITION, VertexAttributeEnum.CAM_ATTRIBUTE_COLOR,VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS,VertexAttributeEnum.CAM_ATTRIBUTE_NORMAL],
            ["aPosition", "aColor","aTexCoord","aNormal"]
        );
        if (this.shaderProgramObject == null) {
            alert("ShaderProgramObject failed");
        }

        // uniforms
        this.modelViewMatrixUnifrom = this.getUniformLocation("uModelViewMatrix");
        this.projectionMatrixUniform = this.getUniformLocation("uProjectionMatrix");
        this.textureSampler1Uniform = this.getUniformLocation("uTexture1Sampler");
        this.textureSampler2Uniform = this.getUniformLocation("uTexture2Sampler");
        this.textureSampler3Uniform = this.getUniformLocation("uTexture3Sampler");
        this.textureSampler4Uniform = this.getUniformLocation("uTexture4Sampler");
        this.height1Uniform = this.getUniformLocation("uHeightOne");
        this.height2Uniform = this.getUniformLocation("uHeightTwo");
        this.height3Uniform = this.getUniformLocation("uHeightThree");
        this.height4Uniform = this.getUniformLocation("uHeightFour");
        this.keyInputUniform = this.getUniformLocation("keyInput");
        this.lightEnabledUniform = this.getUniformLocation("ulightEnabled");
        this.lightPositionUniform = this.getUniformLocation("uLightPosition");
        this.lightDiffuseUniform = this.getUniformLocation("uLightDiffuse");

    }

    createShaderObjects(shaderStr, shaderType)
    {
        let shaderObject = createAndCompileShaders(shaderStr, shaderType);
        if (shaderObject == null) 
        {
            alert(shaderType + " Shader Compilation failed !!!");
            return null;
        }

        return shaderObject;
    }

    getUniformLocation(uniform)
    {
        return gl.getUniformLocation(this.shaderProgramObject, uniform);
    }

    getShaderProgramObject()
    {
        return this.shaderProgramObject;
    }

    uninitializeShader()
    {
        uninitializeShaders();
    }
};