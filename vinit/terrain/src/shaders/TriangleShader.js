class TriangleShader{

    constructor() {
        // Shaders
        this.shaderProgramObject = null;

        // Uniforms
        this.mvpMatrixUniform;
        this.modelMatrixUniform;
        this.viewMatrixUniform;
        this.projectionMatrixUniform;

        this.vertexShaderObject;
        this.fragmentShaderObject;

        // vertex shader
        // vertex shader
        var vertexShaderSourceCode = 
        `#version 300 es
        \n
        precision highp float;
        in vec4 aPosition;
        uniform mat4 uMVPMatrix;
        in vec4 aColor;
        out vec4 oColor;
        void main(void)
        {
        gl_Position = uMVPMatrix * aPosition;
        oColor = aColor;
        }`;

        // vertex shader object
        this.vertexShaderObject = this.createShaderObjects(vertexShaderSourceCode, gl.VERTEX_SHADER);

        // fragment shader
        var fragmentShaderSourceCode =
        `#version 300 es
        \n
        precision highp float;
        in vec4 oColor;
        out vec4 FragColor;
        void main(void)
        {
        FragColor = vec4(1.0, 1.0, 1.0, 1.0);
        }`;

        // fragment shader object
        this.fragmentShaderObject = this.createShaderObjects(fragmentShaderSourceCode, gl.FRAGMENT_SHADER);
        
        // Attach and Link shaderObjects
        this.shaderProgramObject = attachAndLinkShaderObjects(
            [this.vertexShaderObject, this.fragmentShaderObject],
            [VertexAttributeEnum.CAM_ATTRIBUTE_POSITION, VertexAttributeEnum.CAM_ATTRIBUTE_COLOR],
            ["aPosition", "aColor"]
        );
        if (this.shaderProgramObject == null) {
            alert("ShaderProgramObject failed");
        }

        // uniforms
        this.mvpMatrixUniform = this.getUniformLocation("uMVPMatrix");
    }

    createShaderObjects(shaderStr, shaderType)
    {
        let shaderObject = createAndCompileShaders(shaderStr, shaderType);
        if (shaderObject == null) 
        {
            alert(shaderType + " Shader Compilation failed !!!");
            return null;
        }

        return shaderObject;
    }

    getUniformLocation(uniform)
    {
        return gl.getUniformLocation(this.shaderProgramObject, uniform);
    }

    getShaderProgramObject()
    {
        return this.shaderProgramObject;
    }

    uninitializeShader()
    {
        uninitializeShaders();
    }
};