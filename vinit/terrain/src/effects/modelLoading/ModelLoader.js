class ModelLoader
{
    constructor()
    {
        this.modelShader = new ModelShader();

        this.modelPlacer = new ModelPlacer();
    }

    async initialize()
    {
        // this.modelShader.initialize('./assets/models/windmill2/windmill.obj');
        // this.modelShader.initialize('./assets/models/Tree2/trees9.obj');
        // this.modelShader.initialize('./assets/models/Tree1/tree_small_02_4k.obj');
        // this.modelShader.initialize('./assets/models/Tree1/untitled2.obj');
        // this.modelShader.initialize('./assets/models/stone/boulder_01_2k.obj');
        // this.modelShader.initialize('./assets/models/Tree3/jacaranda_treeV7.obj');
        // this.modelShader.initialize('./assets/models/Soldier01/Soldier02.obj');
        this.modelShader.initialize('./assets/models/Train/train.obj');
    }

    display(camera, doGodray)
    {
        this.modelShader.renderModel(this.modelPlacer.getModelMatrix(), camera, doGodray);
    }

    keyEventModel(key)
    {
        // this.modelShader.keyEvent(key);
        this.modelPlacer.getKeyForTransformation(key);
    }

    uninitialize()
    {
        if (this.modelShader)
        {
            delete this.modelShader;
        }

        if (this.modelPlacer)
        {
            delete this.modelPlacer;
        }
    }
};