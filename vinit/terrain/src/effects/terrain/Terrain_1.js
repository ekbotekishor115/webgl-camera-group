

class Terrain
{
    constructor()
    {
        this.img;
        this.heightMapData = [];
        this.vertices = [];
        this.indices = [];
        this.imageHeight = 0.0;
        this.imageWidth = 0.0;
        this.triangleListVAO;
        this.triangleListPositionVBO;
        this.triangleListIBO;
        this.shader = new TerrainShader();
        this.shaderProgramObject = this.shader.getShaderProgramObject();
    }
    init(filename)
    {
        this.img = new Image();
        this.img.src = filename;
    }

    loadHeightMap()
    {
        
        // var image = new Image();
        // image.src = filename;
        // image.onload = function()
        // {
        //     width = image.width;
        //     height = image.height;

        //     console.log("Height : " +  width + " width : " + height);
        // }

      

        const ctx = document.createElement("canvas").getContext("2d"); //using 2d canvas to read image
        ctx.canvas.width = this.img.width;
        ctx.canvas.height = this.img.height;
        ctx.drawImage(this.img, 0, 0);
        var imgData = ctx.getImageData(0, 0, ctx.canvas.width, ctx.canvas.height);
        
        const width = imgData.width;
        this.imageWidth = width;
        const height = imgData.height;
        this.imageHeight = height;

        console.log("Height : " +  width + " width : " + height);

        
        var bytePerPixel, pixeloffset,r,g,b,a;

        for(let i=0; i <  height; ++i)
        {
            for(let j=0; j < width; ++j)
            {
                // bytePerPixel = 4;
                // pixeloffset = imgData.data + (i + this.imageWidth * j) * bytePerPixel;
                // r = pixeloffset[0];
                // g = pixeloffset[1];
                // b = pixeloffset[2];
                // a = 4 >= 4 ? pixeloffset[3] : 0xff;
                pixeloffset = ((i * width + j) * 4);
                r = imgData.data[pixeloffset];

                this.heightMapData[i*width + j] = r / 255.0;
               
            }
        }
        console.log(this.heightMapData);
        
        /*
        const gridWidth = width - 1;
        const gridDepth = height - 1;
        const gridPoints = [];
        for (let z = 0; z <= gridDepth; ++z)
        {
            for (let x = 0; x <= gridWidth; ++x)
            {
                const offset = (z * width + x) * 4;
                // height 0 to 10
                const height = imgData.data[offset] * 10 / 255;
                gridPoints.push(x, height, z);
            }
        }
        */
        

        //shader
        this.shaderProgramObject = this.shader.getShaderProgramObject();

        this.createTriangleList();

        // const ext = gl.getExtension('OES_element_index_uint');
        // if (!ext) {
        //   // fall back to using gl.UNSIGNED_SHORT or tell the user they are out of luck
        //   console.log("out of bound");
        // }
    }
    render(camera)
    {
        
        this.renderTriangleList(camera);
        //this.destroyTriangleList();
    }

    createTriangleList()
    {
        this.prepareAndBindObjectBuffer();
        this.fillObjectBuffer();
        this.fillVertexAttribForObjectBuffer();
        this.unbindObjectBuffer();
    }
    prepareAndBindObjectBuffer()
    {
        //bind VAO
        this.triangleListVAO =  gl.createVertexArray();
        gl.bindVertexArray(this.triangleListVAO);

        //bind VBO
        this.triangleListPositionVBO = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER,this.triangleListPositionVBO);


        this.triangleListIBO = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,this.triangleListIBO);
    }

    fillObjectBuffer()
    {
        this.vertices =  this.initVertices();
        this.indices = this.initIndices();

        //buffer filling
        gl.bufferData(gl.ARRAY_BUFFER,new Float32Array(this.vertices),gl.STATIC_DRAW);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,new Uint16Array(this.indices),gl.STATIC_DRAW);
    }

    fillVertexAttribForObjectBuffer()
    {
        var offset = 0;

        gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);//stride and offset later
        gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION);

    }

    unbindObjectBuffer()
    {
        
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);
        gl.bindVertexArray(null);
    }

    renderTriangleList(camera)
    {
        var modelMatrix = mat4.create();
        var viewMatrix = mat4.create();
        var modelViewMatrix = mat4.create();

        gl.useProgram(this.shaderProgramObject);

        viewMatrix = camera.getViewMatrix();

        mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,-10.0]);

        mat4.multiply(modelViewMatrix,viewMatrix,modelMatrix);

        
        gl.uniformMatrix4fv(this.shader.modelViewMatrixUnifrom,false,modelViewMatrix);
        gl.uniformMatrix4fv(this.shader.projectionMatrixUniform,false,perspectiveProjectionMatrix);

        //bind vbao
        gl.bindVertexArray(this.triangleListVAO);

        //draw
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,this.triangleListIBO);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,new Uint16Array(this.indices),gl.STATIC_DRAW);
        
        //gl.drawElements(gl.TRIANGLES,(this.imageHeight -1) *(this.imageWidth-1) * 6,gl.UNSIGNED_INT,0);
        gl.drawElements(gl.TRIANGLES,(this.imageHeight-1) *(this.imageWidth-1) * 3,gl.UNSIGNED_SHORT,0);
        //gl.drawArrays(gl.TRIANGLES,0,this.indices.length);

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,null);
        //unbind VAO
        gl.bindVertexArray(null);

        gl.useProgram(null);
    }

    initVertices()
    {
        //var idx = 0;
        var z;
        // var vertices = new Array();
        var vertices = [];
        for(z=0; z < this.imageHeight; ++z)
        {
            for(let x=0; x < this.imageWidth;++x)
            {
                // this.vertices[z][x][0] = 0.1 * x;
                // this.vertices[z][x][1] = 0.5 * this.heightMapData[z*this.imageHeight + x];
                // this.vertices[z][x][2] = 1.0 * z;

                //vertex[z][x][0];

               
                // vertices[z] = new Array();
                // vertices[z][x] = new Array();
                // vertices[z][x][0] = 0.1 * x;

                // vertices[z][x][1] = 0.5 * this.heightMapData[z*this.imageHeight + x];

                // vertices[z][x][2] = 1.0 * z;

                // vertex[z + (x * this.imageHeight) + (0*this.imageWidth*this.imageHeight)] = 0.1 * x;
                // vertex[z + (x * this.imageHeight) + (1*this.imageWidth*this.imageHeight)] = 0.5 * this.heightMapData[z*this.imageHeight + x];
                // vertex[z + (x * this.imageHeight) + (2*this.imageWidth*this.imageHeight)] = 1.0 * z;


                // vertices[0 + (z * this.imageHeight) + (x * this.imageHeight * this.imageWidth)] = 0.1 * x;
                // vertices[1 + (z * this.imageHeight) + (x * this.imageHeight * this.imageWidth)] = 0.5 * this.heightMapData[z*this.imageHeight + x];
                // vertices[2 + (z * this.imageHeight) + (x * this.imageHeight * this.imageWidth)] = 1.0 * z;

                //x = z y =x z= 0
                vertices[x + (z * this.imageWidth) + (0 * this.imageHeight * this.imageWidth)] = 0.1 * x;
                vertices[x + (z * this.imageWidth) + (1 * this.imageHeight * this.imageWidth)] = 30.0 * 0.5 * this.heightMapData[z*this.imageHeight + x];
                vertices[x + (z * this.imageWidth) + (2 * this.imageHeight * this.imageWidth)] = 1.0 * z;


            }   
        }
        console.log("vertices length :" + vertices.length);
        console.log(vertices);
        return vertices;
        //console.log(this.vertices);
        // console.log(vertex);
    }

    initIndices()
    {
        var idx = 0;

        var indexBottomLeft;
        var indexBottomRight;
        var indexTopLeft;
        var indexTopRight;

        var indice = [];


        for(let z=0; z < this.imageHeight -1 ; ++z)
        {
            for(let x=0; x < this.imageWidth-1 ; ++x)
            {
                indexBottomLeft = z * this.imageWidth + x;
                indexTopLeft = (z+1) * this.imageWidth + x;
                indexTopRight = (z+1)* this.imageWidth + x + 1;
                indexBottomRight = z * this.imageWidth + x + 1;

                //fill top left triangle
               
                indice[idx++] = indexBottomLeft;

                indice[idx++] = indexTopLeft;

                indice[idx++] = indexTopRight;
                
                
                //fill bottom traingle
                indice[idx++] = indexBottomLeft;

                indice[idx++] = indexTopRight;

                indice[idx++] = indexBottomRight;

            }
        }

        console.log( " indices length " + indice.length);

        //console.log(indice);

        return indice;
    }

    destroyTriangleList()
    {
        if (this.shader)
        {
            this.shader.uninitializeShader();
            this.shader = null;
        }

        if (this.triangleListPositionVBO)
        {
            gl.deleteBuffer(this.triangleListPositionVBO);
            this.triangleListPositionVBO = null;
        }

        if (this.triangleListIBO)
        {
            gl.deleteBuffer(this.triangleListIBO);
            this.triangleListIBO = null;
        }
        
        if (this.triangleListVAO)
        {
            gl.deleteVertexArray(this.triangleListVAO);
            this.triangleListVAO = null;
        }
    }
}