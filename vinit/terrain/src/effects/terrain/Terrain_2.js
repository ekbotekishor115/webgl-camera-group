

class Terrain2
{
    constructor()
    {
        this.img;
        this.shader = new Terrain2Shader();
        this.shaderProgramObject = this.shader.getShaderProgramObject();
        this.terrain2VAO = null;
        this.terrain2PositionVBO = null;
        this.terrain2IndicesVBO = null;
        this.terrain2NormalVBO = null;
        this.terrain2TexcoordVBO = null;
        this.imageWidth;
        this.imageHeight;
    }
    init(filename)
    {
        this.img = new Image();
        this.img.src = filename;
    }

    loadHeightMap()
    {
        const ctx = document.createElement('canvas').getContext('2d');
        // ctx.canvas.width = this.img.width;
        // ctx.canvas.height = this.img.height;
        ctx.canvas.width = 156;
        ctx.canvas.height = 156;
        ctx.drawImage(this.img,0,0);

        // console.log("image width " + this.img.width + "image height " + this.img.height);

        const imgData = ctx.getImageData(0,0, ctx.canvas.width, ctx.canvas.height);
        
        console.log("image width " + imgData.width + "image height " + imgData.height);

        function getHeight(offset)
        {
            const v = imgData.data[offset * 4]; //4~RGBA
            return v *20/255; //0 to 10
        }

        this.imageHeight = imgData.height;
        this.imageWidth = imgData.width;

        const positions = [];
        const texcoords = [];
        const indices = [];
        const height = [];


        const cellsAcross = imgData.width - 1;
        const cellsDeep = imgData.height - 1;

        this.numVertices = (cellsAcross * 2 * (cellsDeep + 1)) +
                      (cellsDeep * 2 * (cellsAcross + 1));

        for(let z = 0 ; z < cellsDeep; ++z)
        {
            for(let x = 0 ; x < cellsAcross; ++x)
            {
                const base0 = z * imgData.width + x;
                const base1 = base0 + imgData.width;

                const h00 = getHeight(base0);
                const h01 = getHeight(base0 + 1);
                const h10 = getHeight(base1);
                const h11 = getHeight(base1 + 1);
                const hm = (h00 + h01 + h10 + h11) / 4;

                height.push(h00,);
                height.push(h01,);
                height.push(h10,);
                height.push(h11,);
                height.push(hm,);


                const x0 = x;
                const x1 = x + 1;
                const z0 = z;
                const z1 = z + 1;
                
                const ndx = positions.length/3;

                positions.push(x0,h00,z0,  
                              x1,h01,z0,  
                              x0,h10,z1,  
                              x1,h11,z1,  
                              (x0+x1) / 2, hm, (z0+z1) / 2,);

                const u0 = x / cellsAcross;
                const v0 = z / cellsDeep;
                const u1 = (x + 1) / cellsAcross;
                const v1 = (z + 1) / cellsDeep;
                texcoords.push(u0, v0,  u1, v0,  u0, v1,  u1, v1,  (u0 + u1) / 2, (v0 + v1) / 2,);

                indices.push(ndx, ndx + 4, ndx + 1,  
                            ndx, ndx + 2, ndx + 4,  
                            ndx + 2, ndx + 3, ndx + 4,  
                            ndx + 1, ndx + 4, ndx + 3,);

            }
        }

        //console.log("height array : " + height);


        const maxAngle = 2 * Math.PI / 180;  // make them facetted
        this.arrays = this.generateNormals({position: positions,  texcoord: texcoords,  indices,}, maxAngle);

        this.numVerts = this.arrays.position.length;
        // this.numVerts = this.arrays.indices.length;
        console.log(this.numVerts);

        //create VAO
        this.terrain2VAO = gl.createVertexArray();
        gl.bindVertexArray(this.terrain2VAO);

        this.terrain2PositionVBO = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER,this.terrain2PositionVBO);
        gl.bufferData(gl.ARRAY_BUFFER,new Float32Array(this.arrays.position),gl.STATIC_DRAW);
        // gl.bufferData(gl.ARRAY_BUFFER,new Float32Array(positions),gl.STATIC_DRAW);
        gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_POSITION);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        this.terrain2NormalVBO = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER,this.terrain2NormalVBO);
        gl.bufferData(gl.ARRAY_BUFFER,new Float32Array(this.arrays.normal),gl.STATIC_DRAW);
        gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_NORMAL, 3, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_NORMAL);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);


        this.terrain2TexcoordVBO = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER,this.terrain2TexcoordVBO);
        gl.bufferData(gl.ARRAY_BUFFER,new Float32Array(this.arrays.texcoord),gl.STATIC_DRAW);
        gl.vertexAttribPointer(VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS, 2, gl.FLOAT, false, 0, 0);
        gl.enableVertexAttribArray(VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS);
        gl.bindBuffer(gl.ARRAY_BUFFER, null);

        this.terrain2IndicesVBO = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,this.terrain2IndicesVBO);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,new Uint16Array(this.arrays.indices),gl.STATIC_DRAW);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);

        gl.bindVertexArray(null);


        this.texture = loadTextureFromFile('./assets/textures/terrain/rock.jpg');
        this.texture2 = loadTextureFromFile('./assets/textures/terrain/lava.jpg');
        this.texture3 = loadTextureFromFile('./assets/textures/terrain/ground.png');

    }

    render(camera)
    {
        gl.useProgram(this.shaderProgramObject);
        gl.enable(gl.DEPTH_TEST);
        gl.enable(gl.CULL_FACE);

      

        var modelMatrix = mat4.create();
        var viewMatrix = mat4.create();

        var lightPosition = camera.getEye();

        var diffuseLight = [0.6,0.6,0.6];

        viewMatrix = camera.getViewMatrix();


        mat4.translate(modelMatrix,modelMatrix,[-200.0,0.0,-10.0]);
        mat4.scale(modelMatrix,modelMatrix,[4.0,2.0,4.0]);

        gl.uniformMatrix4fv(this.shader.modelMatrixUniform, false, modelMatrix);
        gl.uniformMatrix4fv(this.shader.viewMatrixUniform,false,viewMatrix);
        gl.uniformMatrix4fv(this.shader.projectionMatrixUniform,false,perspectiveProjectionMatrix);


        gl.uniform1f(this.shaderProgramObject.height1Uniform, 2.0 * 2.0);
        gl.uniform1f(this.shaderProgramObject.height2Uniform, 4.0 * 2.0);
        gl.uniform1f(this.shaderProgramObject.height3Uniform, 6.0 * 2.0);

        gl.uniform1i(this.shaderProgramObject.lightEnabledUniform, 1);

        gl.uniform3fv(this.shader.lightPositionUniform,lightPosition);

        gl.uniform3fv(this.shader.lightDiffuseUniform,diffuseLight);

        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, this.texture);
        gl.uniform1i(this.shaderProgramObject.textureRockSamplerUniform, 0);

        gl.activeTexture(gl.TEXTURE1);
        gl.bindTexture(gl.TEXTURE_2D, this.texture2);
        gl.uniform1i(this.shaderProgramObject.textureMuddSamplerUniform, 1);

        gl.activeTexture(gl.TEXTURE1);
        gl.bindTexture(gl.TEXTURE_2D, this.texture3);
        gl.uniform1i(this.shaderProgramObject.textureGroundSamplerUniform, 2);

        gl.bindVertexArray(this.terrain2VAO);

        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER,this.terrain2IndicesVBO);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER,new Uint16Array(this.arrays.indices),gl.STATIC_DRAW);
        // gl.drawElements(gl.TRIANGLES, this.numVertices, gl.UNSIGNED_SHORT, null);
        gl.drawElements(gl.TRIANGLES, this.numVerts/3 , gl.UNSIGNED_SHORT, null);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
        // gl.drawArrays(gl.TRIANGLE_FAN, 0, this.numVerts/3);

        gl.bindVertexArray(null);
        gl.bindTexture(gl.TEXTURE_2D, null);

        gl.useProgram(null);
    }




    generateNormals(arrays, maxAngle) 
    {
        const positions = arrays.position;
        const texcoords = arrays.texcoord;
    
        // first compute the normal of each face
        let getNextIndex = this.makeIndiceIterator(arrays);
        const numFaceVerts = getNextIndex.numElements;
        const numVerts = arrays.position.length;
        const numFaces = numFaceVerts / 3;
        const faceNormals = [];
    
        // Compute the normal for every face.
        // While doing that, create a new vertex for every face vertex
        for (let i = 0; i < numFaces; ++i) {
          const n1 = getNextIndex() * 3;
          const n2 = getNextIndex() * 3;
          const n3 = getNextIndex() * 3;
    
          const v1 = positions.slice(n1, n1 + 3);
          const v2 = positions.slice(n2, n2 + 3);
          const v3 = positions.slice(n3, n3 + 3);

          // console.log(n1);
          // console.log(n2);
          // console.log(n3);

          let v1_arr = vec3.fromValues(v1[0], v1[1], v1[2]);
          let v2_arr = vec3.fromValues(v2[0], v2[1], v2[2]);
          let v3_arr = vec3.fromValues(v3[0], v3[1], v3[2]);

          var subtraction1 = vec3.create();
          vec3.subtract(subtraction1,v1_arr,v2_arr);

          var subtraction2 = vec3.create();
          vec3.subtract(subtraction2,v3_arr,v2_arr);

          var crossProduct = vec3.create();
          vec3.cross(crossProduct,subtraction1,subtraction2);

          var normal = vec3.create();

          vec3.normalize(normal,crossProduct);

          //faceNormals.push(m4.normalize(m4.cross(m4.subtractVectors(v1, v2), m4.subtractVectors(v3, v2))));
          
          faceNormals.push(normal);

        }
    
        let tempVerts = {};
        let tempVertNdx = 0;
    
        // this assumes vertex positions are an exact match
    
        function getVertIndex(x, y, z) 
        {
    
          const vertId = x + "," + y + "," + z;
          const ndx = tempVerts[vertId];
          if (ndx !== undefined) {
            return ndx;
          }
          const newNdx = tempVertNdx++;
          tempVerts[vertId] = newNdx;
          return newNdx;
        }
    
        // We need to figure out the shared vertices.
        // It's not as simple as looking at the faces (triangles)
        // because for example if we have a standard cylinder
        //
        //
        //      3-4
        //     /   \
        //    2     5   Looking down a cylinder starting at S
        //    |     |   and going around to E, E and S are not
        //    1     6   the same vertex in the data we have
        //     \   /    as they don't share UV coords.
        //      S/E
        //
        // the vertices at the start and end do not share vertices
        // since they have different UVs but if you don't consider
        // them to share vertices they will get the wrong normals
    
        const vertIndices = [];
        for (let i = 0; i < numVerts; ++i) {
          const offset = i * 3;
          const vert = positions.slice(offset, offset + 3);
          vertIndices.push(getVertIndex(vert));
        }
    
        // go through every vertex and record which faces it's on
        const vertFaces = [];
        getNextIndex.reset();
        for (let i = 0; i < numFaces; ++i) {
          for (let j = 0; j < 3; ++j) {
            const ndx = getNextIndex();
            const sharedNdx = vertIndices[ndx];
            let faces = vertFaces[sharedNdx];
            if (!faces) {
              faces = [];
              vertFaces[sharedNdx] = faces;
            }
            faces.push(i);
          }
        }
    
        // now go through every face and compute the normals for each
        // vertex of the face. Only include faces that aren't more than
        // maxAngle different. Add the result to arrays of newPositions,
        // newTexcoords and newNormals, discarding any vertices that
        // are the same.
        tempVerts = {};
        tempVertNdx = 0;
        const newPositions = [];
        const newTexcoords = [];
        const newNormals = [];
    
        function getNewVertIndex(x, y, z, nx, ny, nz, u, v) 
        {
          const vertId =
              x + "," + y + "," + z + "," +
              nx + "," + ny + "," + nz + "," +
              u + "," + v;
    
          const ndx = tempVerts[vertId];
          // console.log(ndx);
          if (ndx !== undefined) {
            return ndx;
          }
          const newNdx = tempVertNdx++;
          tempVerts[vertId] = newNdx;
          newPositions.push(x, y, z);
          newNormals.push(nx, ny, nz);
          newTexcoords.push(u, v);
          return newNdx;
        }
    
        const newVertIndices = [];
        getNextIndex.reset();
        const maxAngleCos = Math.cos(maxAngle);
        // for each face
        for (let i = 0; i < numFaces; ++i) {
          // get the normal for this face
          const thisFaceNormal = faceNormals[i];
          // for each vertex on the face
          for (let j = 0; j < 3; ++j) 
           {
                const ndx = getNextIndex();
                const sharedNdx = vertIndices[ndx];
                const faces = vertFaces[sharedNdx];
                const norm = [0, 0, 0];
                faces.forEach(faceNdx => {
                // is this face facing the same way
                const otherFaceNormal = faceNormals[faceNdx];
                // const dot = m4.dot(thisFaceNormal, otherFaceNormal);
                const dot = vec3.dot(thisFaceNormal, otherFaceNormal);
                if (dot > maxAngleCos) 
                {
                    //m4.addVectors(norm, otherFaceNormal, norm);
                    vec3.add(norm, otherFaceNormal, norm);
                }
            });
            //m4.normalize(norm, norm);
            vec3.normalize(norm, norm);
            const poffset = ndx * 3;
            const toffset = ndx * 2;
            newVertIndices.push(getNewVertIndex(
                positions[poffset + 0], positions[poffset + 1], positions[poffset + 2],
                norm[0], norm[1], norm[2],
                texcoords[toffset + 0], texcoords[toffset + 1]));
          }
        }
    
        return {
          position: newPositions,
          texcoord: newTexcoords,
          normal: newNormals,
          indices: newVertIndices,
        };
    
    }


    makeIndexedIndicesFn(arrays)
    {
        const indices = arrays.indices;
        let ndx = 0;
        const fn = function() {
          return indices[ndx++];
        };
        fn.reset = function() {
          ndx = 0;
        };
        fn.numElements = indices.length;
        return fn;
    }
    
    makeUnindexedIndicesFn(arrays)
    {
        let ndx = 0;
        const fn = function() {
          return ndx++;
        };
        fn.reset = function() {
          ndx = 0;
        }
        fn.numElements = arrays.positions.length / 3;
        return fn;
    }
    
    makeIndiceIterator(arrays)
    {
        return arrays.indices? this.makeIndexedIndicesFn(arrays) : this.makeUnindexedIndicesFn(arrays);
    }

    update()
    {
        
    }


    uninitialize()
    {
        if (this.shader)
        {
            this.shader.uninitializeShader();
            this.shader = null;
        }

        if (this.terrain2PositionVBO)
        {
            gl.deleteBuffer(this.terrain2PositionVBO);
            this.terrain2PositionVBO = null;
        }

        if (this.terrain2TexcoordVBO)
        {
            gl.deleteBuffer(this.terrain2TexcoordVBO);
            this.terrain2TexcoordVBO = null;
        }

        if (this.terrain2NormalVBO)
        {
            gl.deleteBuffer(this.terrain2NormalVBO);
            this.terrain2NormalVBO = null;
        }

        if (this.terrain2IndicesVBO)
        {
            gl.deleteBuffer(this.terrain2IndicesVBO);
            this.terrain2IndicesVBO = null;
        }
        
        if (this.terrain2VAO)
        {
            gl.deleteVertexArray(this.terrain2VAO);
            this.terrain2VAO = null;
        }
    }
};