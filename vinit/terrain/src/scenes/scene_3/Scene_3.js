

class Scene_3
{
    constructor()
    {
        this.modelMatrix = new ModelPlacer();

        this.terrain = new Terrain2();
        this.railwayTrack = new RailwayTrack2();
    }

    init(heightmap)
    {
        this.initTerrain(heightmap);
    }

    initTerrain(heightmap)
    {
        this.terrain.init(heightmap);
        this.terrain.loadHeightMap();
    }

    render(camera,tiles)
    {
        this.terrain.render(camera);
        this.railwayTrack.render(camera,tiles,this.modelMatrix.getModelMatrix());
    }

    keyEventHandle(key)
    {
        this.modelMatrix.getKeyForTransformation(key);
    }

    uninitialize()
    {
        this.terrain.uninitialize();
        this.railwayTrack.destroy();
    }
};