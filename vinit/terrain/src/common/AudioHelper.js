class AudioHelper
{
    constructor()
    {
        this.SCENE0_START = 0.0;
        this.SCENE1_START = 29.5;
        this.SCENE2_START = 84.0;
        this.SCENE3_START = 127.0;
        this.SCENE4_START = 174.0;

        this.SceneSeek = [this.SCENE0_START, this.SCENE1_START, this.SCENE2_START, this.SCENE3_START, this.SCENE4_START];
        this.SceneNum = 0;

        this.variables = document.querySelector("#variableValues");
    }

    audioControls(key)
    {
        switch (key)
        {
            case 'p':
            case 'P':
                if (!this.audio.paused)
                {
                    this.audio.pause();
                }
                else
                {
                    this.audio.play();
                }
                break;
            case 'ArrowRight':
                this.SceneNum ++;
                if (this.SceneNum > 5)
                {
                    this.SceneNum = 0;
                }
                this.audio.fastSeek(this.SceneSeek[this.SceneNum]);
                break;
            case 'ArrowLeft':
                this.SceneNum --;
                if (this.SceneNum < 0)
                {
                    this.SceneNum = 0;
                }
                this.audio.fastSeek(this.SceneSeek[this.SceneNum]);
                break;
        }
    }

    play(song)
    {
        this.audio = new Audio(song);
        // var audio = new Audio('https://interactive-examples.mdn.mozilla.net/media/cc0-audio/t-rex-roar.mp3'); // Playing audio from web
        this.audio.play();
    }
    
    displaySongTime()
    {
        this.variables.textContent = `Music Time : ${this.audio.currentTime.toFixed(1)} Secs`;
    }

};