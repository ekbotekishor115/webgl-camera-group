class RailwayTrack2
{
    constructor()
    {
        this.leftTrack = new Shapes();
        this.rightTrack = new Shapes();
        this.tie = new Shapes();

        this.shader = new RailwayTrack2Shader();
        this.shaderProgramObject = this.shader.getShaderProgramObject();
    }

    render(cam,totalTiles)
    {
        var modelMatrix = mat4.create();
        var viewMatrix = mat4.create();
        var modelViewMatrix = mat4.create();
        var mvpMatrix = mat4.create();
        var trackLength = (totalTiles/10.0);

        gl.useProgram(this.shaderProgramObject);

        //left track
        
        mat4.identity(modelMatrix);
        mat4.identity(viewMatrix);

        viewMatrix = cam.getViewMatrix();

        mat4.translate(modelMatrix,modelMatrix,[4.0,0.65,-13.0 * trackLength]);

        mat4.scale(modelMatrix,modelMatrix,[0.2,0.2,15.0 * trackLength]);

        mat4.multiply(modelViewMatrix,viewMatrix,modelMatrix);

        mat4.multiply(mvpMatrix,perspectiveProjectionMatrix,modelViewMatrix);

        gl.uniformMatrix4fv(this.shader.mvpMatrixUniform,false,mvpMatrix);
        gl.uniformMatrix4fv(this.shader.modelMatrixUniform,false,modelMatrix);
        gl.uniformMatrix4fv(this.shader.viewMatrixUniform,false,viewMatrix);
        gl.uniformMatrix4fv(this.shader.projectionMatrixUniform,false,perspectiveProjectionMatrix);

        gl.uniform1i(this.shader.lightingEnabledUniform,1);

        gl.uniform3fv(this.shader.lightPositionUniform,cam.getEye());

        gl.uniform3fv(this.shader.laUniform,this.shader.lightAmbient);
        gl.uniform3fv(this.shader.kaUniform,this.shader.materialAmbient);

        gl.uniform3fv(this.shader.ldUniform,this.shader.lightDiffuse);
        gl.uniform3fv(this.shader.kdUniform,this.shader.materialDiffuse);

        gl.uniform3fv(this.shader.lsUniform,this.shader.lightSpecular);
        gl.uniform3fv(this.shader.ksUniform,this.shader.materialSpecular);

        gl.uniform1f(this.shader.materialShinessUniform,this.shader.materialShininess);



        this.leftTrack.drawCube();


        //right track
        
        mat4.identity(modelMatrix);
        mat4.identity(viewMatrix);

        viewMatrix = cam.getViewMatrix();

        mat4.translate(modelMatrix,modelMatrix,[-4.0,0.65,-13.0 * trackLength]);

        mat4.scale(modelMatrix,modelMatrix,[0.2,0.2,15.0 * trackLength]);

        mat4.multiply(modelViewMatrix,viewMatrix,modelMatrix);

        mat4.multiply(mvpMatrix,perspectiveProjectionMatrix,modelViewMatrix);

        gl.uniformMatrix4fv(this.shader.mvpMatrixUniform,false,mvpMatrix);
        gl.uniformMatrix4fv(this.shader.mvpMatrixUniform,false,mvpMatrix);
        gl.uniformMatrix4fv(this.shader.modelMatrixUniform,false,modelMatrix);
        gl.uniformMatrix4fv(this.shader.viewMatrixUniform,false,viewMatrix);
        gl.uniformMatrix4fv(this.shader.projectionMatrixUniform,false,perspectiveProjectionMatrix);

        gl.uniform1i(this.shader.lightingEnabledUniform,1);

        gl.uniform3fv(this.shader.lightPositionUniform,cam.getEye());

        gl.uniform3fv(this.shader.laUniform,this.shader.lightAmbient);
        gl.uniform3fv(this.shader.kaUniform,this.shader.materialAmbient);

        gl.uniform3fv(this.shader.ldUniform,this.shader.lightDiffuse);
        gl.uniform3fv(this.shader.kdUniform,this.shader.materialDiffuse);

        gl.uniform3fv(this.shader.lsUniform,this.shader.lightSpecular);
        gl.uniform3fv(this.shader.ksUniform,this.shader.materialSpecular);

        gl.uniform1f(this.shader.materialShinessUniform,this.shader.materialShininess);

        this.rightTrack.drawCube();



        //tie
        var noOfTiles = totalTiles;

        for(let i= 0 ; i < noOfTiles; ++i)
        {

            mat4.identity(modelMatrix);
            mat4.identity(viewMatrix);


            //matrix transformations
            viewMatrix = cam.getViewMatrix();

            mat4.translate(modelMatrix,modelMatrix,[0.0,0.0,(i*(-2.62))]);
            mat4.scale(modelMatrix,modelMatrix,[5.0,0.5,0.5]);

            mat4.multiply(modelViewMatrix,viewMatrix,modelMatrix);

            mat4.multiply(mvpMatrix,perspectiveProjectionMatrix,modelViewMatrix);

            gl.uniformMatrix4fv(this.shader.mvpMatrixUniform,false,mvpMatrix);
            gl.uniformMatrix4fv(this.shader.mvpMatrixUniform,false,mvpMatrix);
            gl.uniformMatrix4fv(this.shader.modelMatrixUniform,false,modelMatrix);
            gl.uniformMatrix4fv(this.shader.viewMatrixUniform,false,viewMatrix);
            gl.uniformMatrix4fv(this.shader.projectionMatrixUniform,false,perspectiveProjectionMatrix);

            gl.uniform1i(this.shader.lightingEnabledUniform,1);

            gl.uniform3fv(this.shader.lightPositionUniform,cam.getEye());

            gl.uniform3fv(this.shader.laUniform,this.shader.lightAmbient);
            gl.uniform3fv(this.shader.kaUniform,this.shader.materialAmbient);

            gl.uniform3fv(this.shader.ldUniform,this.shader.lightDiffuse);
            gl.uniform3fv(this.shader.kdUniform,this.shader.materialDiffuse);

            gl.uniform3fv(this.shader.lsUniform,this.shader.lightSpecular);
            gl.uniform3fv(this.shader.ksUniform,this.shader.materialSpecular);

            gl.uniform1f(this.shader.materialShinessUniform,this.shader.materialShininess);


            this.tie.drawCube();

        }

        gl.useProgram(null);

    }

    destroy()
    {
        this.leftTrack.uninitializeCube();
        this.rightTrack.uninitializeCube();
        this.tie.uninitializeCube();
    }
};