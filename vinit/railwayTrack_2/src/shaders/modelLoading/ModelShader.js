class ModelShader{

    constructor() {
        // Shaders
        this.shaderProgramObject = null;

        // Uniforms
        this.mvpMatrixUniform;
        this.modelMatrixUniform;
        this.viewMatrixUniform;
        this.projectionMatrixUniform;

        this.vertexShaderObject;
        this.fragmentShaderObject;

        // vertex shader
        // vertex shader
        var vertexShaderSourceCode = 
        `#version 300 es
        \n
        in vec4 a_position;
        in vec3 a_normal;
        in vec3 a_tangent;
        in vec2 a_texcoord;
        in vec4 a_color;

        uniform mat4 u_projection;
        uniform mat4 u_view;
        uniform mat4 u_world;
        uniform vec3 u_viewWorldPosition;
        
        out vec3 v_normal;
        out vec3 v_tangent;
        out vec3 v_surfaceToView;
        out vec2 v_texcoord;
        out vec4 v_color;
        
        void main() {
        vec4 worldPosition = u_world * a_position;
        gl_Position = u_projection * u_view * worldPosition;
        v_surfaceToView = u_viewWorldPosition - worldPosition.xyz;
        mat3 normalMat = mat3(u_world);
        
        v_normal = normalize(normalMat * a_normal);
        v_tangent = normalize(normalMat * a_tangent);
        
        v_texcoord = a_texcoord;
        v_color = a_color;
        }`;


        // vertex shader object
        this.vertexShaderObject = this.createShaderObjects(vertexShaderSourceCode, gl.VERTEX_SHADER);

        // fragment shader
        var fragmentShaderSourceCode =
        `#version 300 es
        \n
        precision highp float;
        in vec3 v_normal;
        in vec3 v_tangent;
        in vec3 v_surfaceToView;
        in vec2 v_texcoord;
        in vec4 v_color;

        uniform vec3 u_diffuse;
        uniform sampler2D diffuseMap;
        uniform vec3 ambient;
        uniform vec3 emissive;
        uniform vec3 specular;
        uniform sampler2D specularMap;
        uniform float shininess;
        uniform sampler2D normalMap;
        uniform float opacity;
        uniform vec3 u_lightDirection;
        uniform vec3 u_ambientLight;
        
        out vec4 FragColor;
        
        void main () {
        vec3 normal = normalize(v_normal) * ( float( gl_FrontFacing ) * 2.0 - 1.0 );
        vec3 tangent = normalize(v_tangent) * ( float( gl_FrontFacing ) * 2.0 - 1.0 );
        vec3 bitangent = normalize(cross(normal, tangent));
        mat3 tbn = mat3(tangent, bitangent, normal);
        normal = texture(normalMap, v_texcoord).rgb * 2. - 1.;
        normal = normalize(tbn * normal);
        vec3 surfaceToViewDirection = normalize(v_surfaceToView);
        vec3 halfVector = normalize(u_lightDirection + surfaceToViewDirection);
        float fakeLight = dot(u_lightDirection, normal) * .5 + .5;
        float specularLight = clamp(dot(normal, halfVector), 0.0, 1.0);
        vec4 specularMapColor = texture(specularMap, v_texcoord);
        vec3 effectiveSpecular = specular * specularMapColor.rgb;
        vec4 diffuseMapColor = texture(diffuseMap, v_texcoord);
        vec3 effectiveDiffuse = u_diffuse * diffuseMapColor.rgb * v_color.rgb;
        float effectiveOpacity = opacity * diffuseMapColor.a * v_color.a;
        
        FragColor = vec4(emissive +
        ambient * u_ambientLight +
        effectiveDiffuse * fakeLight +
        effectiveSpecular * pow(specularLight, shininess),
        effectiveOpacity);
        }`;


        // fragment shader object
        this.fragmentShaderObject = this.createShaderObjects(fragmentShaderSourceCode, gl.FRAGMENT_SHADER);
        
        // Attach and Link shaderObjects
        this.shaderProgramObject = attachAndLinkShaderObjects(
            [this.vertexShaderObject, this.fragmentShaderObject],
            [VertexAttributeEnum.CAM_ATTRIBUTE_POSITION, VertexAttributeEnum.CAM_ATTRIBUTE_NORMAL, VertexAttributeEnum.CAM_ATTRIBUTE_TANGENTS, VertexAttributeEnum.CAM_ATTRIBUTE_TEXCOORDS, VertexAttributeEnum.CAM_ATTRIBUTE_COLOR],
            ["aPosition", "a_normal", "a_tangent", "a_texcoord", "aColor"]
        );
        if (this.shaderProgramObject == null) {
            alert("ShaderProgramObject failed");
        }

        // uniforms
        this.mvpMatrixUniform = this.getUniformLocation("uMVPMatrix");
    }

    createShaderObjects(shaderStr, shaderType)
    {
        let shaderObject = createAndCompileShaders(shaderStr, shaderType);
        if (shaderObject == null) 
        {
            alert(shaderType + " Shader Compilation failed !!!");
            return null;
        }

        return shaderObject;
    }

    getUniformLocation(uniform)
    {
        return gl.getUniformLocation(this.shaderProgramObject, uniform);
    }

    getShaderProgramObject()
    {
        return this.shaderProgramObject;
    }

    uninitializeShader()
    {
        uninitializeShaders();
    }
};