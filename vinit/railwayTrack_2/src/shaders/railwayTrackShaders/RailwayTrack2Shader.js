class RailwayTrack2Shader{

    constructor() {
        // Shaders
        this.shaderProgramObject = null;

        // Uniforms
        this.mvpMatrixUniform;
        this.modelMatrixUniform;
        this.viewMatrixUniform;
        this.projectionMatrixUniform;

        this.textureSamplerUniform;

        this.vertexShaderObject;
        this.fragmentShaderObject;


        this.laUniform;
        this.ldUniform;
        this.lsUniform;

        this.kaUniform;
        this.kdUniform;
        this.ksUniform;

        this.materialShinessUniform;
        this.lightPositionUniform;

        this.lightAmbient = new Float32Array([0.1, 0.1, 0.1]);
        this.lightDiffuse = new Float32Array([1.0, 1.0, 1.0]);
        this.lightSpecular = new Float32Array([1.0, 1.0, 1.0]);
        this.lightPosition = new Float32Array([100.0, 100.0, 100.0]);

        this.materialAmbient = new Float32Array([0.0, 0.0, 0.0]);
        this.materialDiffuse = new Float32Array([0.560,0.570,0.580]);
        this.materialSpecular = new Float32Array([0.7, 0.7, 0.7]);
        this.materialShininess = 128.0;

        // vertex shader
        // vertex shader
        var vertexShaderSourceCode = 
        `#version 300 es
        \n
        precision highp float;
        in vec4 aPosition;
        in vec3 aNormal;
        uniform mat4 uMVPMatrix;
        uniform mat4 uModelMatrix;
        uniform mat4 uViewMatrix;
        uniform mat4 uProjectionMatrix;
        uniform vec3 uLightPosition;
        uniform mediump int uLightEnabled;
        out vec3 oTransformedNormals;
        out vec3 oLightDirection;
        out vec3 oViewerVector;
        //in vec4 aColor;
        //out vec4 oColor;
        void main(void)
        {
            if(uLightEnabled == 1)
            {
                vec4 eyeCoordinates = uViewMatrix * uModelMatrix * aPosition;
                mat3 normalMatrix = mat3(uViewMatrix * uModelMatrix);
                oTransformedNormals = normalMatrix * aNormal;

                oLightDirection = uLightPosition - eyeCoordinates.xyz;
                oViewerVector = -eyeCoordinates.xyz;
            }
            gl_Position = uProjectionMatrix * uViewMatrix * uModelMatrix * aPosition;
            //gl_Position = uMVPMatrix * aPosition;
        //oColor = aColor;
        }`;

        // vertex shader object
        this.vertexShaderObject = this.createShaderObjects(vertexShaderSourceCode, gl.VERTEX_SHADER);

        // fragment shader
        var fragmentShaderSourceCode =
        `#version 300 es
        \n
        precision highp float;
        in vec3 oTransformedNormals;
        in vec3 oLightDirection;
        in vec3 oViewerVector;
        uniform vec3 uLa;
        uniform vec3 uLd;
        uniform vec3 uLs;
        uniform vec3 uKa;
        uniform vec3 uKd;
        uniform vec3 uKs;
        uniform float uMaterialShineness;
        uniform mediump int uLightEnabled;
        //in vec4 oColor;
        out vec4 FragColor;
        void main(void)
        {
            vec3 phone_ADS_Light;
            if(uLightEnabled == 1)
            {
                vec3 ambient = uLa * uKa;
                vec3 normalizedTransformedNormals = normalize(oTransformedNormals);
                vec3 normalizedLightDirection = normalize(oLightDirection);

                vec3 diffuse = uLd * uKd * max(dot(normalizedLightDirection,normalizedTransformedNormals),0.0);
                
                vec3 reflectionVector = reflect(-normalizedLightDirection,normalizedTransformedNormals);
                vec3 normalizedViewerVector = normalize(oViewerVector);

                vec3 specular = uLs * uKs * pow(max(dot(reflectionVector,normalizedViewerVector),0.0), uMaterialShineness);

                phone_ADS_Light = ambient + diffuse + specular;
            }
            else
            {
                phone_ADS_Light = vec3(1.0,1.0,1.0);
            }
            FragColor = vec4(phone_ADS_Light,1.0);
        }`;

        // fragment shader object
        this.fragmentShaderObject = this.createShaderObjects(fragmentShaderSourceCode, gl.FRAGMENT_SHADER);
        
        // Attach and Link shaderObjects
        this.shaderProgramObject = attachAndLinkShaderObjects(
            [this.vertexShaderObject, this.fragmentShaderObject],
            [VertexAttributeEnum.CAM_ATTRIBUTE_POSITION,VertexAttributeEnum.CAM_ATTRIBUTE_NORMAL],
            ["aPosition","aNormal"]
        );
        if (this.shaderProgramObject == null) {
            alert("ShaderProgramObject failed");
        }

        // uniforms
        this.mvpMatrixUniform = this.getUniformLocation("uMVPMatrix");


        this.modelMatrixUniform = gl.getUniformLocation(this.shaderProgramObject, "uModelMatrix");
        this.viewMatrixUniform = gl.getUniformLocation(this.shaderProgramObject, "uViewMatrix");
        this.projectionMatrixUniform = gl.getUniformLocation(this.shaderProgramObject, "uProjectionMatrix");

        this.laUniform = gl.getUniformLocation(this.shaderProgramObject, "uLa");
        this.ldUniform = gl.getUniformLocation(this.shaderProgramObject, "uLd");
        this.lsUniform = gl.getUniformLocation(this.shaderProgramObject, "uLs");

        this.kaUniform = gl.getUniformLocation(this.shaderProgramObject, "uKa");
        this.kdUniform = gl.getUniformLocation(this.shaderProgramObject, "uKd");
        this.ksUniform = gl.getUniformLocation(this.shaderProgramObject, "uKs");

        this.materialShinessUniform = gl.getUniformLocation(this.shaderProgramObject, "uMaterialShineness");
        this.lightPositionUniform = gl.getUniformLocation(this.shaderProgramObject, "uLightPosition");
        this.lightingEnabledUniform = gl.getUniformLocation(this.shaderProgramObject, "uLightEnabled");
        
    }

    createShaderObjects(shaderStr, shaderType)
    {
        let shaderObject = createAndCompileShaders(shaderStr, shaderType);
        if (shaderObject == null) 
        {
            alert(shaderType + " Shader Compilation failed !!!");
            return null;
        }

        return shaderObject;
    }

    getUniformLocation(uniform)
    {
        return gl.getUniformLocation(this.shaderProgramObject, uniform);
    }

    getShaderProgramObject()
    {
        return this.shaderProgramObject;
    }

    uninitializeShader()
    {
        uninitializeShaders();
    }
};