// Function to convert degrees to Radian
function degToRad(degrees) {
    return degrees * Math.PI / 180.0;
}

// load texture
